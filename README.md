# 789 STARTER

### Introduction

...

### Installation

Laravel has a set of requirements in order to ron smoothly in specific environment. Please see [requirements](https://laravel.com/docs/7.x#server-requirements) section in Laravel documentation.

Metronic similarly uses additional plugins and frameworks, so ensure You have [Composer](https://getcomposer.org/) and [Node](https://nodejs.org/) installed on Your machine.

Assuming your machine meets all requirements - let's process to installation of Metronic Laravel integration (skeleton).

1. Open in cmd or terminal app and navigate to this folder
2. Run following commands

```bash
composer install
```

```bash
cp .env.example .env
```

```bash
php artisan key:generate
```

```bash
npm install
```

```bash
npm run dev
```

```bash
php artisan serve
```

And navigate to generated server link (http://127.0.0.1:8000)

Para generar un modelo:

```bash
php artisan krlove:generate:model User
```

Para Correr el Debug Analyse:

```bash
./vendor/bin/phpstan analyse
```

Como crear un nuevo catalogo

1. Cree la migración de la db y el modelo del catalogo a registrar y ejecutela.
2. Copie de la carpeta "App/Http/Controllers/Defafult" el archivo crudDefault carpeta modules y renombre.
3. Copie la carpeta default "resources/views/modules/default" a "resources/views/modules/admin" y renombre y ajuste segun campos de la db.
4. añada el resource dentro de "routes/web.php".
5. Renombre dentro de archivo CrudDefault lo siguiente:

    protected $\_table = "mx789_users"; // nombre de la tabla
    protected $\_module = "admin/users"; // directorio de las vistas
    protected $\_model = User::class; // instancia del modelo
    protected $\_title = "Usuarios"; // titulo del modulo

### Copyright

...
