<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        

        Schema::create('poblations', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 150)->nullable();
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::table('therapists', function (Blueprint $table) {
            $table->dropForeign('therapists_current_complementary_id_foreign');
            $table->dropColumn('current_complementary_id');
            $table->dropForeign('therapists_language_id_foreign');
            $table->dropColumn('language_id');
            $table->dropColumn('poblacion');

            $table->string('lic_otro', )->nullable();
            $table->string('cedula_master', )->nullable();
        });

        Schema::create('therapist_current_complementaries', function (Blueprint $table) {
            $table->unsignedBigInteger('therapist_id');
            $table->unsignedBigInteger('current_complementary_id');
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('therapist_languages', function (Blueprint $table) {
            $table->unsignedBigInteger('therapist_id');
            $table->unsignedBigInteger('languague_id');
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('therapist_poblations', function (Blueprint $table) {
            $table->unsignedBigInteger('therapist_id');
            $table->unsignedBigInteger('poblation_id');
            $table->timestamps();
            $table->softDeletes();
        });
        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('therapists', function (Blueprint $table) {
            $table->dropColumn('lic_otro');
            $table->dropColumn('cedula_master');
        });
    }
};
