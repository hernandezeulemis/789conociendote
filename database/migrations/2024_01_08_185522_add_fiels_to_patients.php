<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('patients', function (Blueprint $table) {
            $table->string('address_z_1', 250)->nullable();
            $table->string('cod_postal_z_1', 150)->nullable();
            $table->string('city_z_1', 150)->nullable();
            $table->string('delegation_z_1', 250)->nullable();
            $table->string('colonia_z_1', 250)->nullable();
            $table->string('address_z_2', 250)->nullable();
            $table->string('cod_postal_z_2', 150)->nullable();
            $table->string('city_z_2', 150)->nullable();
            $table->string('delegation_z_2', 250)->nullable();
            $table->string('colonia_z_2', 250)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('patients', function (Blueprint $table) {
            //
        });
    }
};
