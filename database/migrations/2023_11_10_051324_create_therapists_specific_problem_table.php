<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('therapist_specific_problem', function (Blueprint $table) {
            $table->unsignedBigInteger('therapist_id');
            $table->unsignedBigInteger('specific_problem_id');
            $table->timestamps();
/* 
            $table->foreign('therapist_id')->references('id')->on('therapists');
            $table->foreign('specific_problem_id')->references('id')->on('specific_problems'); */
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('therapeut_specific_problem');
    }
};
