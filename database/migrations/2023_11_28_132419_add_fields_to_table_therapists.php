<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('therapists', function (Blueprint $table) {
            $table->string('qualification')->nullable();
            $table->string('minimum_fee')->nullable();
            $table->enum('invoice', ['si', 'no'])->default('no')->notNullable();
            $table->string('admin_notes',350)->nullable();
            $table->string('therapist_notes',350)->nullable();
            $table->longText('about_me', )->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('therapists', function (Blueprint $table) {
            //
        });
    }
};
