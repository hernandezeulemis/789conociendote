<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('therapists', function (Blueprint $table) {
            $table->unsignedBigInteger('user_terapist_id')->nullable()->after('id');
            $table->foreign('user_terapist_id','fk_user_therapist_1523698741')->references('id')->on('mx789_users');
            $table->enum('status_therapist', ['nuevo','activo', 'baja_temporal','baja_definitiva','principiante'])->default('nuevo')->notNullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('therapists', function (Blueprint $table) {
            $table->dropForeign(['user_terapist_id']);
            $table->dropColumn('user_terapist_id');
            $table->dropColumn('status_therapist');
        });
    }
};
