<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('patients', function (Blueprint $table) {
            $table->id();
            $table->string("full_name");
            $table->string("phone")->nullable();
            $table->double("cost")->nullable();
            $table->double("commission")->nullable();
            $table->date("first_date")->nullable();
            $table->integer('payment_reference_id')->nullable();;
            $table->string('counselor_name')->nullable();
            $table->string('counselor_phone')->nullable();
            $table->text('comments')->nullable();
            $table->enum('status', ['agendado','confirmado','cancelado'])->nullable();
            $table->enum('age', ['de 20 a 30','de 31 a 50','50+'])->nullable();
            $table->enum('reference', ['facebook','instagram','google','recomendación'])->nullable();
            $table->string('population', 250)->nullable();
            $table->string('occupation', 250)->nullable();
            $table->enum('therapy_before', ['si','no'])->nullable();
            $table->unsignedBigInteger('current_principal_id')->nullable();
            $table->foreign('current_principal_id')->references('id')->on('current_principals');
            $table->string('modality', 150)->nullable();
            $table->enum('degree', ['si','no'])->nullable();
            $table->enum('requires_invoice', ['si','no'])->nullable();
            $table->enum('gender', ['hombre','mujer','indiferente'])->nullable();
            $table->unsignedBigInteger('user_id')->nullable()->comment('Id de la Orientadora Conectada');
            $table->foreign('user_id')->references('id')->on('mx789_users');
            $table->date("first_date_quote")->nullable();
            $table->string('reasons')->nullable();
            $table->text('process_notes')->nullable();
            $table->text('therapist_notes')->nullable();
            $table->double('amount_paid')->nullable();
            $table->date("payment_date")->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('patients');
    }
};
