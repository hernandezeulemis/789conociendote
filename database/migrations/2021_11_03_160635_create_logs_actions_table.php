<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLogsActionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mx789_logs_actions', function (Blueprint $table) {
            $table->id();
            $table->string('title');
            $table->string('id_model');
            $table->string('model')->index();
            $table->string('user_id');
            $table->string('comment');
            $table->string('url');
            $table->string('ip');
            $table->string('controller_action');
            $table->string('browser');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mx789_logs_actions');
    }
}
