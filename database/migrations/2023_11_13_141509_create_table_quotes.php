<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('quotes', function (Blueprint $table) {
            $table->id();
            //$table->string('name', 150)->nullable();
            $table->date('date')->notNullable();
            $table->string('hour', 15)->nullable();
            $table->enum('status', ['pendiente','lograda', 'cancelada','reagendada'])->default('pendiente')->notNullable();
            $table->unsignedBigInteger('therapist_id')->nullable();
            $table->foreign('therapist_id')->references('id')->on('therapists');
            $table->unsignedBigInteger('user_id')->nullable();
            $table->foreign('user_id')->references('id')->on('mx789_users');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('quotes');
    }
};
