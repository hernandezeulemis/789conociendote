<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('apply_therapists', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('full_name', 150)->nullable();
            $table->string('email', 150)->notNullable();
            $table->date('birthdate')->notNullable();
            $table->enum('sexo', ['m', 'f'])->notNullable();
            $table->string('lic_psicologia', 5)->nullable();
            $table->unsignedBigInteger('clinical_specialty_id')->nullable();
            $table->foreign('clinical_specialty_id')->references('id')->on('clinical_specialties');
            $table->string('semester', 150)->nullable();
            $table->string('cedula', 150)->nullable();
            $table->unsignedBigInteger('current_principal_id')->nullable();
            $table->foreign('current_principal_id')->references('id')->on('current_principals');
            $table->unsignedBigInteger('current_complementary_id')->nullable();
            $table->foreign('current_complementary_id')->references('id')->on('current_complementaries');
            $table->unsignedBigInteger('specific_problem_id')->nullable();
            $table->string('poblacion', 250)->notNullable();
            $table->unsignedBigInteger('language_id')->nullable();
            $table->foreign('language_id')->references('id')->on('languages');
            $table->string('modality', 150)->notNullable();
            $table->string('address', 250)->notNullable();
            $table->string('cod_postal', 150)->notNullable();
            $table->string('city', 150)->notNullable();
            $table->string('delegation', 250)->notNullable();
            $table->string('colonia', 250)->notNullable();
            $table->string('foto', 250)->notNullable();
            $table->string('titulo', 250)->notNullable();
            $table->enum('status', ['pendiente', 'aceptado','rechazado','cita_agendada','de_alta'])->default('pendiente')->notNullable();
            $table->timestamps();
            $table->softDeletes();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('apply_therapists');
    }
};
