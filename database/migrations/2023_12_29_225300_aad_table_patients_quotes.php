<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('patients_quotes', function (Blueprint $table) {
            $table->id();
            $table->string("full_name")->nullable();
            $table->string("email")->nullable();
            $table->string("phone")->nullable();
            $table->string("comments")->nullable();
            $table->date("date")->nullable();
            $table->string("hour")->nullable();
            $table->string('counselor_name')->nullable();
            $table->enum('status', ['agendado','confirmado','cancelado'])->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('patients_quotes');
    }
};
