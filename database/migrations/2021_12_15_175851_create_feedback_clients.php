<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFeedbackClients extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mx789_feedback_clients', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('img');
            $table->string('comment');
            $table->string('app_cod');
            $table->string('app_coo');
            $table->string('app_jav');
            $table->string('app_lan');
            $table->string('app_nam');
            $table->string('app_ver');
            $table->string('cli_min');
            $table->string('cli_pla');
            $table->string('cli_plu');
            $table->string('cli_use');
            $table->string('glo_url');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mx789_feedback_clients');
    }
}
