<?php

namespace Database\Seeders;

use App\Models\Therapist;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Faker\Factory as Faker;

class TherapistsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
       $faker = Faker::create();

        for ($i = 0; $i < 20; $i++) {
             $therapeut = Therapist::create([
                            'full_name' => $faker->name,
                            'email' => $faker->email,
                            'birthdate' => $faker->date,
                            'sexo' => $faker->randomElement(['m', 'f']),
                            'lic_psicologia' => $faker->numberBetween(10000, 99999),
                            'clinical_specialty_id' => $faker->numberBetween(1, 3),
                            'semester' => $faker->word,
                            'cedula' => $faker->numberBetween(100000, 999999),
                            'current_principal_id' => $faker->numberBetween(4, 35),
                            'current_complementary_id' => $faker->numberBetween(3, 34),
                            //'specific_problem_id' => $faker->numberBetween(1, 36),
                            'poblacion' => $faker->city,
                            'language_id' => $faker->numberBetween(1, 2),
                            'modality' => $faker->word,
                            'address' => $faker->address,
                            'cod_postal' => $faker->postcode,
                            'city' => $faker->city,
                            'delegation' => $faker->word,
                            'colonia' => $faker->word,
                            'foto' => $faker->imageUrl(),
                            'titulo' => $faker->sentence
                        ]);

            $therapeut->specificProblems()->attach($faker->numberBetween(1, 36));
        }
    }
}
