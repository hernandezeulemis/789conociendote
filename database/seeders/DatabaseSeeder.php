<?php

use Illuminate\Database\Seeder;
use App\Models\User;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    private $permissions = [
        'crear usuarios',
        'editar usuarios',
        'ver usuarios',
        'eliminar usuarios',
        'crear permisos',
        'editar permisos',
        'ver permisos',
        'eliminar permisos',
        'crear roles',
        'editar roles',
        'ver roles',
        'eliminar roles',
    ];
    public function run()
    {   
        $this->createPermissions();
        $this->createSupportRoleAndUsers();
        $this->createAdminRoleAndUsers();
        
    }

    public function createPermissions() {
        $permissions = $this->permissions;
        for($i = 0; $i < count($permissions); $i++) {
            $permission = $permissions[$i];
            $permission_exist = Permission::where('name',$permission)->exists();
            if(!$permission_exist){
                Permission::create(['name' => $permission]);
            }
        }
    }

    public function createSupportRoleAndUsers() {
        /* Support role */
        $role_support_name = 'support';
        $role_exist = Role::where('name',$role_support_name)->exists();
        if(!$role_exist){
            $role = Role::create(['name' => $role_support_name]);
        }
        

        $emails_support = env('EMAILS_SUPPORT');
        $array_emails = explode(',',$emails_support);
        for($i = 0; $i < count($array_emails); $i++){
            $email = $array_emails[$i];
            $user_exist = User::where('email',$email)->exists();
            if(!$user_exist) {
                $user = new User();
                $user->name = 'Soporte_'.$i;
                $user->email = $email;
                $user->password = bcrypt('support1234');
                $user->save();
                $user->assignRole($role_support_name);
            }
        }
    }

    public function createAdminRoleAndUsers() {
        /* Admin role */
        $role_admin_name = 'admin';
        $role_exist = Role::where('name',$role_admin_name)->exists();
        if(!$role_exist){
            $role = Role::create(['name' => $role_admin_name]);
            $role->syncPermissions($this->permissions);
        }
        

        $emails_admin = env('EMAILS_ADMIN');
        $array_emails = explode(',',$emails_admin);
        for($i = 0; $i < count($array_emails); $i++){
            $email = $array_emails[$i];
            $user_exist = User::where('email',$email)->exists();
            if(!$user_exist) {
                $user = new User();
                $user->name = 'Admin_'.$i;
                $user->email = $email;
                $user->password = bcrypt('admin1234');
                $user->save();
                $user->assignRole($role_admin_name);
            }
        }
    }
}
