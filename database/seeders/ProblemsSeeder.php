<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\SpecificProblem;

class ProblemsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $arrayProblems = [
            'Perspectiva de genero',
            'Adicciones',
            'Tanatologia',
            'Crianza',
            'Orientación vocacional',
            'Sexualidad',
            'Embarazo',
            'Infertilidad',
            'Perdida perinatal',
            'Duelos',
            'Violencia',
            'Enfermedades crónicas',
            'TCA',
            'TLP',
            'TDA',
            'Discapacidad',
            'Traumas',
            'Abuso sexual',
            'Posparto',
            'Meditación',
            'Energias',
            'Depresión',
            'Ansiedad',
            'Suicidio',
            'Autismo',
            'Codependecia',
            'Problemas de apego',
            'Pacientes',
            'Oncológicos',
            'Problemas de sueño',
            'Psicosomatico',
            'Divorcios',
            'TOC',
            'Autoestima',
            'Violencia domestica '
        ];
        
        foreach($arrayProblems as $problem){
            $specific_problem = SpecificProblem::create(['name' => $problem]);
        }
        
    }
}
