<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\CurrentPrincipal;
use App\Models\CurrentComplementary;

class CurrentsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $arrayCurrents = [
            'TCC',
            'Sistemico',
            'Humanista',
            'Psicoanalisis',
            'Intervencion en crisis',
            'Gestalt',
            'Integrativo',
            'Contextuales',
            'DBTACT',
            'Logoterapia',
            'Jungiano',
            'Counseling',
            'TREC',
            'Psicologia positiva',
            'Terapia corporal',
            'Terapia transpersonal',
            'Metodos alternativos',
            'Terapia Breve',
            'Terapia de arte',
            'Mindufulness',
            'Sexologia',
            'Narrativa',
            'Familiar',
            'Aplicación de pruebas',
            'Terapia interpersonal',
            'Psicodrama',
            'EMDR',
            'Programación neurolinguistica',
            'Coaching',
            'Tanatologia',
            'Aprendizaje',
            'Psicologia del deporte'
        ];
        
        foreach($arrayCurrents as $current){
            $currentPrincipal = CurrentPrincipal::create(['name' => $current]);
            $currentComplementary = CurrentComplementary::create(['name' => $current]);

        }
        
    }
}
