<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


Route::group(['namespace' => 'Api'], function () {
    Route::resource("equipos", "EquiposController");
    Route::resource("traspasos", "TraspasosController");
    Route::resource("checkins", "CheckinsController");
    Route::resource("cierres", "CierresController");
    Route::resource("fallas", "FallasController");
    Route::resource("excedentes", "ExcedentesController");
    Route::resource("rutas", "RutasController");
    Route::resource("bancos", "BancosController");
});

Route::group([

    'middleware' => 'api',
    'prefix' => 'auth'

], function ($router) {

    Route::post('login', 'AuthController@login');
    Route::post('logout', 'AuthController@logout');
    Route::post('refresh', 'AuthController@refresh');
    Route::get('me', 'AuthController@me');

});

Route::post('/user-create', function(Request $request){
    $user = App\User::create([
        'name'=>$request->name,
        'email'=>$request->email,
        'password'=> Hash::make($request->password)
    ]);
    return response()->json($user);
})->name('user');
