<?php

use Illuminate\Support\Facades\Route;
use App\Http\Middleware\CheckSignature;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect("/login");
});


//Auth::routes();

Auth::routes();


Route::group(['middleware' => ['auth', 'check_signature']], function () {
        
    //admin routes

  Route::group(['prefix' => 'admin', 'namespace' => 'Admin'], function () {
        //Route::get('dashboard', 'UserController@profile')->name('dashboard');
        Route::get('/dashboard', function () {
            return redirect("/admin/profile");
        });
        
        //PROFILE
        Route::get('profile',   'ProfileController@profile')->name('profile');
        Route::get('profileshow',   'ProfileController@profileshow')->name('profileshow');
        Route::get('profiletherapist',   'ProfileController@profileTherapist')->name('profileTherapist');
        Route::post('profile/update_profile_therapist', 'ProfileController@updateProfileTherapist')->name('update_profile_therapist');
        Route::post('profile/update_profile', 'ProfileController@updateProfile')->name('update_profile');
        Route::get('contrato', 'ProfileController@contrato')->name('contrato');
        Route::get('contrato_by_id/{id?}', 'ProfileController@contratoById')->name('contrato');
        

        
        Route::get("users/pdf", "UserController@pdf");
        Route::resource("users", "UserController")->name('*','users');
        //Route::resource('users', 'UserController');
        Route::resource("permissions", "PermissionsController")->name('*','permissions');
        Route::resource("roles", "RolesController")->name('*','roles');
        
        //CATALAGOS
        Route::resource("current_principal", "CurrentPrincipalController")->name('*','principal');
        Route::resource("current_complementary", "CurrentComplementaryController")->name('*','complementary');
        Route::resource("specific_problem", "SpecificProblemController")->name('*','specific_problem');
        Route::resource("language", "LanguageController")->name('*','language');
        Route::resource("poblation", "PoblationController")->name('*','poblation');
        Route::resource("clinical_specialties", "ClinicalSpecialtiesController")->name('*','clinical_specialties');
        
        //CANDIDATES
        Route::get("candidates/get_data/{status?}", "CandidatesController@getData")->name('*','candidates.getData');
        
        Route::get("candidates/change_status/{id?}", "CandidatesController@changeStatus")->name('*','candidates.changeStatus');
        Route::get("candidates/decline/{id?}", "CandidatesController@decline")->name('*','candidates.decline');
        Route::get("candidates/de_alta/{id?}", "CandidatesController@deAlta")->name('*','candidates.deAlta');
        Route::post("candidates/send_contact", "CandidatesController@sendContact")->name('*','candidates.sendContact');
        Route::post("candidates/save_quotes", "CandidatesController@saveQuotes")->name('*','candidates.saveQuotes');
        Route::get("candidates/delete_files/{id?}", "CandidatesController@deleteFiles")->name('*','candidates.deleteFiles');
        Route::post("candidates/save_files", "CandidatesController@saveFiles")->name('*','candidates.saveFiles');
        
        Route::resource("candidates", "CandidatesController")->name('*','candidates');

        //QUOTES
        Route::get("quotes/get_data/{status?}", "QuotesController@getData")->name('*','quotes.getData');
        Route::resource("quotes", "QuotesController")->name('*','quotes');
        Route::get("quotes/{status?}", "QuotesController@index");
        Route::get("quotes/get_quote/{id}", "QuotesController@getQuote");
        Route::post("quotes/change_status", "QuotesController@changeStatus")->name('*','quotes.changeStatus');
        Route::post("quotes/save_quotes", "QuotesController@saveQuotes")->name('*','quotes.saveQuotes');

        //TERAPISTAS
        Route::get("therapists/get_data/{status?}", "TherapistsController@getData")->name('*','therapists.getData');
        Route::get("therapists/get_note_for_therapist_id/{id?}", "TherapistsController@getNoteForTherapist")->name('*','therapists.getNoteForTherapist');
        
        Route::resource("therapists", "TherapistsController")->name('*','therapists');
        Route::post("therapists/change_status/{id?}", "TherapistsController@changeStatus")->name('*','therapists.changeStatus');
        Route::post("therapists/save_nota", "TherapistsController@saveNota")->name('*','therapists.saveNota');
        
    });

    Route::resource("default", "DefaultBase\CrudDefaultController")->name('*','default');
    
    
   
    Route::get('feedback', 'HomeController@getFeedBackClient');
    Route::post('feedback', 'HomeController@feedBackClient');
    Route::get('logs_actions', 'HomeController@logsActions');
    Route::get('docs', 'HomeController@docs');

    
});

Route::group(['prefix' => 'contrato', 'namespace' => 'Admin'], function () {
    Route::get('/firmar-contrato', "ContractController@showContract")->name('firmar_contrato');
    Route::post('/firmar-contrato', "ContractController@processContract")->name('procesar_contrato');
    Route::post('/add-firma', "ContractController@addFirma")->name('addFirma');
});


Route::get('logs', '\Rap2hpoutre\LaravelLogViewer\LogViewerController@index');

Route::post("apply_therapists/verificar-email", "ApplyTherapistsController@verificarEmail")->name('apply_therapists.verificarEmail');
Route::post("apply_therapists/store", "ApplyTherapistsController@store")->name('apply_therapists.store');
Route::resource("apply_therapists", "ApplyTherapistsController")->name('*','apply_therapists');
