<?php
// Aside menu
return [

    'items' => [
        // Dashboard
        /* [
            'title' => 'Dashboard',
            'root' => true,
            'icon' => 'fas fa-tachometer-alt icon_aside', // or can be 'flaticon-home' or any flaticon-*
            'page' => '/',
            'new-tab' => false,
            'role' => ['admin','rrhh','therapist'],
        ], */
        [
            'title' => 'Catálogos',
            'root' => true,
            'icon' => 'fas fa-bars icon_aside',
            'bullet' => 'dot',
            'role' => ['admin'],
            'submenu' => [
                [
                    'title' => 'Corriente Principal',
                    'root' => true,
                    'icon' => 'fa fa-asterisk icon_aside', // or can be 'flaticon-home' or any flaticon-*
                    'page' => '/admin/current_principal',
                    'new-tab' => false,
                    'role' => ['admin']
                ],
                [
                    'title' => 'Corriente Complementaria',
                    'root' => true,
                    'icon' => 'media/svg/icons/Design/Layers.svg', // or can be 'flaticon-home' or any flaticon-*
                    'page' => '/admin/current_complementary',
                    'new-tab' => false,
                    'role' => ['admin']
                ],
                [
                    'title' => 'Idiomas',
                    'root' => true,
                    'icon' => 'media/svg/icons/Design/Layers.svg', // or can be 'flaticon-home' or any flaticon-*
                    'page' => '/admin/language',
                    'new-tab' => false,
                    'role' => ['admin']
                ],
                [
                    'title' => 'Población',
                    'root' => true,
                    'icon' => 'media/svg/icons/Design/Layers.svg', // or can be 'flaticon-home' or any flaticon-*
                    'page' => '/admin/poblation',
                    'new-tab' => false,
                    'role' => ['admin']
                ],
                [
                    'title' => 'Problemáticas Especificos',
                    'root' => true,
                    'icon' => 'media/svg/icons/Design/Layers.svg', // or can be 'flaticon-home' or any flaticon-*
                    'page' => '/admin/specific_problem',
                    'new-tab' => false,
                    'role' => ['admin']
                ],
                [
                    'title' => 'Especialidades Clínicas',
                    'root' => true,
                    'icon' => 'media/svg/icons/Design/Layers.svg', // or can be 'flaticon-home' or any flaticon-*
                    'page' => '/admin/clinical_specialties',
                    'new-tab' => false,
                    'role' => ['admin']
                ],
            ]
        ],
        [
            'title' => 'Administración de Usuarios',
            'root' => true,
            'icon' => 'fas fa-users icon_aside',
            'bullet' => 'dot',
            'role' => ['admin'],
            'submenu' => [        
                [
                    'title' => 'Usuarios',
                    'root' => true,
                    'icon' => 'media/svg/icons/Design/Layers.svg', // or can be 'flaticon-home' or any flaticon-*
                    'page' => '/admin/users',
                    'new-tab' => false,
                    'role' => ['admin'],
                ],
                [
                    'title' => 'Roles',
                    'root' => true,
                    'icon' => 'media/svg/icons/Design/Layers.svg', // or can be 'flaticon-home' or any flaticon-*
                    'page' => '/admin/roles',
                    'new-tab' => false,
                    'role' => ['admin'],
                ],
                [
                    'title' => 'Permisos',
                    'root' => true,
                    'icon' => 'media/svg/icons/Design/Layers.svg', // or can be 'flaticon-home' or any flaticon-*
                    'page' => '/admin/permissions',
                    'new-tab' => false,
                    'role' => ['admin'],
                ],       
            ],
        ],
        [
            'title' => 'Candidatos',
            'root' => true,
            'icon' => 'fas fa-user-tie icon_aside', // or can be 'flaticon-home' or any flaticon-*
            'page' => '/admin/candidates',
            'new-tab' => false,
            'role' => ['rrhh'],
        ],
        [
            'title' => 'Citas',
            'root' => true,
            'icon' => 'fas fa-calendar icon_aside', // or can be 'flaticon-home' or any flaticon-*
            'page' => '/admin/quotes',
            'new-tab' => false,
            'role' => ['rrhh'],
        ],
         [
            'title' => 'Terapeutas',
            'root' => true,
            'icon' => 'fas fa-user-md icon_aside', // or can be 'flaticon-home' or any flaticon-*
            'page' => '/admin/therapists',
            'new-tab' => false,
            'role' => ['rrhh'],
        ],

        [
            'title' => 'Mi Pefil',
            'root' => true,
            'icon' => 'fas fa-user-edit icon_aside', // or can be 'flaticon-home' or any flaticon-*
            'page' => '/admin/profile',
            'new-tab' => false,
            'role' => ['admin','rrhh'],
        ],
                [
            'title' => 'Perfil',
            'root' => true,
            'icon' => 'fas fa-users icon_aside',
            'bullet' => 'dot',
            'role' => ['therapist'],
            'submenu' => [        
                [
                    'title' => 'Ver Perfil',
                    'root' => true,
                    'icon' => 'media/svg/icons/Design/Layers.svg', 
                    'page' => '/admin/profileshow',
                    'new-tab' => false,
                    'role' => ['admin','rrhh','therapist'],
                ],
                [
                    'title' => 'Editar Perfil',
                    'root' => true,
                    'icon' => 'media/svg/icons/Design/Layers.svg', 
                    'page' => '/admin/profiletherapist',
                    'new-tab' => false,
                    'role' => ['admin','rrhh','therapist'],
                ],
                [
                    'title' => 'Contrato',
                    'root' => true,
                    'icon' => 'media/svg/icons/Design/Layers.svg',
                    'page' => '/admin/contrato',
                    'new-tab' => true,
                    'role' => ['admin','rrhh','therapist'],
                ],
                  [
                    'title' => 'Actualizar Datos',
                    'root' => true,
                    'icon' => 'media/svg/icons/Design/Layers.svg', 
                    'page' => '/admin/profile',
                    'new-tab' => false,
                    'role' => ['therapist'],
                ], 
                

            ],
        ],
        

    ]

];
