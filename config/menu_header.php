<?php
// Header menu
return [

    'items' => [
        /* example with submenus */
        [
            'title' => 'Menu Principal',
            'root' => true,
            'toggle' => 'click',
            'submenu' => [
                'type' => 'classic',
                'alignment' => 'left',
                'items' => [
                    [
                        'title' => 'Permisos',
                        'page' => 'admin/permissions',
                        'role' => ['admin']

                    ],
                    [
                        'title' => 'Usuarios',
                        'page' => 'admin/users',
                        'role' => ['admin']
                    ],
                    [
                        'title' => 'Roles',
                        'page' => 'admin/roles',
                        'role' => ['admin']
                    ],
                    [
                        'title' => 'Soporte',
                        'page' => 'admin/support',
                        'role' => ['support']
                    ]
                ]
            ]
        ],
        [
            'title' => 'Usuarios',
            'root' => true,
            'page' => '/admin/users',
            'new-tab' => false,
            'role' => ['admin']
        ],
        [
            'title' => 'Permisos',
            'root' => true,
            'page' => '/admin/permissions',
            'new-tab' => false,
            'role' => ['admin']
        ],
        [
            'title' => 'Roles',
            'root' => true,
            'page' => '/admin/roles',
            'new-tab' => false,
            'role' => ['admin']
        ]
    ]
];
