<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Haruncpi\LaravelIdGenerator\IdGenerator;
use Illuminate\Database\Eloquent\SoftDeletes;

class CurrentComplementary extends Model
{   
    use SoftDeletes;
    
    protected $table = 'current_complementaries';
    
    protected $fillable = [
        'id',
        'name' 
    ];
   
}




