<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Haruncpi\LaravelIdGenerator\IdGenerator;
use Illuminate\Database\Eloquent\SoftDeletes;

class ClinicalSpecialty extends Model
{   
    use SoftDeletes;
    
    protected $table = 'clinical_specialties';
    
    protected $fillable = [
        'id',
        'name' 
    ];
 
}




