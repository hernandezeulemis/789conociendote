<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AddresClinic extends Model
{
    use HasFactory;
    
    protected $table = 'addres_clinics';
    
    protected $fillable = [
        'therapist_id',
        'address',
    ];

    public function therapist()
    {
        return $this->belongsTo(Therapist::class, 'therapist_id');
    }
}
