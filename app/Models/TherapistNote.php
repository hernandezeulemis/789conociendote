<?php

namespace App\Models;

use App\Models\Therapist;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class TherapistNote extends Model
{
    use HasFactory;

    protected $table = 'therapit_notes';
    
    protected $fillable = [

        'therapist_id',
        'note'

    ];

    public function therapist()
    {
        return $this->belongsTo(Therapist::class, 'therapist_id');
    }

    public function users()
    {
        return $this->belongsTo(User::class);
    }

}
