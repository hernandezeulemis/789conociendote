<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Degree extends Model
{
    use HasFactory;

    protected $fillable = ['licenciatura', 'institucion', 'therapist_id'];

    public function therapist()
    {
        return $this->belongsTo(Therapist::class);
    }
    

    public function degreeFiles()
    {
        return $this->hasMany(DegreeFile::class);
    }
}
