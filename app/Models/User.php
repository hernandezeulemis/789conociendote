<?php

namespace App\Models;

use App\Helpers\LogAction;
use Illuminate\Support\Facades\Auth;
use Spatie\Permission\Traits\HasRoles;
use Tymon\JWTAuth\Contracts\JWTSubject;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable implements JWTSubject
{
    use Notifiable;
    use HasRoles;
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'mx789_users';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'role'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];


    public static function getRoles()
    {
        return [
            'admin' => 'Admin',
        ];
    }

    // Rest omitted for brevity

    /**
     * Get the identifier that will be stored in the subject claim of the JWT.
     *
     * @return mixed
     */
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }

    public static function getTableName(){
        return with(new static)->getTable();
    }
    protected static function booted()
    {
        static::created(function ($user) {
            LogAction::create('Registro de usuario',$user->id,'App\User','store');
        });
        static::updated(function ($user) {
            LogAction::create('Actualización de usuario',$user->id,'App\User','update');
        });
    }

    public function therapist()
    {
        return $this->hasOne(Therapist::class,'user_terapist_id');
    }

    public static function getCurrent() 
    {
        if(Auth::check()) {
            return Auth::user();
        }
        return false;
    }
    
}
