<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Haruncpi\LaravelIdGenerator\IdGenerator;
use Illuminate\Database\Eloquent\SoftDeletes;

class Poblation extends Model
{   
    use SoftDeletes;
    
    protected $table = 'poblations';
    
    protected $fillable = [
        'id',
        'name' 
    ];
 
}




