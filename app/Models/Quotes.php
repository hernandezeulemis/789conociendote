<?php

namespace App\Models;

use App\Models\Therapist;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Quotes extends Model
{
    use HasFactory;

    protected $table = 'quotes';
    
    protected $fillable = [
        //'name',
        'date',
        'hour',
        'status',
        'therapist_id',
        'user_id'

    ];

    public function therapist()
    {
        return $this->belongsTo(Therapist::class, 'therapist_id');
    }

    public function users()
    {
        return $this->belongsTo(User::class);
    }

}
