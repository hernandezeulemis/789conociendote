<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class DegreeFile extends Model
{
    use HasFactory;

     protected $fillable = ['file_name', 'file_path', 'degree_id'];
     protected $appends = ['url'];

    public function getUrlAttribute()
    {
        return Storage::url($this->file_path);
        /* if ($this->foto) {
            return asset('storage/uploads/' . $this->foto);
            //return asset('uploads/' . $this->foto);
        }

        return null; */
    }

    
    
    public function degree()
    {
        return $this->belongsTo(Degree::class);
    }
}
