<?php

namespace App\Models;

use Carbon\Carbon;
use App\Models\User;
use App\Models\Languague;
use App\Models\SpecificProblem;
use App\Models\CurrentPrincipal;
use App\Models\ClinicalSpecialty;
use App\Models\CurrentComplementary;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Therapist extends Model
{
    use HasFactory;

    protected $table = 'therapists';
    
    protected $fillable = [
        'full_name',
        'email',
        'birthdate',
        'sexo',
        'lic_psicologia',
        'lic_otro',
        'clinical_specialty_id',
        'semester',
        'cedula',
        'current_principal_id',
        'current_complementary_id',
        //'specific_problem_id',
        'clinical_specialty_id',
        'poblacion',
        'language_id',
        'modality',
        'address',
        'cod_postal',
        'city',
        'delegation',
        'colonia',
        'foto',
        'titulo',
        'note',
        'cedula_master',
        'user_id',
        'user_terapist_id',
        'qualification',
        'minimum_fee',
        'invoice',
        'admin_notes',
        'therapist_notes',
        'about_me',
        'if_master_clinica',
        'master_otro',
        'phone',
        'note_qualification',
        'minimum_in_person_fee',
        'minimum_fee_family'
        
    ];

    protected $appends = ['avatar_url'];

    public function getAvatarUrlAttribute()
    {
        if ($this->foto) {
            return asset('storage/uploads/' . $this->foto);
            //return asset('uploads/' . $this->foto);
        }

        return null;
    }

    public function clinicalSpecialty()
    {
        return $this->belongsTo(ClinicalSpecialty::class);
    }

    public function currentPrincipal()
    {
        return $this->belongsTo(CurrentPrincipal::class);
    }

   

    public function specificProblems()
    {
        return $this->belongsToMany(SpecificProblem::class, 'therapist_specific_problem')->withTimestamps();
    }

    public function therapistPoblations()
    {
        return $this->belongsToMany(Poblation::class, 'therapist_poblations')->withTimestamps();
    }


    public function therapistLanguages()
    {
        return $this->belongsToMany(Languague::class, 'therapist_languages')->withTimestamps();
    }

    public function therapistCurrentComplementaries()
    {
        return $this->belongsToMany(CurrentComplementary::class, 'therapist_current_complementaries')->withTimestamps();
    }
    

    public function userTerapist()
    {
        return $this->belongsTo(User::class, 'user_terapist_id', 'id');
    }

    // user rrhh
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function getBirtdateAttribute($value)
    {
        return $value ? Carbon::parse($value)->format('YYYY-mm-dd') : null;
    }

    public function setBirtdateAttribute($value)
    {
        $this->attributes['birtdate'] = $value ? Carbon::createFromFormat('YYYY-mm-dd', $value)->format('Y-m-d') : null;
    }

    public function degrees()
    {
        return $this->hasMany(Degree::class);
    }
}
