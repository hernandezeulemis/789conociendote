<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TherapistScheduleTimeslot extends Model
{
    use HasFactory;

    protected $table = 'therapist_schedule_timeslots';
    
    protected $fillable = [

        'therapist_schedule_id',
        'start_time',
        'end_time'
    ];

    public function schedule()
    {
        return $this->belongsTo(TherapistSchedule::class);
    }
}
