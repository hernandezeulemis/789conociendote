<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Haruncpi\LaravelIdGenerator\IdGenerator;
use Illuminate\Database\Eloquent\SoftDeletes;

class SpecificProblem extends Model
{   
    use SoftDeletes;
    
    protected $table = 'specific_problems';
    
    protected $fillable = [
        'id',
        'name' 
    ];

    public function therapists()
    {
        return $this->belongsToMany(Therapist::class, 'therapist_specific_problem');
    }
 
}




