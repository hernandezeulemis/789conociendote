<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Haruncpi\LaravelIdGenerator\IdGenerator;
use Illuminate\Database\Eloquent\SoftDeletes;

class CurrentPrincipal extends Model
{   
    use SoftDeletes;
    
    protected $table = 'current_principals';
    
    protected $fillable = [
        'id',
        'name' 
    ];
 
}




