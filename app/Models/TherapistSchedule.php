<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TherapistSchedule extends Model
{
    use HasFactory;

    protected $table = 'therapist_schedules';
    
    protected $fillable = [

        'therapist_id',
        'day_of_week'

    ];

    public function therapist()
    {
        return $this->belongsTo(Therapist::class);
    }

    public function timeslots()
    {
        return $this->hasMany(TherapistScheduleTimeslot::class);
    }
}
