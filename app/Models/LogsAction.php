<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Haruncpi\LaravelIdGenerator\IdGenerator;

class LogsAction extends Model
{   
    public $incrementing = false;
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'mx789_logs_actions';
    
    protected $fillable = [
        'id',
        'title',
        'id_model',
        'model',
        'user_id',
        'comment',
        'url',
        'ip',
        'controller_action',
        'browser'
    ];
    public static function boot()
    {
        parent::boot();
        self::creating(function ($model) {
            $model->id = IdGenerator::generate(['table' => 'mx789_logs_actions', 'length' => 6, 'prefix' =>date('y')]);
        });
    }
}
