<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Haruncpi\LaravelIdGenerator\IdGenerator;
use Illuminate\Database\Eloquent\SoftDeletes;

class Languague extends Model
{   
    use SoftDeletes;
    
    protected $table = 'languages';
    
    protected $fillable = [
        'id',
        'name' 
    ];
 
}




