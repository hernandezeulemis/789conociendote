<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Haruncpi\LaravelIdGenerator\IdGenerator;

class FeedbackClients extends Model
{   
    protected $table = 'mx789_feedback_clients';
    public $incrementing = false;
    protected $fillable = [
        'id',
        'img',
        'comment',
        'app_cod',
        'app_coo',
        'app_jav',
        'app_lan',
        'app_nam',
        'app_ver',
        'cli_min',
        'cli_pla',
        'cli_plu',
        'cli_use',
        'glo_url'
    ];
    public static function boot()
    {
        parent::boot();
        self::creating(function ($model) {
            $model->id = IdGenerator::generate(['table' => 'mx789_feedback_clients', 'length' => 6, 'prefix' =>date('y')]);
        });
    }
}
