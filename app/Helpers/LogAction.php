<?php 
// Code within app\Helpers\Helper.php
namespace App\Helpers;

use Illuminate\Support\Facades\Schema;
use App\Models\LogsAction;
use Illuminate\Support\Facades\Auth;
use Browser;
use Request;

class LogAction
{
    public static function create($title="Log",$id_model,$model,$controller,$comment='S/C'){
        
        $log = new LogsAction();
        $log->title = $title;
        $log->id_model = $id_model;
        $log->model = $model;
        $log->controller_action = $controller;
        $log->user_id = Auth::id() ?? '';
        $log->url = Request::url();
        $log->ip = Request::ip();
        $log->comment = $comment;
        $log->browser = Browser::detect()->browserName();
        $log->save();
        
        return $log;
    }
}
