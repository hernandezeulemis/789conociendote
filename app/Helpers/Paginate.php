<?php

namespace App\Helpers;

use Illuminate\Support\Facades\Schema;

class Paginate
{
    public static function get($model, $request, $tableName = null, $with = null, $where = null)
    {
        ## Read value
        $draw = $request->get('draw');
        $start = $request->get("start");
        $rowperpage = $request->get("length"); // Rows display per page

        $columnIndex_arr = $request->get('order');
        $columnName_arr = $request->get('columns');
        $order_arr = $request->get('order');
        $search_arr = $request->get('search');

        $columnIndex = $columnIndex_arr[0]['column']; // Column index
        $columnName = $columnName_arr[$columnIndex]['data']; // Column name
        $columnSortOrder = $order_arr[0]['dir']; // asc or desc
        $searchValue = $search_arr['value']; // Search value
        //colunms name

        $colunmsName = Schema::getColumnListing($tableName);

//        $colunmsName = [];
//        foreach ($request->input("columns") as $column){
//            $colunmsName[] = $column["data"];
//        }
        // Total records
        if (gettype($model) === 'object') {
            $records = $model->orderBy($columnName, $columnSortOrder);
            if ($with) {
                $records->with($with);
            }
            if ($where) {
                $records->where($where);
            }

            $records->select($tableName . '.*');
            $records->skip($start);
            $records->where(function ($q) use ($colunmsName, $tableName, $searchValue) {
                foreach ($colunmsName as $colunm) {
                    $q->orWhere($tableName . '.' . $colunm, 'like', '%' . $searchValue . '%');
                }
            });
            $records = $records->take($rowperpage)->get();
            $totalRecords = $model->select('count(*) as allcount')->count();
            $totalRecordswithFilter = $model->select('count(*) as allcount');
            $totalRecordswithFilter->where(function ($q) use ($colunmsName, $tableName, $searchValue) {
                foreach ($colunmsName as $colunm) {
//                    $q->orWhere($tableName . '.' . $colunm, 'like', '%' . $searchValue . '%');
                    $q->orWhere( $colunm, 'like', '%' . $searchValue . '%');
                }
            });
        } else {

            $totalRecords = $model::select('count(*) as allcount')->count();
            $totalRecordswithFilter = $model::select('count(*) as allcount');
            $records = $model::orderBy($columnName, $columnSortOrder);
            if ($with) {
                $records->with($with);
            }
            if ($where) {
                $records->where($where);
            }
            $records->where(function ($q) use ($colunmsName, $tableName, $searchValue) {
                foreach ($colunmsName as $colunm) {
//                    $q->orWhere($tableName . '.' . $colunm, 'like', '%' . $searchValue . '%');
                    $q->orWhere($colunm, 'like', '%' . $searchValue . '%');
                }
            });

            $records->select($tableName . '.*');
            $records->skip($start);
            $records = $records->take($rowperpage)->get();

            $totalRecordswithFilter->where(function ($q) use ($colunmsName, $tableName, $searchValue) {
                foreach ($colunmsName as $colunm) {
                    $q->orWhere($tableName . '.' . $colunm, 'like', '%' . $searchValue . '%');
                }
            });

        }
        // Fetch records


        $response = array(
            "draw" => intval($draw),
            "iTotalRecords" => $totalRecords,
            "iTotalDisplayRecords" => $totalRecordswithFilter->count(),
            "aaData" => $records
        );

        return $response;
    }
}
