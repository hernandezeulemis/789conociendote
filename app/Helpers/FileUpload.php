<?php // Code within app\Helpers\Helper.php
namespace App\Helpers;
use Request;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image as Image;
use Illuminate\Support\Facades\File;

class FileUpload
{
    public static function create($files,$folder = ''){
        
        $imgs= array();
        $default_folder = '/uploads/';
        $path = public_path() . $default_folder . $folder ;
        if(isset($files)){
            foreach($files as $file){
                dd($file);
                $fileName = $file->getClientOriginalName();
                $file->move($path, $fileName);
    
                $path_img = $default_folder . $folder . '/' . $fileName;
    
                array_push($imgs,$path_img);
            }
            if(count($imgs) === 1) {
                return $imgs[0];
            }
        }
        return $imgs;
    }
    public static function store($file,$folder = '')
    {   
        if (isset($file)) {
            $image = base64_encode(file_get_contents($file));
            $image_parts = explode(";base64,", $file);
            $image_type_aux = explode("image/", $image_parts[0]);
            $ext = $image_type_aux[1];
            /* $ext = $image->extension(); */
            $name = time() . "" . rand(10, 99) . "_1." . $ext;
            $data = $image_parts;
            $default_folder = '/uploads/';
            $path = public_path(). $default_folder . $folder . '/' .$name ;
            $full_path = public_path(). $default_folder . $folder;
            if (!File::exists($full_path)) {
                File::makeDirectory($full_path, 0755, true, true);
            }
            Image::make(file_get_contents($file))->save($path);
            $key = md5(base64_encode($file));
            $url = $folder == '' ? $name : $folder.'/'.$name;
            $data = [
                'url' => $url,
                'ext' => $ext,
                'mime' => $image_type_aux,
                'key' => $key
            ];
    
    
            return $data;
        }
    }

    public static function base64ToImage($file) 
    {
        $folderPath = 'storage/uploads/';
        $path = storage_path('/uploads');

        
        $image_parts = explode(";base64,", $file);

        $array1 = explode(';', $file);
        $array2 = explode('/', $array1[0]);

        $image_type = $array2[1];
        $image_base64 = base64_decode($image_parts[1]);

        $fileName = uniqid() . '.'.$image_type;
        $file = $folderPath . $fileName;

        

        Storage::disk('public')->put('uploads/' . $fileName, $image_base64);
        

        return $fileName;
    }

    public static function storeMedia($request)
    {
        $path = storage_path('app/public/uploads');

        try {
            if (!file_exists($path)) {
                mkdir($path, 0755, true);
            }
        } catch (\Exception $e) {
            dd("catch error");
        }

        $file = $request;
        $trim = trim($file->getClientOriginalName());
        
        $name = uniqid() . '_' . str_replace(' ', '', $trim);

        $file->move($path, $name);

       return   $name;
    }

}
