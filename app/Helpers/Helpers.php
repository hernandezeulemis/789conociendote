<?php

namespace App\Helpers;
use Config;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;

class Helpers
{

    /**
     * Display the specified resource.
     *
     * @param int $e
     * @param int $request
     * @return $referenceId
    */

    public static function logError($e, $request = null)
    {
        $referenceId = time() . "-" . rand(1000, 9999);

        Log::info('__START_ERROR: ' . $referenceId);
        Log::info('EXCEPTION:' . $referenceId);
        report($e);
        if ($request) {
            Log::info('REQUEST:' . $referenceId);
            Log::info($request->all());
        }
        if (!Auth::guest()) {
            Log::info('USER:' . $referenceId);
            Log::info(auth()->user()->toArray());
        }
        Log::info('__END_ERROR: ' . $referenceId);

        return $referenceId;
    }



    /**
     * Display the specified resource.
     *
     * @param int $meter_type
     * @param int $field
     * @param int $data
     * @return $include
    */
    public static function getMeterTypesIncluded($meter_type, $field, $data)
    {
        $included = 0;
        foreach ($data as $option) {
            if ($option->meter_type == $meter_type) {
                $included = $option->{$field} ?? 0;
                break;
            }
        }

        return $included;
    }
}
