<?php

namespace App\Http\Middleware;

use Closure;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Providers\RouteServiceProvider;

class CheckSignature
{
    public function handle(Request $request, Closure $next)
    {
        $user = Auth::user();
        //dd($user->roles[0]->name === 'therapist' && !$user->has_signature, $user->roles[0], $user->has_signature);

        if ($user->roles[0]->name === 'therapist' && !$user->has_signature) {
           
            return redirect(RouteServiceProvider::CONTRATO);
            //return redirect()->route('firmar_contrato');CONTRATO
        }

        return $next($request);
    }
}
