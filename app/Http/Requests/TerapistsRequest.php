<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class TerapistsRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true; // Cambia esto según tus reglas de autorización
    }

    public function rules()
    {
        return [
            'full_name' => 'nullable|string|max:150',
            'email' => 'required|string|max:150',
            'birthdate' => 'required|date',
            'sexo' => 'required|in:m,f',
            'lic_psicologia' => 'nullable|string|max:5',
            'clinical_specialty_id' => 'nullable|exists:clinical_specialties,id',
            'semester' => 'nullable|string|max:150',
            'cedula' => 'nullable|string|max:150',
            'current_principal_id' => 'nullable|exists:current_principals,id',
            'current_complementary_id' => 'nullable|exists:current_complementaries,id',
            'specific_problem_id' => 'nullable|exists:specific_problems,id',
            'clinical_specialty_id' => 'nullable|exists:clinical_specialties,id',
            'poblacion' => 'required|string|max:250',
            'language_id' => 'nullable|exists:languages,id',
            'modality' => 'required|string|max:150',
            'address' => 'required|string|max:250',
            'cod_postal' => 'required|string|max:150',
            'city' => 'required|string|max:150',
            'delegation' => 'required|string|max:250',
            'colonia' => 'required|string|max:250',
            'foto' => 'required|string|max:250',
            'titulo' => 'required|string|max:250',
        ];
    }
}
