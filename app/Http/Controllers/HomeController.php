<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\LogsAction;
use App\Models\FeedbackClients;
use App\Helpers\Paginate;
use App\Helpers\FileUpload;
use App\Mail\FeedBackEmailService;
use Illuminate\Support\Facades\Mail;




class HomeController extends Controller
{
    protected $_title = "App";

    public function __construct()
    {
        parent::__construct();
        $this->middleware('role:support|admin')->only(['getFeedBackClient']);
        $this->middleware('role:admin')->only(['logsActions']);

        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('home');
    }

    public function logsActions(Request $request)
    {
        
        $logsActions = LogsAction::orderBy('id','asc')->get();
        return view('logs.index', compact('logsActions'));
    }

    public function feedBackClient(Request $request)
    {
        $img = FileUpload::store($request->img64,'feedback');
        
        $feedBack = new FeedbackClients();
        $feedBack->img = $img['url'];
        $feedBack->comment = strlen($request->comment) == 0  ? 'S/M' : $request->comment;
        $feedBack->app_cod = $request->debugData['application']['codeName'];
        $feedBack->app_coo = $request->debugData['application']['cookie'];
        $feedBack->app_jav = $request->debugData['application']['java'];
        $feedBack->app_lan = $request->debugData['application']['language'];
        $feedBack->app_nam = $request->debugData['application']['name'];
        $feedBack->app_ver = $request->debugData['application']['version'];
        $feedBack->cli_min = $request->debugData['client']['mimeTypes'];
        $feedBack->cli_pla = $request->debugData['client']['platform'];
        $feedBack->cli_plu = $request->debugData['client']['plugins'];
        $feedBack->cli_use = $request->debugData['client']['userAgent'];
        $feedBack->glo_url = $request->debugData['global']['url'];
        $feedBack->save();
        $emails_support = env('EMAILS_SUPPORT');
        $array_emails = explode(',',$emails_support);
        for($i = 0; $i < count($array_emails); $i++){
            $email = $array_emails[$i];
            Mail::to($email)->send(new FeedBackEmailService($feedBack));
        }
        return response()->json($feedBack);
    }

    public function getFeedBackClient(Request $request) {
        $feedbacks = FeedbackClients::orderBy('id','asc')->get();
        return view('feedback.index', compact('feedbacks'));
    }
    public function docs(Request $request) {
        return view('docs.docs');
    }

}
