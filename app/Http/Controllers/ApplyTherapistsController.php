<?php

namespace App\Http\Controllers;

use App\Models\Degree;
use App\Helpers\Helpers;
use App\Helpers\Paginate;
use App\Models\Languague;
use App\Models\Poblation;
use App\Models\Therapist;
use App\Models\DegreeFile;
use App\Helpers\BaseHelper;
use App\Helpers\FileUpload;
use App\Models\AddresClinic;
use Illuminate\Http\Request;
use App\Models\SpecificProblem;
use App\Models\CurrentPrincipal;
use App\Models\ClinicalSpecialty;
use App\Http\Controllers\Controller;
use App\Models\CurrentComplementary;
use App\Http\Requests\TerapistsRequest;

class ApplyTherapistsController extends Controller
{

    protected $_table = "therapists";
    protected $_module = "apply_therapists";
    protected $_model = Therapist::class;
    protected $_title = "Postulación de Terapeutas";

    public function __construct()
    {
        parent::__construct();

    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request)
    {
        if (\request()->ajax()) {
            
            $paginate = Paginate::get($this->_model, $request, $this->_table);
            return response()->json($paginate);
        }
        $clinical_specialties       = ClinicalSpecialty::all()->pluck('name','id')->toArray();
        $current_principals         = CurrentPrincipal::all()->pluck('name','id')->toArray(); 
        $current_complementaries    = CurrentComplementary::all()->pluck('name','id')->toArray();
        $specific_problems          = SpecificProblem::all()->pluck('name','id')->toArray();
        $language                   = Languague::all()->pluck('name','id')->toArray();
        $poblation                  = Poblation::all()->pluck('name','id')->toArray();

        return view(strtolower($this->_module . "." . __FUNCTION__),compact('clinical_specialties','current_principals','current_complementaries','specific_problems','language','poblation'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        $obj = new $this->_model();
        return view(strtolower("modules." . $this->_module . "." . __FUNCTION__), compact('obj'));
    }

    protected function _getValidations($action = null, $id = null)
    {
        return [];

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       try {
            $existingUser = Therapist::where('email', $request->email)->first();

            if ($existingUser) {
                return response()->json([
                                        'list'    => $existingUser,
                                        'success' => false,
                                        'message' => 'El email ya está registrado.'
                                ]);
            }


            if($request->hasFile('avatar')){
                    
                 $pathFoto = FileUpload::storeMedia($request->file('fileFoto'), 'therapists');
                 $request['foto'] = $pathFoto;
                
            }
            
            $obj = $this->_model::create($request->except(['specific_problem_id','poblacion','language_id','current_complementary_id','titulos','avatar','fileFoto','address','cod_postal','city','delegation','colonia']));
               
            $therapeut = Therapist::find($obj->id);
            $therapeut->specificProblems()->attach($request['specific_problem_id']);
            $therapeut->therapistPoblations()->attach($request['poblacion']);
            $therapeut->therapistCurrentComplementaries()->attach($request['current_complementary_id']);
            $therapeut->therapistLanguages()->attach($request['language_id']);


            $titulos = $request->titulos;
            
            if($titulos){
                foreach ($titulos as $titulo) {

                    $licenciatura   = $titulo['licenciatura'];
                    $institucion    = $titulo['institucion'];
                    $archivos       = $titulo['file'];

                    $degree = new Degree();
                    $degree->licenciatura   = $licenciatura;
                    $degree->institucion    = $institucion;
                    $degree->therapist_id   = $therapeut->id;
                    $degree->save();
                    

                    foreach ($archivos as $archivo) {

                        $degreeFile = new DegreeFile();
                        $degreeFile->file_name = $archivo->getClientOriginalName();
                        $degreeFile->file_path = $archivo->store('degree_files');
                        $degreeFile->degree_id = $degree->id;
                        $degreeFile->save();

                        $path = storage_path('storage/uploads/therapists');
                        $trim = trim($archivo->getClientOriginalName());
                        $name = uniqid() . '_' . str_replace(' ', '', $trim);
                        $archivo->move($path, $name);
                    }

                }
            }
            
             $address = [
                "address" => $request->address,
                "cod_postal" => $request->cod_postal,
                "city" =>$request->city,
                "delegation" =>$request->delegation,
                "colonia" => $request->colonia
            ];
        
            $combinedArray = [];
            $filteredAddress = array_filter($address); // Eliminar valores null
            
            if (is_array($filteredAddress)) {
                if (count($filteredAddress) > 0) {
                    $maxCount = max(array_map('count', $filteredAddress));
                }else{
                    $maxCount = 0;
                }   
                
            } else {
                $maxCount = 0;
            }

            for ($i = 0; $i < $maxCount; $i++) {
                $tempArray = [];
                foreach ($address as $key => $value) {
                    $tempArray[$key] = isset($value[$i]) ? $value[$i] : null;
                }
                $combinedArray[] = $tempArray;
            }


            //$deleteaddrresbyid = AddresClinic::where('therapist_id',$obj->id)->delete();
            
            if (count($combinedArray) > 0) {
                foreach($combinedArray as $index => $data){
                    $addressclinic = new AddresClinic();
                    $addressclinic->therapist_id = $obj->id;
                    $addressclinic->address      = $data['address'] ;
                    $addressclinic->cod_postal   = $data['cod_postal'];
                    $addressclinic->city         = $data['city'];
                    $addressclinic->delegation   = $data['delegation'];
                    $addressclinic->colonia      = $data['colonia'];
                    $addressclinic->save();  
                }
            }
              
        } catch (\Exception $e) {

            $logId = Helpers::logError($e, $request);
            return back()->withErrors(['error' => __($this->_ERROR_MSG) . " ($logId): " . $e->getMessage()]);
        }
        
        return response()->json([
                'list'    => $obj,
                'success' => true,
                'message' => 'guardado con exito'
        ]);

    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $obj = $this->_model::findOrFail($id);
        return view(strtolower("modules." . $this->_module . "." . __FUNCTION__), compact('obj'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $obj = $this->_model::findOrFail($id);
        return view(strtolower("modules." . $this->_module . "." . __FUNCTION__), compact('obj'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, $this->_getValidations('update', $id));

        try {
            $all = $request->all();
            $obj = $this->_model::find($id)->fill($all);

            $obj->save();
        } catch (\Exception $e) {
            $logId = Helpers::logError($e, $request);
            return back()->withErrors(['error' => __($this->_ERROR_MSG) . " ($logId): " . $e->getMessage()]);
        }

        return redirect($this->_module)->with('success', __($this->_SUCCESS_MSG));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $obj = $this->_model::findOrFail($id)->delete();

        } catch (\Exception $e) {
            $logId = Helpers::logError($e, \request());
            return back()->withErrors(['error' => __($this->_ERROR_MSG) . " ($logId): " . $e->getMessage()]);
        }
        return back()->with('success', __($this->_SUCCESS_MSG));
    }

    public function verificarEmail(Request $request)
    {
        $existingUser = Therapist::where('email', $request->email)->first();

        if ($existingUser) {
            return response()->json([
                                    'list'    => $existingUser,
                                    'success' => false,
                                    'message' => 'El email ya está registrado.'
                            ]);
        }else{
            return response()->json([
                                    'list'    => $existingUser,
                                    'success' => true,
                                    //'message' => 'El email ya está registrado.'
                            ]);
        }


    }
}
