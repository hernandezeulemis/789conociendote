<?php

namespace App\Http\Controllers\Api;
use App\Helpers\Helper;
use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\View;

class CrudDefaultController extends Controller
{
    protected $_model = User::class;

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $query = $this->_model::query();

        return $query->get();
    }


    protected function _getValidations($action = null)
    {
        return [];
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, $this->_getValidations('store'));
        try {
            $all = $request->all();
            $obj = $this->_model::create($all);
        } catch (\Exception $e) {
            $logId = Helper::logError($e, $request);
            return response(['error' => __($this->_ERROR_MSG) . " ($logId): " . $e->getMessage()], 500);
        }
        return $obj;
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $obj = $this->_model::findOrFail($id);
        return $obj;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, $this->_getValidations('update'));
        try {
            $all = $request->all();
            $obj = $this->_model::find($id)->fill($all);
            $obj->save();
        } catch (\Exception $e) {
            $logId = Helper::logError($e, $request);
            return response(['error' => __($this->_ERROR_MSG) . " ($logId): " . $e->getMessage()], 500);
        }
        return $obj;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $obj = $this->_model::findOrFail($id)->delete();
        } catch (\Exception $e) {
            $logId = Helper::logError($e, \request());
            return back()->withErrors(['error' => __($this->_ERROR_MSG) . " ($logId): " . $e->getMessage()]);
        }
        return response()->json(['message'=>'Eliminado con éxito']);
    }
}
