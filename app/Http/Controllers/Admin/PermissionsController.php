<?php

namespace App\Http\Controllers\Admin;

use App\Helpers\BaseHelper;
use App\Helpers\Paginate;
use App\Http\Controllers\Controller;
use Spatie\Permission\Models\Permission;
use Illuminate\Http\Request;

class PermissionsController extends Controller
{

    protected $_table = "permissions";
    protected $_module = "admin/permissions";
    protected $_model = Permission::class;
    protected $_title = "Permisos";

    public function __construct()
    {
        parent::__construct();
        $this->middleware('can:crear permisos')->only(['store','create']);
        $this->middleware('can:editar permisos')->only(['edit','update']);
        $this->middleware('can:ver permisos')->only(['show']);
        $this->middleware('can:eliminar permisos')->only(['destroy']);
        //$this->middleware('role:admin')->only(['index']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        if (\request()->ajax()) {
            return Paginate::get($this->_model, $request, $this->_table);
        }
        return view(strtolower("modules." . $this->_module . "." . __FUNCTION__));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        $obj = new $this->_model();
        return view(strtolower("modules." . $this->_module . "." . __FUNCTION__), compact('obj'));
    }

    protected function _getValidations($action = null)
    {

        return [
            "name" => "required|min:3",
        ];
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $this->validate($request, $this->_getValidations('store'));

        try {
            $all = $request->all();

            $obj = $this->_model::create($all);
        } catch (\Exception $e) {
            $logId = Helper::logError($e, $request);
            return back()->withErrors(['error' => __($this->_ERROR_MSG) . " ($logId): " . $e->getMessage()]);
        }

        return redirect($this->_module)->with('success', __($this->_SUCCESS_MSG));

    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $obj = $this->_model::findOrFail($id);
        return view(strtolower("modules." . $this->_module . "." . __FUNCTION__), compact('obj'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $obj = $this->_model::findOrFail($id);
        return view(strtolower("modules." . $this->_module . "." . __FUNCTION__), compact('obj'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, $this->_getValidations('update'));

        try {
            $all = $request->all();
            $obj = $this->_model::find($id)->fill($all);

            $obj->save();
        } catch (\Exception $e) {
            $logId = Helper::logError($e, $request);
            return back()->withErrors(['error' => __($this->_ERROR_MSG) . " ($logId): " . $e->getMessage()]);
        }

        return redirect($this->_module)->with('success', __($this->_SUCCESS_MSG));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $obj = $this->_model::findOrFail($id)->delete();

        } catch (\Exception $e) {
            $logId = Helper::logError($e, \request());
            return back()->withErrors(['error' => __($this->_ERROR_MSG) . " ($logId): " . $e->getMessage()]);
        }
        return back()->with('success', __($this->_SUCCESS_MSG));
    }
}
