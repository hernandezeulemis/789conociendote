<?php

namespace App\Http\Controllers\Admin;

use App\User;
use App\Models\Quotes;
use App\Helpers\Helpers;
use App\Models\Language;
use App\Helpers\Paginate;
use App\Models\Languague;
use App\Models\Poblation;
use App\Models\Therapist;
use App\Helpers\BaseHelper;
use App\Models\AddresClinic;
use Illuminate\Http\Request;
use App\Models\TherapistNote;
use App\Models\SpecificProblem;

use App\Models\CurrentPrincipal;
use App\Models\ClinicalSpecialty;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Http\Traits\TherapistsTrait;
use App\Models\CurrentComplementary;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Storage;

class TherapistsController extends Controller
{
    use TherapistsTrait;

    protected $_table = "therapists";
    protected $_module = "admin/therapists";
    protected $_model = Therapist::class;
    protected $_title = "Terapeutas";

    public function __construct()
    {
        parent::__construct();
        $this->middleware('can:crear terapeutas')->only(['store','create']);
        $this->middleware('can:editar terapeutas')->only(['edit','update']);
        $this->middleware('can:ver terapeutas')->only(['show']);
        $this->middleware('can:eliminar terapeutas')->only(['destroy']);
        $this->middleware('role:rrhh')->only(['index']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request)
    {
        /* if (\request()->ajax()) {
            
            $paginate = Paginate::get($this->_model, $request, $this->_table);
            
            
        } */
        return view(strtolower("modules." . $this->_module . "." . __FUNCTION__));
    }

    public function getData ($filter = null)
    {
 
        if($filter == 'all'){
            $therapist = Therapist::with(['user'])->whereNotNull('user_terapist_id')->where('status','de_alta')->orderBy('id', 'DESC')->get();    
            
        }else{

            $therapist = Therapist::with(['user'])->where('status_therapist', $filter)->orderBy('id', 'DESC')->get();
        }
       
        return response()->json(['list' => $therapist]);
    }
    
    public function create()
    {

        $obj = new $this->_model();
        return view(strtolower("modules." . $this->_module . "." . __FUNCTION__), compact('obj'));
    }

    protected function _getValidations($action = null, $id = null)
    {
        return [];

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
 
        $this->validate($request, $this->_getValidations('store'));

        try {
            $obj = $this->_model::create($request->all());

        } catch (\Exception $e) {

            $logId = Helpers::logError($e, $request);
            return back()->withErrors(['error' => __($this->_ERROR_MSG) . " ($logId): " . $e->getMessage()]);
        }
        return redirect($this->_module)->with('success', __($this->_SUCCESS_MSG));

    }

  
    public function show($id)
    {
         $obj = $this->_model::with(['clinicalSpecialty','currentPrincipal','specificProblems'])->findOrFail($id);

        $specificProblems           = $obj->specificProblems->pluck( 'id')->toArray();
        $specific_problems          = SpecificProblem::all()->pluck('name','id')->toArray();
        $clinical_specialties       = ClinicalSpecialty::all()->pluck('name','id')->toArray();
        $current_principals         = CurrentPrincipal::all()->pluck('name','id')->toArray(); 
        $current_complementaries    = CurrentComplementary::all()->pluck('name','id')->toArray();
        $current_complementary      = $obj->therapistCurrentComplementaries->pluck( 'id')->toArray();
        $languages                  = Languague::all()->pluck('name','id')->toArray();
        $language                   = $obj->therapistLanguages->pluck( 'id')->toArray();
        $poblation                  = $obj->therapistPoblations->pluck( 'id')->toArray();
        $poblations                 = Poblation::all()->pluck('name','id')->toArray();
        $degree                     = $obj->degrees()->with('degreeFiles')->get();
        $diasSemana = [
                        'Lunes' => [],
                        'Martes' => [],
                        'Miércoles' => [],
                        'Jueves' => [],
                        'Viernes' => [],
                        'Sábado' => [],
                        'Domingo' => []
                    ];

        // Obtén los registros de therapist_schedules para el terapeuta actual
        $schedules = DB::table('therapist_schedules')
            ->where('therapist_id', $obj->id)
            ->get();

        foreach ($schedules as $schedule) {
            $dayOfWeek = $schedule->day_of_week;

            $timeslots = DB::table('therapist_schedule_timeslots')
                ->where('therapist_schedule_id', $schedule->id)
                ->get();

            $intervalos = [];
            foreach ($timeslots as $timeslot) {
                $intervalos[] = [
                    'start_time' => $timeslot->start_time,
                    'end_time' => $timeslot->end_time,
                    'modality'   => $timeslot->modality
                ];
            }

            $diasSemana[$dayOfWeek] = $intervalos;
        }
        foreach($diasSemana as $dia => $intervalos ){
            foreach($intervalos as $index => $intervalo ){
                
            }
        }

        $user = Auth::user();
        $addressclinic = AddresClinic::where('therapist_id',$id)->get();
        $nameview = 'TERAPEUTA';

        return view(strtolower("modules." . $this->_module . "." . __FUNCTION__),compact('obj','specificProblems','clinical_specialties','current_principals',
                                                                                        'current_complementaries','specific_problems','language','languages',
                                                                                        'poblations','poblation','current_complementary','degree', 'diasSemana', 'user',
                                                                                        'addressclinic','nameview','intervalos'));
    }

    
    public function edit($id)
    {
        $obj = $this->_model::with(['clinicalSpecialty','currentPrincipal','specificProblems'])->findOrFail($id);

        $specificProblems           = $obj->specificProblems->pluck( 'id')->toArray();
        $specific_problems          = SpecificProblem::all()->pluck('name','id')->toArray();
        $clinical_specialties       = ClinicalSpecialty::all()->pluck('name','id')->toArray();
        $current_principals         = CurrentPrincipal::all()->pluck('name','id')->toArray(); 
        $current_complementaries    = CurrentComplementary::all()->pluck('name','id')->toArray();
        $current_complementary      = $obj->therapistCurrentComplementaries->pluck( 'id')->toArray();
        $languages                  = Languague::all()->pluck('name','id')->toArray();
        $language                   = $obj->therapistLanguages->pluck( 'id')->toArray();
        
        $poblation                  = $obj->therapistPoblations->pluck( 'id')->toArray();
        $poblations                 = Poblation::all()->pluck('name','id')->toArray();
        $degree                     = $obj->degrees()->with('degreeFiles')->get();
        $diasSemana = [
                        'Lunes' => [],
                        'Martes' => [],
                        'Miércoles' => [],
                        'Jueves' => [],
                        'Viernes' => [],
                        'Sábado' => [],
                        'Domingo' => []
                    ];

        // Obtén los registros de therapist_schedules para el terapeuta actual
        $schedules = DB::table('therapist_schedules')
            ->where('therapist_id', $obj->id)
            ->get();

        $intervalos = [];
        
        foreach ($schedules as $schedule) {
            $dayOfWeek = $schedule->day_of_week;

            $timeslots = DB::table('therapist_schedule_timeslots')
                ->where('therapist_schedule_id', $schedule->id)
                ->get();

            
            foreach ($timeslots as $timeslot) {
                $intervalos[] = [
                    'start_time' => $timeslot->start_time,
                    'end_time' => $timeslot->end_time,
                    'modality'   => $timeslot->modality
                ];
            }

            $diasSemana[$dayOfWeek] = $intervalos;
        }
        $user = Auth::user();
        $addressclinic = AddresClinic::where('therapist_id', $obj->id)->get();
        $nameview = 'TERAPEUTA';

        return view(strtolower("modules." . $this->_module . "." . __FUNCTION__),compact('obj','specificProblems','clinical_specialties','current_principals',
                                                                                        'current_complementaries','specific_problems','language','languages',
                                                                                        'poblations','poblation','current_complementary','degree','diasSemana', 'user',
                                                                                        'addressclinic','nameview','intervalos'));

    }

    
    public function update(Request $request, $id)
    { 
       

        try {
            $therapist =  Therapist::findOrFail($id);
            $this->updateTherapists($request, $therapist);

        } catch (\Exception $e) {
            $logId = Helpers::logError($e, $request);
            return back()->withErrors(['error' => __($this->_ERROR_MSG) . " ($logId): " . $e->getMessage()]);
        }

        return redirect($this->_module)->with('success', __($this->_SUCCESS_MSG));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $obj = $this->_model::findOrFail($id)->delete();

        } catch (\Exception $e) {
            $logId = Helpers::logError($e, \request());
            return back()->withErrors(['error' => __($this->_ERROR_MSG) . " ($logId): " . $e->getMessage()]);
        }
        return back()->with('success', __($this->_SUCCESS_MSG));
    }

    public function changeStatus(Request $request)
    {
        $obj = Therapist::find($request->id);
        $obj->status_therapist = $request->status;
        $obj->save();

        return response()->json(['list' => $obj]);
     
    }
    

  /*   public function saveNota(Request $request)
    {
        $therapist = Therapist::find($request->id);
        $therapist->note = $request->mensaje;
        $therapist->save();
        
        return response()->json(['success' => true]);
    } */

     public function saveNota(Request $request)
    {
        $therapist = TherapistNote::create($request->all());
        $therapist->save();
        
        return response()->json(['success' => true]);
    }

    public function getNoteForTherapist($id)
    {
        $notes = TherapistNote::where('therapist_id',$id)->get();
        return response()->json(['list' => $notes]);
       
    }

   
}
