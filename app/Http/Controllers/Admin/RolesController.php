<?php

namespace App\Http\Controllers\Admin;

use App\Helpers\BaseHelper;
use App\Helpers\Paginate;
use App\Http\Controllers\Controller;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use Illuminate\Http\Request;


class RolesController extends Controller
{

    protected $_table = "roles";
    protected $_module = "admin/roles";
    protected $_model = Role::class;
    protected $_model2 = Permission::class;
    protected $_title = "Roles";

    public function __construct()
    {
        parent::__construct();
        
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        if (\request()->ajax()) {
            $model = $this->_model::with('permissions');//where('name','!=','support')->
            return Paginate::get($model, $request, $this->_table);
        }
        return view(strtolower("modules." . $this->_module . "." . __FUNCTION__));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        $obj = new $this->_model();
        $permissions = $this->_model2::all();
        return view(strtolower("modules." . $this->_module . "." . __FUNCTION__), compact('obj','permissions'));
    }

    protected function _getValidations($action = null)
    {

        return [
            "name" => "required|min:3",
        ];
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $this->validate($request, $this->_getValidations('store'));

        try {
            $obj = $this->_model::create(['name' => $request->name]);
            $obj->syncPermissions($request->names);
        } catch (\Exception $e) {
            $logId = Helper::logError($e, $request);
            return back()->withErrors(['error' => __($this->_ERROR_MSG) . " ($logId): " . $e->getMessage()]);
        }

        return redirect($this->_module)->with('success', __($this->_SUCCESS_MSG));

    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $obj = $this->_model::findOrFail($id);
        return view(strtolower("modules." . $this->_module . "." . __FUNCTION__), compact('obj'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $obj = $this->_model::with('permissions')->findOrFail($id);
        $permissions = $this->_model2::all();
        return view(strtolower("modules." . $this->_module . "." . __FUNCTION__), compact('obj','permissions'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, $this->_getValidations('update'));

        try {
            $obj = $this->_model::find($id);
            $obj->name = $request->name;
            $obj->save();
            $obj->syncPermissions($request->names);

        } catch (\Exception $e) {
            $logId = Helper::logError($e, $request);
            return back()->withErrors(['error' => __($this->_ERROR_MSG) . " ($logId): " . $e->getMessage()]);
        }

        return redirect($this->_module)->with('success', __($this->_SUCCESS_MSG));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $obj = $this->_model::findOrFail($id)->delete();

        } catch (\Exception $e) {
            $logId = Helper::logError($e, \request());
            return back()->withErrors(['error' => __($this->_ERROR_MSG) . " ($logId): " . $e->getMessage()]);
        }
        return back()->with('success', __($this->_SUCCESS_MSG));
    }
}
