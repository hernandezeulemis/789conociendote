<?php

namespace App\Http\Controllers\Admin;


use Carbon\Carbon;
use App\Models\User;
use App\Helpers\FileUpload;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;

class ContractController extends Controller
{
    public function processContract(Request $request)
    {
        $user = Auth::user();

        $user->has_signature = true;
        //$user->status_sign = 'firmado';
        $user->save();


        return Redirect::route('profileshow');
    }

    public function showContract()
    {
        $user = User::getCurrent();
        $fechaActual = Carbon::now();
        $year = $fechaActual->year;
        $mes = $fechaActual->month;
        $dia = (strlen($fechaActual->day) == 1) ? '0'.$fechaActual->day : $fechaActual->day ;
        $date = $dia.'/'.$mes.'/'.$year;
    
 
 		$data = [
            'header' => 'Mi título',
            'footer' => 'Mi footer',
            'users'  => $user,
            'fecha'  => $date,
            'therapist' => $user->therapist
        ];
        
        return view('contract.show', compact('data'));
    }

    public function addFirma(Request $request)
    {
        $file = FileUpload::store($request->firma, 'firmas');  
        $user = Auth::user();

        $user->has_signature = true;
        $user->status_sign = 'firmado';
        $user->signature_path = $file["url"];
        $user->save();
   
  

        return response()->json($user);
        
    }
}
