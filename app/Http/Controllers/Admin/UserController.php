<?php

namespace App\Http\Controllers\Admin;

use Browser;
use App\Models\User;
use App\Models\Language;
use App\Helpers\Paginate;
use App\Models\Languague;
use App\Models\Poblation;
use App\Models\Therapist;
use App\Helpers\LogAction;
use App\Models\LogsAction;
use App\Helpers\BaseHelper;
use App\Helpers\FileUpload;
use Illuminate\Http\Request;
use App\Models\SpecificProblem;
use App\Models\CurrentPrincipal;
use App\Models\ClinicalSpecialty;
use Barryvdh\DomPDF\Facade as PDF;
use Spatie\Permission\Models\Role;
use Illuminate\Support\Facades\Log;
use App\Http\Controllers\Controller;
use App\Models\CurrentComplementary;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\View;
use Yajra\DataTables\Utilities\Helper;

class UserController extends Controller
{

    protected $_table = "mx789_users";
    protected $_module = "admin/users";
    protected $_model = User::class;
    protected $_model2 = Role::class;
    protected $_title = "Usuarios";

    public function __construct()
    {
        parent::__construct();
       
        /* $this->middleware('can:crear usuarios')->only(['store','create']);
        $this->middleware('can:editar usuarios')->only(['edit','update']);
        $this->middleware('can:ver usuarios')->only(['show']);
        $this->middleware('can:eliminar usuarios')->only(['destroy']);
        $this->middleware('role:admin')->only(['index']); */

    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request)
    {   
        if (\request()->ajax()) {
            $paginate = Paginate::get($this->_model, $request,$this->_table);
            return response()->json($paginate);
        }
        return view(strtolower("modules." . $this->_module . "." . __FUNCTION__));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        $obj = new $this->_model();
        $roles = $this->_model2::with('permissions')->get();
        return view(strtolower("modules." . $this->_module . "." . __FUNCTION__), compact('obj','roles'));
    }

    protected function _getValidations($action = null,$id=null)
    {
        if($action == 'store'){
            return [
                'name' => ['required', 'string', 'max:255'],
                'email' => ['required', 'string', 'email', 'max:255', 'unique:mx789_users'],
                'password' => ['required', 'string', 'min:8'],
                'roles' => ['required']
            ];
        }else{
            return [
                'name' => ['required', 'string', 'max:255'],
                'email' => ['required', 'string', 'email', 'max:255', 'unique:mx789_users,email,'.$id],
                'password' => ['nullable','string', 'min:8'],
                'roles' => ['required']
            ];
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $this->validate($request, $this->_getValidations('store'));

        try {
            $imgs = FileUpload::create($request->file('file'),'users');
            $obj = $this->_model::create([
                'name' => $request->name,
                'email' => $request->email,
                'password' => Hash::make($request->password)
            ]);
            $log = LogAction::create('Registro de usuario',$obj->id,$this->_model,$request->route()->getActionMethod());

            $obj->syncRoles($request->roles);

        } catch (\Exception $e) {
            
            $logId = Helper::logError($e, $request);
            return back()->withErrors(['error' => __($this->_ERROR_MSG) . " ($logId): " . $e->getMessage()]);
        }
        if (\request()->ajax()) {
            return response()->json(['message' => $imgs]);
        }
        return redirect($this->_module)->with('success', __($this->_SUCCESS_MSG));

    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $obj = $this->_model::findOrFail($id);
        $roles = $this->_model2::with('permissions')->get();
        $logsActions = LogsAction::where('model',$this->_model)->where('id_model',$id)->orderBy('id','asc')->get();
        return view(strtolower("modules." . $this->_module . "." . __FUNCTION__), compact('obj','roles','logsActions'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $obj = $this->_model::with('roles')->findOrFail($id);
        $roles = $this->_model2::with('permissions')->get();
        return view(strtolower("modules." . $this->_module . "." . __FUNCTION__), compact('obj','roles'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, $this->_getValidations('update',$id));

        try {
            $obj = $this->_model::find($id);
            $obj->name = $request->name;
            $obj->email = $request->email;
            if($request->password != ''){
                $obj->password = Hash::make($request->password);
            }
            $obj->save();
            $obj->syncRoles($request->roles);
        } catch (\Exception $e) {
            $logId = Helper::logError($e, $request);
            return back()->withErrors(['error' => __($this->_ERROR_MSG) . " ($logId): " . $e->getMessage()]);
        }

        return redirect($this->_module)->with('success', __($this->_SUCCESS_MSG));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $obj = $this->_model::findOrFail($id)->delete();

        } catch (\Exception $e) {
            $logId = Helper::logError($e, \request());
            return back()->withErrors(['error' => __($this->_ERROR_MSG) . " ($logId): " . $e->getMessage()]);
        }
        return back()->with('success', __($this->_SUCCESS_MSG));
    }

    public function pdf() {
        $users = $this->_model::limit(20)->get();
        $data = [
            'header' => 'MI titulo',
            'footer' => 'Mi footer',
            'users' => $users
        ];
        $pdf = PDF::loadView(strtolower("modules." . $this->_module . "." . __FUNCTION__), $data);
    
        return $pdf->stream('archivo.pdf');
    }
}
