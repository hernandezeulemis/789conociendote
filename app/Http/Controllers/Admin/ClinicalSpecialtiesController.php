<?php

namespace App\Http\Controllers\Admin;

use App\Helpers\Helpers;
use App\Helpers\Paginate;
use App\Helpers\BaseHelper;
use Illuminate\Http\Request;
use App\Models\ClinicalSpecialty;
use App\Http\Controllers\Controller;

class ClinicalSpecialtiesController extends Controller
{

    protected $_table = "clinical_specialties";
    protected $_module = "admin/clinical_specialties";
    protected $_model = ClinicalSpecialty::class;
    protected $_title = "Especialidades Clinicas";

    public function __construct()
    {
        parent::__construct();

    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request)
    {
        if (\request()->ajax()) {
            
            $paginate = Paginate::get($this->_model, $request, $this->_table);
            return response()->json($paginate);
        }
        return view(strtolower("modules." . $this->_module . "." . __FUNCTION__));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        $obj = new $this->_model();
        return view(strtolower("modules." . $this->_module . "." . __FUNCTION__), compact('obj'));
    }

    protected function _getValidations($action = null, $id = null)
    {
        return [];

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $this->validate($request, $this->_getValidations('store'));

        try {
            $obj = $this->_model::create($request->all());

        } catch (\Exception $e) {

            $logId = Helpers::logError($e, $request);
            return back()->withErrors(['error' => __($this->_ERROR_MSG) . " ($logId): " . $e->getMessage()]);
        }
        return redirect($this->_module)->with('success', __($this->_SUCCESS_MSG));

    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $obj = $this->_model::findOrFail($id);
        return view(strtolower("modules." . $this->_module . "." . __FUNCTION__), compact('obj'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $obj = $this->_model::findOrFail($id);
        return view(strtolower("modules." . $this->_module . "." . __FUNCTION__), compact('obj'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, $this->_getValidations('update', $id));

        try {
            $all = $request->all();
            $obj = $this->_model::find($id)->fill($all);

            $obj->save();
        } catch (\Exception $e) {
            $logId = Helpers::logError($e, $request);
            return back()->withErrors(['error' => __($this->_ERROR_MSG) . " ($logId): " . $e->getMessage()]);
        }

        return redirect($this->_module)->with('success', __($this->_SUCCESS_MSG));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $obj = $this->_model::findOrFail($id)->delete();

        } catch (\Exception $e) {
            $logId = Helpers::logError($e, \request());
            return back()->withErrors(['error' => __($this->_ERROR_MSG) . " ($logId): " . $e->getMessage()]);
        }
        return back()->with('success', __($this->_SUCCESS_MSG));
    }
}
