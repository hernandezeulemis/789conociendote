<?php

namespace App\Http\Controllers\Admin;

use App\User;
use App\Helpers\Helpers;
use App\Helpers\Paginate;
use App\Helpers\BaseHelper;
use Illuminate\Http\Request;
use App\Models\Quotes;
use App\Http\Controllers\Controller;
use League\CommonMark\Extension\SmartPunct\Quote;

class QuotesController extends Controller
{

    protected $_table = "quotes";
    protected $_module = "admin/quotes";
    protected $_model = Quotes::class;
    protected $_title = "Listado de Citas";

    public function __construct()
    {
        parent::__construct();

    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request, $filter = null)
    {
        
        
        /* if (\request()->ajax()) {
            
            $paginate = Paginate::get($quotes, $request, $this->_table);
            return response()->json($paginate);
            
       
       // return response()->json($quotes);
        } */
        return view(strtolower("modules." . $this->_module . "." . __FUNCTION__));
    }

    public function getData($filter = null)
    {
        if($filter == 'all'){
            $quotes = Quotes::with(['therapist'])->orderBy('id', 'DESC')->get();
            
        }else{
            $quotes = Quotes::with(['therapist'])->where('status', $filter)->orderBy('id', 'DESC')->get();

        }

        return response()->json(['list' => $quotes]);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        $obj = new $this->_model();
        return view(strtolower("modules." . $this->_module . "." . __FUNCTION__), compact('obj'));
    }

    protected function _getValidations($action = null, $id = null)
    {
        return [];

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $this->validate($request, $this->_getValidations('store'));

        try {
            $obj = $this->_model::create($request->all());

        } catch (\Exception $e) {

            $logId = Helpers::logError($e, $request);
            return back()->withErrors(['error' => __($this->_ERROR_MSG) . " ($logId): " . $e->getMessage()]);
        }
        return redirect($this->_module)->with('success', __($this->_SUCCESS_MSG));

    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $obj = $this->_model::with(['therapist'])->findOrFail($id);
        
        return view(strtolower("modules." . $this->_module . "." . __FUNCTION__), compact('obj'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $obj = $this->_model::findOrFail($id);
        return view(strtolower("modules." . $this->_module . "." . __FUNCTION__), compact('obj'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, $this->_getValidations('update', $id));

        try {
            $all = $request->all();
            $obj = $this->_model::find($id)->fill($all);

            $obj->save();
        } catch (\Exception $e) {
            $logId = Helpers::logError($e, $request);
            return back()->withErrors(['error' => __($this->_ERROR_MSG) . " ($logId): " . $e->getMessage()]);
        }

        return redirect($this->_module)->with('success', __($this->_SUCCESS_MSG));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $obj = $this->_model::findOrFail($id)->delete();

        } catch (\Exception $e) {
            $logId = Helpers::logError($e, \request());
            return back()->withErrors(['error' => __($this->_ERROR_MSG) . " ($logId): " . $e->getMessage()]);
        }
        return back()->with('success', __($this->_SUCCESS_MSG));
    }

    public function changeStatus(Request $request)
    {
        $obj = Quotes::find($request->id);
        $obj->status = $request->status;
        $obj->save();

      /*   $changestatus = Quote::find($id);
        $changestatus->status = 'aceptado';
        $changestatus->save(); */

        return response()->json(['list' => $obj]);
     
    }

    public function getQuote($id)
    {
        return Quotes::with(['therapist'])->find($id);
    }

    public function saveQuotes(Request $request)
    {
        $quote = Quotes::find($request->id);
        $quote->status  = 'pendiente';//'reagendada';
        $quote->hour    = $request->hour;
        $quote->date    = $request->date;
        $quote->save();


        return response()->json([
                'list'    => $quote,
                'success' => true,
                'message' => 'Registro procesado con éxito'
            ]);
    }
}
