<?php

namespace App\Http\Controllers\Admin;

use App\Models\User;

use App\Helpers\Helpers;
use App\Models\Languague;
use App\Models\Poblation;
use App\Models\Therapist;
use App\Helpers\BaseHelper;
use App\Models\AddresClinic;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use App\Models\SpecificProblem;
use Barryvdh\DomPDF\Facade\Pdf;
use App\Models\CurrentPrincipal;
use App\Models\ClinicalSpecialty;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Http\Traits\TherapistsTrait;
use App\Models\CurrentComplementary;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class ProfileController extends Controller
{
    use TherapistsTrait;

    public function contrato()
    {
        $user = User::getCurrent();
        $fechaActual = Carbon::now();
        $year = $fechaActual->year;
        $mes = $fechaActual->month;
        $dia = (strlen($fechaActual->day) == 1) ? '0'.$fechaActual->day : $fechaActual->day ;
        $date = $dia.'/'.$mes.'/'.$year;
    
 
 		$data = [
            'header' => 'Mi título',
            'footer' => 'Mi footer',
            'users'  => $user,
            'fecha'  => $date,
            'therapist' => $user->therapist
        ];
		
        $pdf = PDF::loadView('profile.contrato', compact('data'));
    
        return $pdf->stream('archivo.pdf');
    }
    public function contratoById($id)
    {
        $therapist = Therapist::find($id);
        $user = User::find($therapist->user_terapist_id);
        $fechaActual = Carbon::now();
        $year = $fechaActual->year;
        $mes = $fechaActual->month;
        $dia = (strlen($fechaActual->day) == 1) ? '0'.$fechaActual->day : $fechaActual->day ;
        $date = $dia.'/'.$mes.'/'.$year;
    
 
 		$data = [
            'header' => 'Mi título',
            'footer' => 'Mi footer',
            'users'  => $user,
            'fecha'  => $date,
            'therapist' => ($therapist) ? $therapist : []
        ];
            
        $pdf = PDF::loadView('profile.contrato', compact('data'));
    
        return $pdf->stream('archivo.pdf');
    }

    public function profile()
    {
        $user = Auth::user();
               
        return view('modules.admin.profile.profile', compact('user'));

    }
    public function profileTherapist()
    {
        $user = Auth::user();
               
        if($user->hasRole('therapist')) {

            $obj = Therapist::with(['clinicalSpecialty','currentPrincipal','specificProblems'])->where('user_terapist_id', $user->id)->first();

            $specificProblems           = $obj->specificProblems->pluck( 'id')->toArray();
            $specific_problems          = SpecificProblem::all()->pluck('name','id')->toArray();
            $clinical_specialties       = ClinicalSpecialty::all()->pluck('name','id')->toArray();
            $current_principals         = CurrentPrincipal::all()->pluck('name','id')->toArray(); 
            $current_complementaries    = CurrentComplementary::all()->pluck('name','id')->toArray();
            $current_complementary      = $obj->therapistCurrentComplementaries->pluck( 'id')->toArray();
            $languages                  = Languague::all()->pluck('name','id')->toArray();
            $language                   = $obj->therapistLanguages->pluck( 'id')->toArray();
            $poblation                  = $obj->therapistPoblations->pluck( 'id')->toArray();
            $poblations                 = Poblation::all()->pluck('name','id')->toArray();
            $degree                     = $obj->degrees()->with('degreeFiles')->get();

            $diasSemana = [
                        'Lunes' => [],
                        'Martes' => [],
                        'Miércoles' => [],
                        'Jueves' => [],
                        'Viernes' => [],
                        'Sábado' => [],
                        'Domingo' => []
                    ];

            // Obtén los registros de therapist_schedules para el terapeuta actual
            $schedules = DB::table('therapist_schedules')
                ->where('therapist_id', $obj->id)
                ->get();
            $intervalos = [];
            foreach ($schedules as $schedule) {
                $dayOfWeek = $schedule->day_of_week;

                $timeslots = DB::table('therapist_schedule_timeslots')
                    ->where('therapist_schedule_id', $schedule->id)
                    ->get();

                
                foreach ($timeslots as $timeslot) {
                    $intervalos[] = [
                        'start_time' => $timeslot->start_time,
                        'end_time' => $timeslot->end_time,
                        'modality'   => $timeslot->modality
                    ];
                }

                $diasSemana[$dayOfWeek] = $intervalos;
            }

            $user = Auth::user();
        $addressclinic = AddresClinic::where('therapist_id', $obj->id)->get();
        $nameview = 'TERAPEUTA';
       
            return view("modules.admin.profile.therapist",compact('obj','user','specificProblems','clinical_specialties','current_principals',
                                                                    'current_complementaries','specific_problems','degree','language','languages',
                                                                    'poblations','poblation','current_complementary','diasSemana',
                                                                    'addressclinic','nameview','intervalos'));

        }

    }
    public function profileshow()
    {
         $user = Auth::user();
               
        if($user->hasRole('therapist')) {
           // $obj = Therapist::with(['clinicalSpecialty','currentPrincipal','therapistCurrentComplementaries','specificProblems','therapistLanguages'])->where('user_terapist_id', $user->id)->first();

            $obj = Therapist::with(['clinicalSpecialty','currentPrincipal','specificProblems'])->where('user_terapist_id', $user->id)->first();

            $specificProblems           = $obj->specificProblems->pluck( 'id')->toArray();
            $specific_problems          = SpecificProblem::all()->pluck('name','id')->toArray();
            $clinical_specialties       = ClinicalSpecialty::all()->pluck('name','id')->toArray();
            $current_principals         = CurrentPrincipal::all()->pluck('name','id')->toArray(); 
            $current_complementaries    = CurrentComplementary::all()->pluck('name','id')->toArray();
            $current_complementary      = $obj->therapistCurrentComplementaries->pluck( 'id')->toArray();
            $languages                  = Languague::all()->pluck('name','id')->toArray();
            $language                   = $obj->therapistLanguages->pluck( 'id')->toArray();
            $poblation                  = $obj->therapistPoblations->pluck( 'id')->toArray();
            $poblations                 = Poblation::all()->pluck('name','id')->toArray();
            $degree                     = $obj->degrees()->with('degreeFiles')->get();
            $diasSemana = [
                        'Lunes' => [],
                        'Martes' => [],
                        'Miércoles' => [],
                        'Jueves' => [],
                        'Viernes' => [],
                        'Sábado' => [],
                        'Domingo' => []
                    ];

        // Obtén los registros de therapist_schedules para el terapeuta actual
        $schedules = DB::table('therapist_schedules')
            ->where('therapist_id', $obj->id)
            ->get();
        $intervalos = [];
        foreach ($schedules as $schedule) {
            $dayOfWeek = $schedule->day_of_week;

            $timeslots = DB::table('therapist_schedule_timeslots')
                ->where('therapist_schedule_id', $schedule->id)
                ->get();

            
            foreach ($timeslots as $timeslot) {
                $intervalos[] = [
                    'start_time' => $timeslot->start_time,
                    'end_time' => $timeslot->end_time,
                    'modality'   => $timeslot->modality
                ];
            }

            $diasSemana[$dayOfWeek] = $intervalos;
        }
        foreach($diasSemana as $dia => $intervalos ){
            foreach($intervalos as $index => $intervalo ){
                
            }
        }
        
        $user = Auth::user();
        $addressclinic = AddresClinic::where('therapist_id', $obj->id)->get();
        $nameview = 'TERAPEUTA';
       
        return view("modules.admin.profile.therapistshow",compact('obj','user','specificProblems','clinical_specialties','current_principals','current_complementaries','specific_problems','degree','language','languages','poblations','poblation','current_complementary','diasSemana', 'user',
                                                                                        'addressclinic','nameview','intervalos'));
              
            //return view('modules.admin.home.therapist',compact('obj','user','specificProblems','clinical_specialties','current_principals','current_complementaries','specific_problems','language'));
        }else {
            return view('modules.admin.profile.profile', compact('user'));
        }
    }
 

    public function updateProfile(Request $request) 
    {
       try {
            $user = User::find($request->edit_user_id);
            $user->password = Hash::make($request->password);
            $user->name     = $request->name;
            $user->email    = $request->email;
            $user->save();
        } catch (\Exception $e) {
            $logId = Helpers::logError($e, $request);
            return back()->withErrors(['error' => __($this->_ERROR_MSG) . " ($logId): " . $e->getMessage()]);
        }
        //$user = $therapist->userTerapist; 

        if($user->hasRole('therapist')) {
            return redirect()->route('profile')->with(['success'=>'Perfil actualizado exitosamente.']);
            
        }else {
            return redirect()->route('profile')->with(['success'=>'Perfil actualizado exitosamente.']);
        }
        
    }

    public function updateProfileTherapist(Request $request)
    {
        try {
      
            $therapist =  Therapist::with(['user'])->where('id',$request->id)->first();
            
            $this->updateTherapists($request, $therapist);
    
        } catch (\Exception $e) {
            $logId = Helpers::logError($e, $request);
            return back()->withErrors(['error' => __($this->_ERROR_MSG) . " ($logId): " . $e->getMessage()]);
        }
        $user = $therapist->userTerapist; 

        if($user->hasRole('therapist')) {
            return redirect()->back()->with(['success' => 'Perfil actualizado exitosamente.'])->withInput();
            //return redirect()->route('profile')->with(['success'=>'Perfil actualizado exitosamente.']);
            
        }
    }
}