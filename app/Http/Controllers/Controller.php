<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Facades\View;
use App\Models\User;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    protected $_table = "";
    protected $_module = "";
    protected $_model = User::class;
    protected $_title = "";
    protected $_ERROR_MSG = "hUBO UN ERROR";
    protected $_SUCCESS_MSG = "La acción se realizó con éxito";

    function __construct()
    {
        View::share("_module", strtoupper($this->_module));
        View::share("_title", __($this->_title));
        View::share("page_title", __($this->_title));
        View::share("_module_route", ($this->_module));
        View::share("page_breadcrumbs", [
            ['page' => url($this->_module), 'title' => $this->_title]
        ]);
    }
}
