<?php
namespace App\Http\Traits;

use App\Models\Degree;
use App\Models\Languague;
use App\Models\Poblation;
use App\Models\DegreeFile;
use App\Helpers\FileUpload;
use App\Models\AddresClinic;
use Illuminate\Http\Request;
use App\Models\SpecificProblem;
use App\Models\CurrentPrincipal;
use App\Models\ClinicalSpecialty;
use App\Models\TherapistSchedule;
use Illuminate\Support\Facades\DB;
use App\Models\CurrentComplementary;
use App\Models\TherapistScheduleTimeslot;

trait TherapistsTrait
{
    public function saveFilesTrait(Request $request)
    {
        $titulos = $request->titulos;
        
        if($titulos){
            foreach ($titulos as $titulo) {

                $licenciatura   = $titulo['licenciatura'];
                $institucion    = $titulo['institucion'];
                $archivos       = $titulo['file'];
                $therapeut_id   = $titulo['therapeut_id'];

                $degree = new Degree();
                $degree->licenciatura   = $licenciatura;
                $degree->institucion    = $institucion;
                $degree->therapist_id   = $therapeut_id;
                $degree->save();
                

                foreach ($archivos as $archivo) {

                    $degreeFile = new DegreeFile();
                    $degreeFile->file_name = $archivo->getClientOriginalName();
                    $degreeFile->file_path = $archivo->store('degree_files');
                    $degreeFile->degree_id = $degree->id;
                    $degreeFile->save();

                    $path = storage_path('storage/uploads/therapists');
                    $trim = trim($archivo->getClientOriginalName());
                    $name = uniqid() . '_' . str_replace(' ', '', $trim);
                    $archivo->move($path, $name);
                }

            }
        }
            $degreeLast = Degree::with('degreeFiles')->where('id',$degree->id)->get();

            return $degreeLast;
              
    }

    public function deleteFiles($id)
    {
        $deleteDegree = Degree::where('id',$id)->delete();

        return response()->json([
            'data'    => $deleteDegree,
            'success' => true,
            'message' => 'Registro Eliminado con éxito'
        ]);
    }

    public function updateTherapists($request, $therapist)
    { //dd($request->all());
        if($request->has('avatar_64')){
                if($request->avatar_64){  
                    
                    $pathFoto = FileUpload::base64ToImage($request->avatar_64);
                    
                    $request['foto'] = $pathFoto;
                }
            }
            $therapist->update($request->except(['specific_problem_id','poblacion','language_id','current_complementary_id','titulos','avatar','fileFoto', 'titulos','address','cod_postal','city','delegation','colonia']));

            $specificProblems = $request->input('specific_problem_id', []);
            $therapist->specificProblems()->sync($specificProblems);

            $poblacion = $request->input('poblacion', []);
            $therapist->therapistPoblations()->sync($poblacion);

            $language_id = $request->input('language_id', []);
            
            $therapist->therapistLanguages()->sync($language_id);

            $current_complementary_id = $request->input('current_complementary_id', []);
            $therapist->therapistCurrentComplementaries()->sync($current_complementary_id); 
            
            $therapistId = $therapist->id;
     
            foreach ($request->timeslots as $dia => $intervalos) {
                
                $startTimes = array_filter($intervalos['start_time']);
                $endTimes = array_filter($intervalos['end_time']);
                $modalities = array_filter($intervalos['modality']);

                if (!empty($startTimes) && !empty($endTimes)) {
                    
                    $therapistSchedule = TherapistSchedule::where('therapist_id', $therapistId)->where('day_of_week', $dia)->delete();
        
                    $scheduleId = DB::table('therapist_schedules')->insertGetId([
                        'therapist_id' => $therapistId,
                        'day_of_week' => $dia,
                        'created_at' => now(),
                        'updated_at' => now(),
                    ]);

                    $startTimes  = $intervalos['start_time'];
                    $endTimes    = $intervalos['end_time'];
                    $modalities  = $intervalos['modality'];
                    foreach ($startTimes as $index => $startTime) {
                        if (!empty($startTime) && !empty($endTimes[$index])) {
                            $endTime  = $endTimes[$index];
                            $modality = $modalities[$index];
                            
                            //$therapistSchedule = TherapistScheduleTimeslot::where('therapist_id', $therapistId)->where('day_of_week', $dia)->delete();

                            DB::table('therapist_schedule_timeslots')->insert([
                                'therapist_schedule_id' => $scheduleId,
                                'start_time' => $startTime,
                                'end_time'   => $endTime,
                                'modality'   => $modality,
                                'created_at' => now(),
                                'updated_at' => now(),
                            ]);
                        }
                    }
                }
            }
             $address = [
                "address" => $request->address,
                "cod_postal" => $request->cod_postal,
                "city" =>$request->city,
                "delegation" =>$request->delegation,
                "colonia" => $request->colonia
            ];
            
            $combinedArray = [];
            $filteredAddress = array_filter($address); // Eliminar valores null
           
            if (is_array($filteredAddress)) {
                 
                if (count($filteredAddress) > 0) {
                    
                    $maxCount = max(array_map('count', $filteredAddress));
                    
                }else{
                    
                    $maxCount = 0;
                }   
                
            } else {
                $maxCount = 0;
            }

            for ($i = 0; $i < $maxCount; $i++) {
                $tempArray = [];
                foreach ($address as $key => $value) {
                    $tempArray[$key] = isset($value[$i]) ? $value[$i] : null;
                }
                $combinedArray[] = $tempArray;
            }

           
            $deleteaddrresbyid = AddresClinic::where('therapist_id',$therapist->id)->delete();
            if (count($combinedArray) > 0) {
                foreach($combinedArray as $index => $data){
                
                    $addressclinic = new AddresClinic();
                    $addressclinic->therapist_id = $therapist->id;
                    $addressclinic->address      = $data['address'] ;
                    $addressclinic->cod_postal   = $data['cod_postal'];
                    $addressclinic->city         = $data['city'];
                    $addressclinic->delegation   = $data['delegation'];
                    $addressclinic->colonia      = $data['colonia'];
                    $addressclinic->save();  
                }
            }

            return $therapist;
    }

  
    public function showTherapists($id)
    {
        $obj = $this->_model::with(['clinicalSpecialty','currentPrincipal','specificProblems'])->findOrFail($id);

        $specificProblems           = $obj->specificProblems->pluck( 'id')->toArray();
        $specific_problems          = SpecificProblem::all()->pluck('name','id')->toArray();
        $clinical_specialties       = ClinicalSpecialty::all()->pluck('name','id')->toArray();
        $current_principals         = CurrentPrincipal::all()->pluck('name','id')->toArray(); 
        $current_complementaries    = CurrentComplementary::all()->pluck('name','id')->toArray();
        $current_complementary      = $obj->therapistCurrentComplementaries->pluck( 'id')->toArray();
        $languages                  = Languague::all()->pluck('name','id')->toArray();
        $language                   = $obj->therapistLanguages->pluck( 'id')->toArray();
        $poblation                  = $obj->therapistPoblations->pluck( 'id')->toArray();
        $poblations                 = Poblation::all()->pluck('name','id')->toArray();
        $degree                     = $obj->degrees()->with('degreeFiles')->get();

        return view(strtolower("modules." . $this->_module . "." . __FUNCTION__),compact('obj','specificProblems','clinical_specialties','current_principals',
                                                                                        'current_complementaries','specific_problems','language','languages',
                                                                                        'poblations','poblation','current_complementary','degree'));

    }


}