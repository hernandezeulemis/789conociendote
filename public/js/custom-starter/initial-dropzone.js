const initialDropzone = (id,formName,idButton) => {
    Dropzone.options[id] = {
        autoDiscover: false,
        autoProcessQueue : false,
        acceptedFiles : ".png,.jpg,.gif,.bmp,.jpeg",
        uploadMultiple:true,
        
        sending: function(file, xhr, formData) {
            //se selecciona el formulario para capturar los inputs automaticamente y que se envien en el upload de los archivos
          const inputs = document.forms[formName].getElementsByTagName("input")
          for( let i = 0; i < inputs.length; i++) {
              const name = $(inputs[i]).attr("name")
              const value = $(inputs[i]).val()
              formData.append(name, value);
          }
        },
        init:function(){
          var submitButton = document.querySelector(idButton);
          myDropzone = this;
     
          submitButton.addEventListener('click', function(e){
              //se revisa si hay archivos seleccionados para que el formulario se envié correctamente
              const files = $('.dz-preview')
              if(files.length>0) {
                  e.preventDefault();
                  e.stopPropagation();
                  myDropzone.processQueue();
              }
          });
     
          this.on("complete", function(){
            if(this.getQueuedFiles().length == 0 && this.getUploadingFiles().length == 0)
            {
              var _this = this;
              _this.removeAllFiles();
              alert('Registro realizado con éxito')
            }
          });
     
        }
    };
}