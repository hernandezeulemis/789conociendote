const btnVer = (id,url,name='Ver') =>  `
<a  class="btn btn-info" href="${ url }/${ id }">
    ${ name }
</a>`

const btnEditar = (id,url,name='Editar') =>  `

<a  class="btn btn-warning" href="${ url }/${ id }/edit">
    ${name}
</a>`

const btnBorrar = (id,url,token,name="Borrar") =>  `
<a  class="btn btn-danger click-btn" id="${ id }" href="#">${name}</a>
<form action="${ url }/${ id } " method="post">
    <input type="hidden" name="_method" value="DELETE">            
    <input type="hidden" name="_token" value="${ token }">
    <a  class="delete-btn " style="display:none;" id="delete-btn-${ id }" href="#">Borrar</a>
</form>
`
$(document).on('click','.click-btn',function(){
    let val = $(this).attr('id')
    $(`#delete-btn-${val}`).trigger('click')
})

const pdf = () => {
    const table = $('.table').DataTable()
    console.log(table.rows().data())
}

const datatables = ({columns,url,excel = false, param}) => {
    const dom = excel === true ? 'Bfrtip' : 'frtip'
    const urlB =  (param != '') ? param : ''
    

    $('.table').DataTable({
        destroy: true,
        dom: dom,
        buttons: ['excel'],
        processing: true,
        serverSide: true,
        pageLength: 50,
        stateSave: false,
        responsive: true,
        //ajax: url + urlB,
        ajax: {
                url: url,
                data: {
                        status: param,
                    },
            },
        language: {
            sProcessing:    "Procesando...",
            sLengthMenu:    "Mostrar _MENU_ registros",
            sZeroRecords:   "No se encontraron resultados",
            sEmptyTable:    "Ningún dato disponible en esta tabla",
            sInfo:          "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
            sInfoEmpty:     "Mostrando registros del 0 al 0 de un total de 0 registros",
            sInfoFiltered:  "(filtrado de un total de _MAX_ registros)",
            sInfoPostFix:   "",
            sSearch:        "Buscar:",
            sUrl:           "",
            sInfoThousands:  ",",
            sLoadingRecords: "Cargando...",
            oPaginate: {
                sFirst:    "Primero",
                sLast:    "Último",
                sNext:    "Siguiente",
                sPrevious: "Anterior"
            },
            oAria: {
                sSortAscending:  ": Activar para ordenar la columna de manera ascendente",
                sSortDescending: ": Activar para ordenar la columna de manera descendente"
            }
        },
        //////////ESTO ES LO UNICO QUE SE DEFINE//////////
        columns:columns
    });
}

const canPermission = (payload) => {
    return permissionsUser.some(permission => payload.includes(permission.name))
}
const canRole = (payload) => {
    return rolesUser.some(role => payload.includes(role.name))
} 

const barChart = (id) => {
    const myChart = new Chart(
    document.getElementById(id),
        {
            type: 'bar',
            data: data,
            options: {}
        }
    );
}

