
    $(document).ready(function() {
        window.arrayCertificate = [];
        var starValue = '{{$obj->qualification}}'
        //var therapeut_id = '{{ $obj->id }}'
        var therapeut_id = $('#therapeut_id').val();


        $('.rating .star').each(function() {
            var valor = parseInt($(this).data('value')); 
            
            if (valor <= starValue) {
                $(this).addClass('gold'); 
            }
        });

        getAddress = function(val){
            var id = $('#cod_postal'+val+'').val();
            $.get({
                url: 'https://sepomex.789.com.mx/' + id,
                type: 'get',
                beforeSend: function() {
                    //$.LoadingOverlay('show');
                },
                success: function(response, statusText, xhr) {
                    
                    $('#colonia'+val+'').val(response['asentamientos'][0]);
                    $('#city'+val+'').val(response['estados'][0]);
                    $('#delegation'+val+'').val(response['municipios'][0]);

                },
                complete: function() {
                    //$.LoadingOverlay('hide');
                }
            });
        }

        $('.cod_postal').blur(function() {
            console.log("on blur");
            $.get({
                url: 'https://sepomex.789.com.mx/' + $(this).val(),
                type: 'get',
                beforeSend: function() {
                    //$.LoadingOverlay('show');
                },
                success: function(response, statusText, xhr) {
                    
                    $('#colonia').val(response['asentamientos'][0]);
                    $('#city').val(response['estados'][0]);
                    $('#delegation').val(response['municipios'][0]);
                },
                complete: function() {
                    //$.LoadingOverlay('hide');
                }
            });
        });

        $("#formCRUDEditCandidate").validate({
            errorClass: "error",
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            },
            submitHandler: function(form, event) {
                event.preventDefault();

                var formData = new FormData(form); // Crear un objeto FormData directamente desde el formulario

                
                var foto = document.getElementById('foto');
                var titulos = document.getElementById('titulos');
                formData.append('fileFoto', foto.files[0]);
                formData.append('fileTitulo', titulos.files[0]);
                console.log(arrayCertificate)

                for (let i = 0; i < arrayCertificate.length; i++) {
                    formData.append(`school[${i}][licenciatura]`, arrayCertificate[i].licenciatura);
                    formData.append(`school[${i}][institucion]`, arrayCertificate[i].institucion);

                    for (let j = 0; j < arrayCertificate[i].file.length; j++) {
                        formData.append(`school[${i}][file][${j}]`, arrayCertificate[i].file[j]);
                    }
                }


                $.ajax({
                    type: "POST",
                    url: url + "/store",
                    headers: {
                        "X-CSRF-TOKEN": CSRF_TOKEN
                    },
                    data: formData,
                    processData: false, // Deshabilitar el procesamiento automático de datos
                    contentType: false, // Deshabilitar la configuración automática del tipo de contenido
                    success: function(response) {
                        form.reset();
                        Swal.fire({
                            icon: 'success',
                            title: 'Datos guardados exitosamente',
                            showConfirmButton: false,
                            timer: 1500
                        });
                    },
                    error: function(xhr, status, error) {
                        console.log(xhr.responseText);
                    }
                });
            }
        });

        degreedelete = function(event,id) {
            event.preventDefault()
            $.get(urlBase + "/admin/candidates/delete_files/"+id, function(response) {
                if (response.success){
                    var div = document.getElementById('file_'+id);
                    div.innerHTML = '';
                    //$('#file_' + id).empty()
                    div.style.display = "none"
                    Swal.fire({
                        position: "top-end",
                        icon: "success",
                        title: "Operación Exitosa!!!",
                        showConfirmButton: false,
                        timer: 2500
                    });
                }
                
                });
    
        }

        $('#specific_problems, #current_complementaries, #poblacion, #language, .timeslots').select2();
        $('#specific_problems, #current_complementaries, #poblacion, #language').select2({
            multiple: true
        });
        $('.master_clinica').on('change',function(){
            if($(this).val() == 'otro'){
                $('#master_text').removeClass('hidden');
                $('#maestria').addClass('hidden'); 
                $('#clinical_specialties').val('')
            }else if($(this).val() == 'si'){
                $('#maestria').removeClass('hidden');
                $('#master_text').addClass('hidden'); 
                $('#master_otro').val('')
            }else{
                $('#maestria').addClass('hidden'); 
                $('#master_text').addClass('hidden'); 
                $('#clinical_specialties').val('')
            }
        })

        $('#lic_psicologia').on('change',function(){
            if($(this).val() == 'otro'){
                $('#lic_text').removeClass('hidden');
               
                
            }else if($(this).val() == 'si'){
                
                $('#lic_text').addClass('hidden'); 
               
            }else{
                
                $('#lic_text').addClass('hidden'); 
                
            }
        })
        /* codigo para agregar los archivos de licenciatura */
        var dataUpload = []
            getImgBase = function () {
   
                const _this = this
                var event = event || window.event;
                var files = event.target.files;
                var filesname = '';

                for (let i = 0; i < files.length; i++) {
                    const reader = new FileReader()
                    dataUpload.push(files[i]);
                    filesname += '<span class="btn btn-default btn-sm ml-3" style="cursor:pointer">' + files[i].name + '</span>';
                  
                }
                $('#archivos_seleccionados').html(filesname);
            }
            delImg = function (index) {console.log(index)
                dataUpload.splice(index,1);
            }
     
            $('#btnAdd').click(function() {
    
                var lic = $('#licenciatura').val()
                var int = $('#institucion').val()
        
                if(lic != '' && int != '' && dataUpload.length > 0 ){
                    var loadingOverlay = document.getElementById('loading');
                    loadingOverlay.classList.remove('hidden');
    
                    arrayCertificate.push({
                        licenciatura : lic,
                        institucion: int,
                        file:dataUpload
                    })
                    var formData = new FormData()
                
                    for (let i = 0; i < arrayCertificate.length; i++) {
                        formData.append(`titulos[${i}][licenciatura]`, arrayCertificate[i].licenciatura);
                        formData.append(`titulos[${i}][institucion]`, arrayCertificate[i].institucion);
                        formData.append(`titulos[${i}][therapeut_id]`,therapeut_id)
                        for (let j = 0; j < arrayCertificate[i].file.length; j++) {
                            formData.append(`titulos[${i}][file][${j}]`, arrayCertificate[i].file[j]);
                        }
                    }
                    $.ajax({
                        type: "POST",
                        url: urlBase + "/admin/candidates/save_files",
                        headers: {
                            "X-CSRF-TOKEN": token
                        },
                        data: formData,
                        processData: false, // Deshabilitar el procesamiento automático de datos
                        contentType: false, // Deshabilitar la configuración automática del tipo de contenido
                        success: function(response) {
                            console.log(response)
                            Swal.fire({
                                icon: 'success',
                                title: 'Datos guardados exitosamente',
                                showConfirmButton: false,
                                timer: 1500
                            });
                              // Construir el HTML
                            var html = '';
                            for (var i = 0; i < response.list.length; i++) {
                                var certificate = response.list[i];
                                var filesHtml = '';

                                // Construir el HTML para los archivos subidos
                                for (var j = 0; j < certificate.degree_files.length; j++) {
                                    var file = certificate.degree_files[j];
                                    var fileName = file.file_name;
                                    var file_path = file.url;
                                    var fileExtension = fileName.substring(fileName.lastIndexOf('.') + 1);
                                    filesHtml += '<span class="btn btn-default btn-sm ml-3">' + fileName + ' (' + fileExtension + ')</span>';
                                }

                                html  += '<div id="file_'+certificate.id+'"  class="card mb-3 card-ppal">';
                                html  += '<div  class="card-body" style="display: flex;justify-content: space-between;align-items: baseline;flex-wrap:wrap">';
                                html  += '<h5 class="card-title">' + certificate.licenciatura + '</h5>';
                                html  += '<h6 class="card-subtitle mb-2 text-muted">' + certificate.institucion + '</h6>';
                                html  += '<div class="card-text"><a href="'+file_path+'" target="_blank" class="btn btn-default btn-sm ml-3" style="cursor:pointer">' + filesHtml + '</a></div>';
                                html  += '<div class="bt_eliminar">';
                                html  += '<button class="btn btn-danger btn-sm delete-certificate" onclick="degreedelete(event,'+certificate.id+')">Eliminar</button>';
                                html  += '</div>';
                                html  += '</div>';
                                html  += '</div>';
                            }
                            loadingOverlay.classList.add('hidden');
                         
                            // Mostrar el HTML construido
                            $('#certificateList').append(html); 
                        },
                        error: function(xhr, status, error) {
                            console.log(xhr.responseText);
                        }
                    });

                    var lic = $('#licenciatura').val('')
                    var int = $('#institucion').val('')
                    var file = $('#fichero-tarifas').val('')
                    dataUpload = []
                    $('#archivos_seleccionados').empty()
                }else{
                    alert('Debe completar los campos')
                }
                
              
            

                // Manejar el evento de eliminación de elementos
                $('.delete-certificate').click(function() {
                    var index = $(this).data('index');
                    arrayCertificate.splice(index, 1);
                    $(this).closest('div').remove();
                });


            });
            

        /* fin del codigo */
        var int = 100
        $('#addAddress').click(function() {
            int++
            var html = '';

            html +='<div class="form-group fv-plugins-icon-container">';
            html +='<label class="col-6 col-form-label">Dirección de Consultorio</label>';
            html +='<textarea name="address[]" id="address" cols="30" rows="3" class="form-control form-control-solid" ></textarea>';
            html +='<span class="badge badge-danger badge-remove-address"><i style="color:white;" class="fas fa-times"></i></span>';
          
            html +='<div class="row">';
            html += '<div class="col-xl-3">';
            html += '<div class="form-group fv-plugins-icon-container">';
            html += '<label class="col-6 col-form-label">Código Postal</label>';
            html += '<input onblur="getAddress('+int+')" type="text" name="cod_postal[]" id="cod_postal'+int+'" class="form-control form-control-solid form-control-lg cod_postal"  placeholder="Código Postal">';
            html += '</div>';
            html += '</div>';
            html += '<div class="col-xl-3">';
            html += '<div class="form-group fv-plugins-icon-container">';
            html += '<label class="col-6 col-form-label">Ciudad</label>';
            html += '<input type="text" class="form-control form-control-solid form-control-lg" name="city[]" id="city'+int+'">';
            html += '</div>';
            html += '</div>';
            html += '<div class="col-xl-3">';
            html += '<div class="form-group fv-plugins-icon-container">';
            html += '<label class="col-9 col-form-label">Delegación o Municipio</label>';
            html += '<input type="text" class="form-control form-control-solid form-control-lg" name="delegation[]" id="delegation'+int+'">';
            html += '</div>';
            html += '</div>';
            html += '<div class="col-xl-3">';
            html += '<div class="form-group fv-plugins-icon-container">';
            html += '<label class="col-6 col-form-label">Colonia</label>';
            html += '<input type="text" class="form-control form-control-solid form-control-lg" name="colonia[]" id="colonia'+int+'">';
            html += '</div>';
            html += '</div>';
            html +='</div>';
            html +='</div>';
       
            $('#addressclinic').append(html); 
        });
        // Función para eliminar la sección de dirección
        function removeAddressSection(button) {
        var addressSection = button.closest('.form-group');
        addressSection.remove();
        }

        // Evento para eliminar la dirección al hacer clic en el botón
        $(document).on('click', '.badge-remove-address', function() {
            int--
            console.log(int)
        removeAddressSection(this);
        });

        var input = document.getElementById('imagenPerfilInput');
        var preview = document.getElementById('imagenPerfilPreview');
        var avatar64Input = document.getElementById('avatar_64');
        
        
        input.addEventListener('change', function(event) {
        var archivo = event.target.files[0];
            if (archivo) {
                var lector = new FileReader();

                lector.onload = function() {
                preview.src = lector.result;
                avatar64Input.value = lector.result; 
                }

                lector.readAsDataURL(archivo);
            }
        });

        const editIcon = document.querySelector('.edit-icon');
        const imagenPerfilInput = document.getElementById('imagenPerfilInput');

        editIcon.addEventListener('click', () => {
            imagenPerfilInput.click();
        });

        

        tippy('[data-tippy-content]');

        tippy('#cedula_master', {
            content: "Escribe el número de tu cédula profesional más actual",
        });

        tippy('#current_complementaries', {
            content: "Elige las opciones que correspondan con tu tipo de trabajo.",
        });

        tippy('#specific_problems', {
            content: "Elige una o varias opciones de problemáticas con las que trabajas",
        });

        tippy('#foto', {
            content: "Sube una foto de frente y de buena calidad, procura que se vea profesional, esta foto aparecerá en el perfil que le compartiremos a los paciente que refiramos contigo.",
        });

        tippy('#current_principals', {
            content: "Elige la corriente con la que trabajas principalmente",
        });

        tippy('.kt-avatar__cancel', {
            content: "Cancel avatar",
        });

        tippy('.kt-avatar__upload', {
            content: "Cargar Archivo",
        });

        tippy('kt_uppy_5_input_control', {
            content: "El tamaño máximo de archivo es 1 MB y el número máximo de archivos es 5.",
        });

        
    });
    document.addEventListener('DOMContentLoaded', function() {
            const stars = document.querySelectorAll('.star');
            stars.forEach(function(star) {
                star.addEventListener('mouseover', function() {
                //selectStars(star.dataset.value);
                });

                star.addEventListener('mouseout', function() {
                resetStars();
                });

                star.addEventListener('click', function() {
                highlightStars(star.dataset.value);
                
                });
            });
            function highlightStars(value) {
                stars.forEach(function(star) {
                if (star.dataset.value <= value) {
                    $('#qualification').val(value)
                    star.style.color = 'burlywood';
                    star.classList.add('selected');
                } else {
                    star.classList.remove('selected');
                    star.style.color = 'gray';
                }
                });
            }
            function resetStars() {
                stars.forEach(function(star) {
                star.classList.remove('selected');
                });
            }
            function selectStars(value) {
            highlightStars(value);

            }
    });

    function removerElemntHorario(button) {
        var wrapper = button.closest('.form-group');
            wrapper.remove();
    }

  
    var timeslotCount = {
        'Lunes': 0,
        'Martes': 0,
        'Miércoles': 0,
        'Jueves': 0,
        'Viernes': 0,
        'Sábado': 0,
        'Domingo': 0
    };

       function addElementHorario(dia) {
        
        var container = document.getElementById(dia);
            var index = timeslotCount[dia];

        var html = `<div  class="form-group row align-items-center" id="content-lunes">     
                            <div class="col-md-3">
                                <div class="kt-form__group--inline" style="display:none">
                                    <div class="kt-form__label">
                                        <label class="label-day">Lunes</label>
                                    </div>
                                    <div class="kt-form__control">
                                        <input type="hidden" class="form-control" placeholder="Lunes" name="day_of_week" id="lunes"> 
                                    </div>
                                </div>
                                <div class="d-md-none kt-margin-b-10"></div>
                            </div>
                            <div class="col-md-2">
                                <div class="kt-form__group--inline">
                                    <div class="kt-form__label">
                                        <label for="timeslots[${index}][start_time]">Hora de inicio: </label>
                                    </div>
                                    <div class="kt-form__control">
                                        <select name="timeslots[${dia}][start_time][]" class="form-control timeslot-start-time timeslots select2" style="opacity:1">
                                            
                                        </select>  
                                    </div>
                                </div>
                                <div class="d-md-none kt-margin-b-10"></div>
                            </div>  
                            <div class="col-md-2">
                                <div class="kt-form__group--inline">
                                    <div class="kt-form__label">
                                        <label for="timeslots[${index}][end_time]">Hora de fin:</label>
                                    </div>
                                    <div class="kt-form__control">
                                        <select name="timeslots[${dia}][end_time][]" class="form-control timeslot-end-time timeslots select2" style="opacity:1">
                                            
                                        </select>  
                                    </div>
                                </div>
                                <div class="d-md-none kt-margin-b-10"></div>
                            </div>
                            <div class="col-md-2">
                                <div class="kt-form__group--inline">
                                    <div class="kt-form__label">
                                        <label for="timeslots[${index}][modality]">Modalidad:</label>
                                    </div>
                                    <div class="kt-form__control">
                                        <select name="timeslots[${dia}][modality][]" class="form-control timeslot-modality timeslots select2" style="opacity:1">
                                            <option value="">Seleccione Modalidad</option>
                                            <option value="lineal">Lineal</option>
                                            <option value="presencial">Presencial</option>
                                            <option value="ambas">Ambas</option> 
                                        </select>  
                                    </div>
                                </div>
                                <div class="d-md-none kt-margin-b-10"></div>
                            </div>
                            <div class="col-md-3 form-group-last" style="margin-top:42px">
                                <div class="kt-form__group--inline">
                                    <div class="kt-form__control">
                                        <button type="button" class="btn btn-danger" onclick="removerElemntHorario(this)" ><i class="fa fa-trash "></i></button> 
                                    </div>
                                </div>
                                <div class="d-md-none kt-margin-b-10"></div>
                        </div>
                        </div> `;
                        console.log(timeslotCount)
        container.insertAdjacentHTML('beforeend', html); // Agregar el HTML al contenedor
        //timeslotCount++;
                        console.log(timeslotCount)
        var wrapper = container.lastElementChild; // Obtener el último elemento agregado

        var selectStart = wrapper.querySelector('select.timeslot-start-time');
        var selectEnd = wrapper.querySelector('select.timeslot-end-time');
        

        for (var hours = 0; hours < 24; hours++) {
            for (var minutes = 0; minutes < 60; minutes += 15) {
                var time = padZero(hours) + ":" + padZero(minutes);
                var optionStart = new Option(time, time);
                var optionEnd = new Option(time, time);
                selectStart.appendChild(optionStart);
                selectEnd.appendChild(optionEnd);
            
            }
        }

        timeslotCount[dia]++;
    }

    function eliminarElemento(btn) {
        var container = $(btn).closest('.col-md-3');
        container.add(container.siblings('.col-md-3')).remove();
    }
    
    function padZero(num) {
        return num < 10 ? "0" + num : num;
    }
    $('#modality').change(function(){
        var cod_postal = document.getElementById('cod_postal');
        var city = document.getElementById('city');
        var delegation = document.getElementById('delegation');
        var colonia = document.getElementById('colonia');
  
     
        console.log($('#modality').val())

        if ($('#modality').val() == 'lineal'){
            $('#filed_direction').addClass('hidden');
            $('#minimum_in_fee_lineal').removeClass('hidden');
           $('#minimum_in_fee').addClass('hidden');
            cod_postal.removeAttribute('required');
            city.removeAttribute('required');
            delegation.removeAttribute('required');
            colonia.removeAttribute('required');
      
        }else if($('#modality').val() == 'presencial'){
            $('#filed_direction').removeClass('hidden');
            $('#minimum_in_fee').removeClass('hidden');
            $('#minimum_in_fee_lineal').addClass('hidden');
       
        }else{
            $('#filed_direction').removeClass('hidden');
            $('#minimum_in_fee,#minimum_in_fee_lineal').removeClass('hidden');
            //$('#minimum_in_family').addClass('hidden');
        }
  
    });



    
