var timeslotCount = {
        'Lunes': 0,
        'Martes': 0,
        'Miércoles': 0,
        'Jueves': 0,
        'Viernes': 0,
        'Sábado': 0,
        'Domingo': 0
    };

       function addElementHorario(dia) {
        
        var container = document.getElementById(dia);
            var index = timeslotCount[dia];

        var html = `<div  class="form-group row align-items-center" id="content-lunes">     
                            <div class="col-md-3">
                                <div class="kt-form__group--inline" style="display:none">
                                    <div class="kt-form__label">
                                        <label class="label-day">Lunes</label>
                                    </div>
                                    <div class="kt-form__control">
                                        <input type="hidden" class="form-control" placeholder="Lunes" name="day_of_week" id="lunes"> 
                                    </div>
                                </div>
                                <div class="d-md-none kt-margin-b-10"></div>
                            </div>
                            <div class="col-md-3">
                                <div class="kt-form__group--inline">
                                    <div class="kt-form__label">
                                        <label for="timeslots[${index}][start_time]">Hora de inicio: </label>
                                    </div>
                                    <div class="kt-form__control">
                                        <option value="">Seleccione Intervalo</option>
                                        <select name="timeslots[${dia}][start_time][]" class="form-control timeslot-start-time timeslots select2" style="opacity:1">
                                            
                                        </select>  
                                    </div>
                                </div>
                                <div class="d-md-none kt-margin-b-10"></div>
                            </div>  
                            <div class="col-md-3">
                                <div class="kt-form__group--inline">
                                    <div class="kt-form__label">
                                        <label for="timeslots[${index}][end_time]">Hora de fin:</label>
                                    </div>
                                    <div class="kt-form__control">
                                        <option value="">Seleccione Intervalo</option>
                                        <select name="timeslots[${dia}][end_time][]" class="form-control timeslot-end-time timeslots select2" style="opacity:1">
                                            
                                        </select>  
                                    </div>
                                </div>
                                <div class="d-md-none kt-margin-b-10"></div>
                            </div>
                            <div class="col-md-3 form-group-last" style="margin-top:25px">
                                <div class="kt-form__group--inline">
                                    <div class="kt-form__control">
                                        <button type="button" class="btn btn-danger"  ><i class="fa fa-trash "></i></button> 
                                    </div>
                                </div>
                                <div class="d-md-none kt-margin-b-10"></div>
                        </div>
                        </div> `;
                        console.log(timeslotCount)
        container.insertAdjacentHTML('beforeend', html); // Agregar el HTML al contenedor
    
        var wrapper = container.lastElementChild; // Obtener el último elemento agregado

        var selectStart = wrapper.querySelector('select.timeslot-start-time');
        var selectEnd = wrapper.querySelector('select.timeslot-end-time');
        

        for (var hours = 0; hours < 24; hours++) {
            for (var minutes = 0; minutes < 60; minutes += 15) {
                var time = padZero(hours) + ":" + padZero(minutes);
                var optionStart = new Option(time, time);
                var optionEnd = new Option(time, time);
                selectStart.appendChild(optionStart);
                selectEnd.appendChild(optionEnd);
            
            }
        }

        timeslotCount[dia]++;
    }
    
    function padZero(num) {
        return num < 10 ? "0" + num : num;
    }