 <form class="form fv-plugins-bootstrap fv-plugins-framework" id="formCRUDTerapeutas">
     @csrf
     <div class="pb-5" data-wizard-type="step-content" data-wizard-state="current">
         <h3 class="mb-10 font-weight-bold text-dark">CANDIDATO</h3>
         <div style="text-align: center;">
             @if ($obj->avatar_url)
                <img id="imagenPerfilPreview" width="150" height="150" src="{{ asset($obj->avatar_url) }}" alt="Foto" style=" margin-bottom:5%">
            @else
                <div class="avatar-placeholder">
                    <img id="imagenPerfilPreview" width="150" height="150" src="{{ asset('img/avatar.png') }}" alt="Foto" style="margin-bottom:5%">
                    
                </div>
            @endif
         </div>
         <div class="row">
             <div class="col-xl-6">
                 <div class="form-group fv-plugins-icon-container">
                     <label>Nombre Completo</label>
                     <input type="text" class="form-control form-control-solid form-control-lg" name="full_name" id="full_name" placeholder="Nombre Completo" value="{{$obj->full_name}}" disabled>
                     <div class="fv-plugins-message-container"></div>
                 </div>
             </div>
             <div class="col-xl-6">
                 <div class="form-group">
                     <label>Correo</label>
                     <input type="email" class="form-control form-control-solid form-control-lg" name="email" id="email" placeholder="Correo" value="{{$obj->email}}" disabled>
                 </div>
             </div>
         </div>
         <div class="row">
             <div class="col-xl-4">
                 <div class="form-group fv-plugins-icon-container">
                     <label>Fecha de Nacimiento</label>
                     <input class="form-control date" type="date" value="{{ $obj->birthdate }}" id="" name="birthdate" disabled>
                     <span class="form-text text-muted">Agregue Fecha de Nacimiento.</span>
                     <div class="fv-plugins-message-container"></div>
                 </div>
             </div>
             <div class="col-xl-4">
                <div class="form-group fv-plugins-icon-container">
                    <label class="col-6 col-form-label">Teléfono</label>
                    <input type="text" class="form-control form-control-solid form-control-lg" name="phone" id="phone" placeholder="Teléfono" value="{{$obj->phone}}" required>
                    <div class="fv-plugins-message-container"></div>
                </div>
            </div>
             <div class="col-xl-4">
                 <div class="form-group fv-plugins-icon-container">
                     <label>Sexo</label>
                     <select name="sexo" id="sexo" class="form-control form-control-solid form-control-lg" disabled>
                         <option value="">Seleccione...</option>
                         <option value="m" {{($obj->sexo == 'm') ? 'selected' : ''  }}>Masculino</option>
                         <option value="f" {{($obj->sexo == 'f') ? 'selected' : ''  }}>Femenino</option>
                     </select>
                 </div>
                 <!--end::Input
                            
                            -->
             </div>
         </div>
         <div class="row">
             <div class="col-xl-6">
                 <!--begin::Input-->
                 <div class="form-group fv-plugins-icon-container">
                     <label>Licenciatura en Psicología</label>
                     <select name="lic_psicologia" id="lic_psicologia" class="form-control form-control-solid form-control-lg" disabled>
                         <option value="">Seleccione...</option>
                         <option value="si" {{ ($obj->lic_psicologia == 'si') ? 'selected' : '' }}>Si</option>
                         <option value="no" {{ ($obj->lic_psicologia == 'no') ? 'selected' : '' }}>No</option>
                         <option value="otro" {{ ($obj->lic_psicologia == 'otro') ? 'selected' : '' }}>Otro</option>
                     </select>
                     <div class="fv-plugins-message-container"></div>
                 </div>
                 <!--end::Input-->
             </div>
             
             <div class="col-xl-6 {{ ($obj->lic_psicologia == 'otro') ? '' : 'hidden' }}" id="lic_text">
                        <div class="form-group fv-plugins-icon-container">
                            <label class="col-6 col-form-label">Otro</label>
                            <input type="text" class="form-control form-control-solid form-control-lg" name="lic_otro" id="lic_otro" required placeholder="" value=" {{$obj->lic_otro }}">
                            <div class="fv-plugins-message-container"></div>
                        </div>
                    </div>
             <div class="col-xl-6">
                 <div class="form-group fv-plugins-icon-container">
                     <label class="col-6 col-form-label">Maestría o Especialidad<span class="require">*</span></label>
                     <select name="if_master_clinica" class="form-control form-control-solid form-control-lg master_clinica" required disabled>
                         <option value="">Seleccione...</option>
                         <option value="si" {{ ($obj->if_master_clinica == 'si') ? 'selected' : '' }}>Si</option>
                         <option value="no" {{ ($obj->if_master_clinica == 'no') ? 'selected' : '' }}>No</option>
                         <option value="otro" {{ ($obj->if_master_clinica == 'otro') ? 'selected' : '' }}>Otro</option>
                     </select>
                     <div class="fv-plugins-message-container"></div>
                 </div>
             </div>
            
             <div id="maestria" class="col-xl-6 {{ ($obj->if_master_clinica == 'si') ? '' : 'hidden' }}">
                 <!--begin::Select-->
                 <div class="form-group fv-plugins-icon-container">
                     <label class="col-6 col-form-label">Maestría o Especialidad Clínica</label>
                     <select name="clinical_specialty_id" id="clinical_specialties" class="form-control form-control-solid form-control-lg" required disabled>
                         <option value="">Seleccione...</option>
                         @foreach($clinical_specialties as $key => $text)
                         <option value="{{ $key }}" {{ $key == $obj->clinical_specialty_id   ? 'selected' : '' }}>{{ $text }}</option>
                         @endforeach
                     </select>
                     <div class="fv-plugins-message-container"></div>
                 </div>
                 <!--end::Select-->
             </div>
             <div class="col-xl-6 {{ ($obj->if_master_clinica == 'otro') ? '' : 'hidden' }}" id="master_text">
                 <div class="form-group fv-plugins-icon-container">
                     <label class="col-6 col-form-label">Otro</label>
                     <input type="text" class="form-control form-control-solid form-control-lg" name="master_otro" id="master_otro"  placeholder="" value=" {{ $obj->master_otro }}" disabled>
                     <div class="fv-plugins-message-container"></div>
                 </div>
             </div>
             <div class="col-xl-6">
                 <!--begin::Input-->
                 <div class="form-group fv-plugins-icon-container">
                     <label class="col-6 col-form-label">Semestre de Maestría<span class="require">*</span></label>
                     <select name="semester" id="semester" class="form-control form-control-solid form-control-lg" disabled>
                         <option value="primera_mitad" {{($obj->semester == 'primera_mitad') ? 'selected' : ''  }}>1era Mitad</option>
                         <option value="segunda_mitad" {{($obj->semester == 'segunda_mitad') ? 'selected' : ''  }}>2da Mita</option>
                         <option value="terminada" {{($obj->semester == 'terminada') ? 'selected' : ''  }}>Terminada</option>
                     </select>

                 </div>
                 <!--end::Input-->
             </div>
             <div class="col-xl-6">
                 <div class="form-group fv-plugins-icon-container">
                     <label class="col-9 col-form-label">Cédula Licenciatura</label>
                     <input type="text" class="form-control form-control-solid form-control-lg"  name="cedula" id="cedula" value="{{$obj->cedula}}" placeholder="Cédulas Profesionales" data-tippy-content="Escribe el número de tu cédula profesional más actual">
                 </div>
             </div>
             <div class="col-xl-6">
                 <div class="form-group fv-plugins-icon-container">
                     <label class="col-9 col-form-label">Cédula Maestría</label>
                     <input type="text" class="form-control form-control-solid form-control-lg"  name="cedula_master" id="cedula_master" value="{{$obj->cedula_master}}" placeholder="Cédulas Maestría">
                 </div>
             </div>
             <div class="col-xl-6">
                 <!--begin::Input-->
                 <div class="form-group fv-plugins-icon-container">
                     <label class="col-6 col-form-label">Corriente Principal</label>
                     <select name="current_principal_id" id="current_principals" class="form-control form-control-solid form-control-lg" disabled>
                         <option value="">Seleccione...</option>
                         @foreach($current_principals as $key => $text)

                         <option value="{{ $key }}" {{ $key == $obj->current_principal_id   ? 'selected' : '' }}>{{ $text }}</option>
                         @endforeach
                     </select>
                 </div>
                 <!--end::Input-->
             </div>
             <div class="col-xl-6">
                 <!--begin::Select-->
                 <div class="form-group fv-plugins-icon-container">
                     <label class="col-6 col-form-label">Corriente Complementaria</label>
                     <select name="current_complementary_id[]" id="current_complementaries" class="form-control form-control-solid form-control-lg select2" disabled multiple>
                         <option value="">Seleccione...</option>
                         @foreach($current_complementaries as $key => $text)
                         <option value="{{ $key }}" {{ in_array($key,  $current_complementary ?? []) ? 'selected' : '' }}>{{ $text }}</option>
                         @endforeach
                     </select>

                 </div>
                 <!--end::Select-->
             </div>
             <div class="col-xl-6">
                 <div class="form-group fv-plugins-icon-container">
                     <label class="col-6 col-form-label">Problemáticas Específicas</label>
                     <select name="specific_problem_id[]" id="specific_problems" class="form-control form-control-solid form-control-lg select2" multiple disabled>
                         <option value="">Seleccione...</option>
                         @foreach($specific_problems as $key => $text)
                         <option value="{{ $key }}" {{ in_array($key,  $specificProblems ?? []) ? 'selected' : '' }}>{{ $text }}</option>

                         @endforeach
                     </select>
                 </div>
             </div>
             <div class="col-xl-6">
                 <div class="form-group fv-plugins-icon-container">
                     <div class="form-group">
                         <label>Población</label>
                         <select name="poblacion[]" id="poblacion" class="form-control form-control-solid form-control-lg select2" multiple disabled>
                             @foreach($poblations as $key => $text)
                             <option value="{{ $key }}" {{ in_array($key,  $poblation ?? []) ? 'selected' : '' }}>{{ $text }}</option>
                             @endforeach
                         </select>
                     </div>
                 </div>
             </div>
             <div class="col-xl-6">
                 <div class="form-group fv-plugins-icon-container">
                     <label class="col-6 col-form-label">Idioma</label>
                     <select name="language_id" id="language" class="form-control form-control-solid form-control-lg" disabled>
                         <option value="">Seleccione...</option>
                         @foreach($languages as $key => $text)
                         <option value="{{ $key }}" {{ in_array($key,  $language ?? []) ? 'selected' : '' }}>{{ $text }}</option>
                         @endforeach
                     </select>
                 </div>
             </div>
             <div class="col-xl-6">
                 <div class="form-group fv-plugins-icon-container">
                     <label class="col-6 col-form-label">Modalidad</label>
                     <select name="modality" id="modality" class="form-control form-control-solid form-control-lg" disabled>
                         <option value="lineal" {{($obj->modality == 'lineal') ? 'selected' : ''  }}>Línea</option>
                         <option value="presencial" {{($obj->modality == 'presencial') ? 'selected' : ''  }}>Presencial</option>
                         <option value="ambas" {{($obj->modality == 'ambas') ? 'selected' : ''  }}>Ambas</option>
                     </select>
                 </div>
             </div>

             
             
         </div>
            <div class="row {{($obj->modality == 'lineal') ? 'hidden' : ''  }}">
                <div class="col-md-12">
                    <span style="font-size: large; font-weight: 600; color: #e7a4a2">Dirección de Consultorio</span><br>
                    
                </div>
            @if(isset($addressclinic))    
                @foreach($addressclinic as $key => $address)
                    <div class="col-xl-12">
                        <div class="form-group fv-plugins-icon-container">
                            <label class="col-12 col-form-label">Dirección </label>
                            <textarea name="address[]" id="address" cols="30" rows="3" class="form-control form-control-solid" >{{$address->address}}</textarea>
                        </div>
                    </div>
                    <div class="col-xl-3">
                        <div class="form-group fv-plugins-icon-container">
                            <label class="col-6 col-form-label">Código Postal</label>
                            <input type="text" name="cod_postal[]" id="cod_postal{{$key}}" class="form-control form-control-solid form-control-lg"  placeholder="Código Postal" value="{{$address->cod_postal}}">
                        </div>
                    </div>
                    <div class="col-xl-3">
                        <div class="form-group fv-plugins-icon-container">
                            <label class="col-6 col-form-label">Ciudad</label>
                            <input type="text" class="form-control form-control-solid form-control-lg" name="city[]" id="city{{$key}}" value="{{$address->city}}">
                        </div>
                    </div>
                    <div class="col-xl-3">
                        <div class="form-group fv-plugins-icon-container">
                            <label class="col-9 col-form-label">Delegación o Municipio</label>
                            <input type="text" class="form-control form-control-solid form-control-lg" name="delegation[]" id="delegation{{$key}}" value="{{$address->delegation}}">
                        </div>
                    </div>
                    <div class="col-xl-3">
                        <div class="form-group fv-plugins-icon-container">
                            <label class="col-6 col-form-label">Colonia</label>
                            <input type="text" class="form-control form-control-solid form-control-lg" name="colonia[]" id="colonia{{$key}}" value="{{$address->colonia}}">
                        </div>
                    </div>
                @endforeach
            @endif
            </div>

         <div class="row">
             <h3 class="ml-4">Licenciaturas/Maestrías/Diplomados</h3>
             @foreach($degree as $degre)
             <div class="card mb-3 mr-3 ml-3" style="width:100%">
                 <div class="card-body" style="display: flex;justify-content: space-around;align-items: baseline;flex-wrap:wrap">
                     <h5 class="card-title">{{ $degre->licenciatura }}</h5>
                     <h6 class="card-subtitle mb-2 text-muted">{{ $degre->institucion }}</h6>
                     @foreach($degre->degreeFiles as $degreefiles)
                     <div class="card-text"><a href="{{ url($degreefiles->url) }}" target="_blank" class="btn btn-default btn-sm ml-3" style="cursor:pointer">{{ $degreefiles->file_name }}</a></div>
                     @endforeach
                 </div>
             </div>
             @endforeach
         </div>
         <div class="row">
             <span class="sub-titulo">DATOS COMPLEMENTARIOS</span>
         </div>
        <div class="row">
            <span class="sub-titulo">Horarios de Trabajo</span>
        </div>
               
                @include('modules.admin.candidates._horarios_view')
         <div class="row">
             @php
                        $start = 50;
                        $end = 1500;
                        $step = 50;
                    @endphp

                    <div  id="minimum_in_fee_lineal" class="col-xl-3 {{ ($obj->modality == 'lineal'  || $obj->modality == 'ambas') ? '' : 'hidden'  }}">
                        <div class="form-group">
                            <label id="minimum_fee_label" class="col-9 col-form-label">Honorario Mínimo en Línea </label>
                            <select  class="form-control form-control-solid form-control-lg" name="minimum_fee" id="minimum_fee" disabled placeholder="Honorario Mínimo" value="{{$obj->minimum_fee}}" >
                                @foreach (range($start, $end, $step) as $number)
                                    <option value="{{$number}}" {{($obj->minimum_fee == $number) ? 'selected' : ''  }}>{{$number}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div id="minimum_in_fee" class="col-xl-3 {{( $obj->modality == 'presencial' || $obj->modality == 'ambas') ? '' : 'hidden'  }}">
                        <div class="form-group">
                            <label  class="col-9 col-form-label">Honorario Mínimo Presencial</label>
                            <select  class="form-control form-control-solid form-control-lg" name="minimum_in_person_fee" id="minimum_in_person_fee" placeholder="Honorario Mínimo" disabled value="{{$obj->minimum_in_person_fee}}" >
                                @foreach (range($start, $end, $step) as $number)
                                    <option value="{{$number}}" {{($obj->minimum_in_person_fee == $number) ? 'selected' : ''  }}>{{$number}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div id="minimum_in_family" class="col-xl-3">
                        <div class="form-group">
                            <label id="minimum_fee_label" class="col-12 col-form-label">Honorario mínimo Pareja o Familia</label>
                            <select  class="form-control form-control-solid form-control-lg" name="minimum_fee_family" id="minimum_fee_family" placeholder="Honorario Mínimo" disabled value="{{$obj->minimum_fee_family}}" >
                                @foreach (range($start, $end, $step) as $number)
                                    <option value="{{$number}}" {{($obj->minimum_fee_family == $number) ? 'selected' : ''  }}>{{$number}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
             <div class="col-xl-3">
                 <div class="form-group fv-plugins-icon-container">
                     <label class="col-6 col-form-label">Factura</label>
                     <select name="invoice" id="invoice" class="form-control form-control-solid form-control-lg" disabled>
                         <option value="">Seleccione...</option>
                         <option value="si" {{($obj->invoice == 'si') ? 'selected' : ''  }}>Si</option>
                         <option value="no" {{($obj->invoice == 'no') ? 'selected' : ''  }}>No</option>
                     </select>
                 </div>
             </div>
             @role('rrhh')
                <div class="col-xl-3">
                    <div class="form-group fv-plugins-icon-container">
                        <label class="col-6 col-form-label">Calificación</label>
                        <div class="rating">
                            <span class="star fas fa-star" data-value="1"></span>
                            <span class="star fas fa-star" data-value="2"></span>
                            <span class="star fas fa-star" data-value="3"></span>
                            <span class="star fas fa-star" data-value="4"></span>
                            <span class="star fas fa-star" data-value="5"></span>
                        </div>
                        <!-- <input type="text" class="form-control form-control-solid form-control-lg" name="qualification" id="qualification" placeholder="Calificación" value="{{$obj->qualification}}" disabled> -->

                    </div>
                </div>
                <div class="col-xl-3">
                        <div class="form-group">
                            <label class="col-6 col-form-label">Nota Calificación</label>
                            <input type="text" class="form-control form-control-solid form-control-lg" name="note_qualification" id="note_qualification" placeholder="Nota" value="{{$obj->note_qualification}}" >
                        </div>
                    </div>
             @endrole
         </div>
         @role('rrhh')
         <div class="row">
             <div class="col-xl-12">
                 <div class="form-group fv-plugins-icon-container">
                     <label class="col-6 col-form-label">Nota Administrador</label>
                     <textarea class="form-control form-control-solid form-control-lg" name="admin_notes" id="admin_notes" placeholder="Notas Administrado" disabled>{{$obj->admin_notes}}</textarea>
                     <div class="fv-plugins-message-container"></div>
                 </div>
             </div>
         </div>
         @endrole
         @role('therapist')
         <div class="row">
             <div class="col-xl-12">
                 <div class="form-group">
                     <label class="col-6 col-form-label">Nota Terapeuta</label>
                     <textarea class="form-control form-control-solid form-control-lg" name="therapist_notes" id="therapist_notes" placeholder="Notas Terapeuta" disabled>{{$obj->therapist_notes}}</textarea>
                 </div>
             </div>
         </div>
         @endrole
         <div class="row">
             <div class="col-xl-12">
                 <div class="form-group fv-plugins-icon-container">
                     <label class="col-6 col-form-label">Acerca de Mí</label>
                     <textarea class="form-control form-control-solid form-control-lg" name="about_me" id="about_me" placeholder="Acerca de Mí" disabled>{{$obj->about_me}}</textarea>
                     <div class="fv-plugins-message-container"></div>
                 </div>
             </div>
         </div>

     </div>
 </form>