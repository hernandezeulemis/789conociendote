@php
$start_time = strtotime('00:00');
$end_time = strtotime('23:45');
$interval = 15 * 60;
@endphp
<style>
    .add-timeslot-btn {
        background-color: #e6a2a0 !important;
        border-color: #e6a2a0 !important;
        color: #fff !important;
        margin-top: 9% !important;
        width: 100%;
    }
    .plus {
        color: #fff !important;
        font-weight: bold !important;
        font-size: large !important;
    }
    .label-day {
        font-size: 23px !important;
        text-align: justify;
    }
</style>
<div class="form-group form-group-last row" id="kt_repeater_2">
    @foreach($diasSemana as $dia => $intervalos)
        <div class="col-lg-12">
            @if(!empty($intervalos))
                @foreach($intervalos as $index => $intervalo)
                    <div  class="form-group row align-items-center" id="content-{{$dia}}">
                        <div class="col-md-3">
                            <div class="kt-form__group--inline" @if($index> 0) style="display:none" @endif>
                                <div class="kt-form__label">
                                    <label class="label-day">{{$dia}}</label>
                                </div>
                                <div class="kt-form__control">
                                    <input type="hidden" class="form-control" placeholder="{{$dia}}" name="day_of_week" value="{{$dia}}">
                                </div>
                            </div>
                            <div class="d-md-none kt-margin-b-10"></div>
                        </div>
                        <div class="col-md-2">
                            <div class="kt-form__group--inline">
                                <div class="kt-form__label">
                                    <label for="timeslots[{{$dia}}][start_time][]">Hora de inicio:</label>
                                </div>
                                <div class="kt-form__control">
                                    <select name="timeslots[{{$dia}}][start_time][]" class="form-control timeslot-start-time timeslots select2" style="opacity: 1;" disabled>
                                        <option value="">Seleccione Intervalo</option>
                                        @for ($time = $start_time; $time <= $end_time; $time +=$interval) @php $formatted_time=date('H:i', $time); @endphp <option value="{{ $formatted_time }}" @if($formatted_time==date('H:i', strtotime($intervalo["start_time"]))) selected @endif>{{ $formatted_time }}</option>
                                            @endfor
                                    </select>
                                </div>
                            </div>
                            <!-- Resto de los campos de horario -->
                        </div>
                        <div class="col-md-2">
                            <div class="kt-form__group--inline">
                                <div class="kt-form__label">
                                    <label for="timeslots[0][end_time]">Hora de fin:</label>
                                </div>
                                <div class="kt-form__control">
                                    <select name="timeslots[{{$dia}}][end_time][]" class="form-control timeslot-end-time timeslots select2" style="opacity: 1;" disabled>
                                        <option value="">Seleccione Intervalo</option>
                                        @for ($time = $start_time; $time <= $end_time; $time +=$interval) @php $formatted_time=date('H:i', $time); @endphp <option value="{{ $formatted_time }}" @if($formatted_time==date('H:i', strtotime($intervalo["end_time"])) ) selected @endif >{{ $formatted_time }}</option>
                                            @endfor
                                    </select>
                                </div>
                            </div>
                            <div class="d-md-none kt-margin-b-10"></div>
                        </div>
                        <div class="col-md-2">
                                <div class="kt-form__group--inline">
                                    <div class="kt-form__label">
                                        <label for="timeslots[0][end_time]">Modalidad:</label>
                                    </div>
                                    <div class="kt-form__control">
                                        <select name="timeslots[{{$dia}}][modality][]" class="form-control timeslot-end-time timeslots select2" style="opacity: 1;" disabled>
                                            <option value="">Seleccione Modalidad</option>
                                            <option value="lineal"  {{ $intervalo['modality'] == 'lineal'  ? 'selected' : '' }} >Línea</option>
                                            <option value="presencial"  {{ $intervalo['modality'] == 'presencial'  ? 'selected' : '' }}>Presencial</option>
                                            <option value="ambas" {{ $intervalo['modality'] == 'ambas'  ? 'selected' : '' }}>Ambas</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="d-md-none kt-margin-b-10"></div>
                            </div>
                        <div class="col-md-3 form-group-last">
                            <div class="kt-form__group--inline">
                                <div class="kt-form__control">
                                    
                                </div>
                            </div>
                            <div class="d-md-none kt-margin-b-10"></div>
                        </div>
                    </div>
                @endforeach
            @endif
            <div id="{{ $dia }}"></div>
        </div>
    @endforeach
</div>