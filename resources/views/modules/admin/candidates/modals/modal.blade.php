<div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-xl">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Contactar</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form id="FormContact" action="#" role="form" style="display: contents;">
            <div class="form-group hide">
              <input type="hidden" class="therapist_id" name="id">
            </div>
            <div class="form-group">
                <div class="col-12">
                  <span id="email_therapist"></span>
                </div><br>
                <div class="col-12">
                    
                    <div class="summernote" ></div>
                    <input type="hidden" name="mensaje" id="mensaje">
                </div>
                <br>
                <div class="col-12">
                  <span id="phone_therapist"></span>
                </div>
            </div>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
        <button type="button" class="btn btn-primary" id="submitContact">Enviar</button>
      </div>
    </div>
  </div>
</div>