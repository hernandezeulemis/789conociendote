<div class="modal fade" id="mQuotes" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Agendar Cita</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form id="FormQuotes" action="#" role="form" style="display: contents;">
            <div class="form-group hide">
                <input type="hidden" id="quote_id" name="id">
                <input type="hidden" class="therapist_id" name="therapist_id">
                <input type="hidden" class="form-control type-form user_id"  name="user_id" value="{{ \Auth::user()->id }}">
            </div>
            <div class="form-group">
                <div class="row">
                    
                    <div class="col-6">
                        <label for="">Fecha</label>
                        <input type="date" class="form-control" name="date" id="date" required>
                    </div>
                     <div class="col-6">
                        <label for="">Hora</label>
                        <input type="time" class="form-control" name="hour" id="hour" required>
                    </div>
                </div>
                
            </div>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
        <button type="button" class="btn btn-primary" id="submitQuotes">Guardar</button>
      </div>
    </div>
  </div>
</div>