<form class="form fv-plugins-bootstrap fv-plugins-framework" id="formCRUDEditCandidate">
           <input type="hidden" id="therapeut_id"  value="{{ $obj->id }}"/>
            @csrf
            <div class="pb-5" data-wizard-type="step-content" data-wizard-state="current">
                <h3 class="mb-10 font-weight-bold text-dark">CANDIDATO</h3>
                <div class="white_section" style="display:flex;justify-content:center">
                    <div style="display:flex;flex-direction:column;align-items: center" >
                        <!-- <div class="profile-picture">
                            <input type="file" name="avatar" id="imagenPerfilInput" accept="image/*">
                            <input type="hidden" name="avatar_64" id="avatar_64">
                            
                            <img id="imagenPerfilPreview" width="150" height="150" src="{{ asset($obj->avatar_url) }}" alt="Foto" style="margin-bottom:2%">
                            
                        </div> -->
                        <div class="profile-picture">
                            <input type="file" name="avatar" id="imagenPerfilInput" accept="image/*">
                            <input type="hidden" name="avatar_64" id="avatar_64">
                            
                            @if ($obj->avatar_url)
                                <img id="imagenPerfilPreview" width="150" height="150" src="{{ asset($obj->avatar_url) }}" alt="Foto" style=" margin-bottom:5%">
                            @else
                                <div class="avatar-placeholder">
                                    <img id="imagenPerfilPreview" width="150" height="150" src="{{ asset('img/avatar.png') }}" alt="Foto" style="margin-bottom:5%">
                                    <div class="pencil-container">
                                        <span class="edit-icon">&#9998;</span>
                                    </div>
                                </div>
                            @endif
                        </div>
                        <input type="hidden" name="id" value="{{ $obj->id }}">
                       <!--  <div class="btn_save_image mt-4">
                            <button class="btn btn-info btn-sm">Guardar Imagen</button>
                        </div> -->
                    </div>
                </div>
                  <p class="text-center" style="font-weight: bold;">Sube una fotografia de buena calidad con fondo<br> blanco de frente que se vea tu rostro </p>

                  <span class="form-text text-muted text-center">Archivos Permitidos:  png, jpg, jpeg.</span>
                  <span class="form-text text-muted text-center">Tamaño máximo permitido 2MB.</span>
                  
                <div class="row">
                    <div class="col-xl-6">
                        <div class="form-group fv-plugins-icon-container">
                            <label class="col-6 col-form-label">Nombre Completo</label>
                            <input type="text" class="form-control form-control-solid form-control-lg" name="full_name" id="full_name" placeholder="Nombre Completo" value="{{$obj->full_name}}" required>
                            <div class="fv-plugins-message-container"></div>
                        </div>
                    </div>
                    <div class="col-xl-6">
                        <div class="form-group">
                            <label class="col-6 col-form-label">Correo</label>
                            <input type="email" class="form-control form-control-solid form-control-lg" name="email" id="email" placeholder="Correo" value="{{$obj->email}}" required>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xl-4">
                        <div class="form-group fv-plugins-icon-container">
                            <label class="col-6 col-form-label">Fecha de Nacimiento</label>
                            <input class="form-control date" type="date" value="{{ $obj->birthdate }}" id="" name="birthdate" required>
                            <span class="form-text text-muted">Agregue Fecha de Nacimiento.</span>
                            <div class="fv-plugins-message-container"></div>
                        </div>
                    </div>
                    <div class="col-xl-4">
                        <div class="form-group fv-plugins-icon-container">
                            <label class="col-6 col-form-label">Teléfono</label>
                            <input type="text" class="form-control form-control-solid form-control-lg" name="phone" id="phone" placeholder="Teléfono" value="{{$obj->phone}}" required>
                            <div class="fv-plugins-message-container"></div>
                        </div>
                    </div>
                    <div class="col-xl-4">
                        <div class="form-group fv-plugins-icon-container">
                            <label class="col-6 col-form-label">Sexo</label>
                            <select name="sexo" id="sexo" class="form-control form-control-solid form-control-lg" required>
                                <option value="">Seleccione...</option>
                                <option value="m" {{($obj->sexo == 'm') ? 'selected' : ''  }}>Masculino</option>
                                <option value="f" {{($obj->sexo == 'f') ? 'selected' : ''  }}>Femenino</option>
                            </select>
                        </div>
                        
                    </div>
                </div>
                <div class="row">
                    <div class="col-xl-6">
                        <!--begin::Input-->
                        <div class="form-group fv-plugins-icon-container">
                            <label class="col-6 col-form-label">Licenciatura en Psicología</label>
                            <select name="lic_psicologia" id="lic_psicologia" class="form-control form-control-solid form-control-lg" required>
                                <option value="">Seleccione...</option>
                                <option value="si" {{ ($obj->lic_psicologia == 'si') ? 'selected' : '' }}>Si</option>
                                <option value="no" {{ ($obj->lic_psicologia == 'no') ? 'selected' : '' }}>No</option>
                                <option value="otro" {{ ($obj->lic_psicologia == 'otro') ? 'selected' : '' }}>Otro</option>
                            </select>
                            <div class="fv-plugins-message-container"></div>
                        </div>
                        <!--end::Input-->
                    </div>
                    <div class="col-xl-6 {{ ($obj->lic_psicologia == 'otro') ? '' : 'hidden' }}" id="lic_text">
                        <div class="form-group fv-plugins-icon-container">
                            <label class="col-6 col-form-label">Otro</label>
                            <input type="text" class="form-control form-control-solid form-control-lg" name="lic_otro" id="lic_otro" required placeholder="" value=" {{$obj->lic_otro }}">
                            <div class="fv-plugins-message-container"></div>
                        </div>
                    </div>
                     <div class="col-xl-6">
                        <div class="form-group fv-plugins-icon-container">
                            <label class="col-6 col-form-label">Maestría o Especialidad<span class="require">*</span></label>
                            <select name="if_master_clinica"  class="form-control form-control-solid form-control-lg master_clinica" required>
                                <option value="">Seleccione...</option>
                                <option value="si" {{ ($obj->if_master_clinica == 'si') ? 'selected' : '' }}>Si</option>
                                <option value="no" {{ ($obj->if_master_clinica == 'no') ? 'selected' : '' }}>No</option>
                                <option value="otro" {{ ($obj->if_master_clinica == 'otro') ? 'selected' : '' }}>Otro</option>
                            </select>
                            <div class="fv-plugins-message-container"></div>
                        </div>
                    </div>
                    <div id="maestria" class="col-xl-6 {{ ($obj->if_master_clinica == 'si') ? '' : 'hidden' }}">
                        <!--begin::Select-->
                        <div class="form-group fv-plugins-icon-container">
                            <label class="col-6 col-form-label">Maestría o Especialidad Clínica</label>
                            <select name="clinical_specialty_id" id="clinical_specialties" class="form-control form-control-solid form-control-lg" >
                                <option value="">Seleccione...</option>
                                @foreach($clinical_specialties as $key => $text)
                                <option value="{{ $key }}" {{ $key == $obj->clinical_specialty_id   ? 'selected' : '' }}>{{ $text }}</option>
                                @endforeach
                            </select>
                            <div class="fv-plugins-message-container"></div>
                        </div>
                        <!--end::Select-->
                    </div>
                    <div class="col-xl-6 {{ ($obj->if_master_clinica == 'otro') ? '' : 'hidden' }}" id="master_text">
                        <div class="form-group fv-plugins-icon-container">
                            <label class="col-6 col-form-label">Otro</label>
                            <input type="text" class="form-control form-control-solid form-control-lg" name="master_otro" id="master_otro" placeholder="" value=" {{ $obj->master_otro }}">
                            <div class="fv-plugins-message-container"></div>
                        </div>
                    </div>
                    <div class="col-xl-6">
                        <!--begin::Input-->
                        <div class="form-group fv-plugins-icon-container">
                            <label class="col-6 col-form-label">Semestre de Maestría<span class="require">*</span></label>
                            <select name="semester" id="semester" class="form-control form-control-solid form-control-lg" required>
                                <option value="primera_mitad" {{($obj->semester == 'primera_mitad') ? 'selected' : ''  }}>1era Mitad</option>
                                <option value="segunda_mitad" {{($obj->semester == 'segunda_mitad') ? 'selected' : ''  }}>2da Mitad</option>
                                <option value="terminada" {{($obj->semester == 'terminada') ? 'selected' : ''  }}>Terminada</option>
                            </select>

                        </div>
                        <!--end::Input-->
                    </div>
                    <div class="col-xl-6">
                        <div class="form-group fv-plugins-icon-container">
                            <label class="col-9 col-form-label">Cédula Licenciatura</label>
                            <input type="text" class="form-control form-control-solid form-control-lg"  name="cedula" id="cedula" placeholder="Cédulas Profesionales" data-tippy-content="Escribe el número de tu cédula profesional más actual" value="{{$obj->cedula}}">
                        </div>
                    </div>
                    <div class="col-xl-6">
                        <div class="form-group fv-plugins-icon-container">
                            <label class="col-9 col-form-label">Cédula Maestría</label>
                            <input type="text" class="form-control form-control-solid form-control-lg" name="cedula_master" id="cedula_master" placeholder="Cédulas Maestría" value="{{$obj->cedula_master}}">
                        </div>
                    </div>
                    <div class="col-xl-6">
                        <!--begin::Input-->
                        <div class="form-group fv-plugins-icon-container">
                            <label class="col-6 col-form-label">Corriente Principal</label>
                            <select name="current_principal_id" id="current_principals" class="form-control form-control-solid form-control-lg" required>
                                <option value="">Seleccione...</option>
                                @foreach($current_principals as $key => $text)

                                <option value="{{ $key }}" {{ $key == $obj->current_principal_id   ? 'selected' : '' }}>{{ $text }}</option>
                                @endforeach
                            </select>
                        </div>
                        <!--end::Input-->
                    </div>
                    <div class="col-xl-6">
                        <!--begin::Select-->
                        <div class="form-group fv-plugins-icon-container">
                            <label class="col-12 col-form-label">Corrientes Complementarias <strong>(Puedes seleccionar más de una opción)</strong></label>
                            <select name="current_complementary_id[]" id="current_complementaries" class="form-control form-control-solid form-control-lg select2" multiple>
                                <option value="">Seleccione...</option>
                                @foreach($current_complementaries as $key => $text)
                                <option value="{{ $key }}" {{ in_array($key,  $current_complementary ?? []) ? 'selected' : '' }}>{{ $text }}</option>
                                @endforeach
                            </select>

                        </div>
                        <!--end::Select-->
                    </div>
                    
                    
           
                    <div class="col-xl-6">
                        <div class="form-group fv-plugins-icon-container">
                            <label class="col-12 col-form-label">Problemáticas Específicas <strong>(Puedes seleccionar más de una opción)</strong></label>
                            <select name="specific_problem_id[]" id="specific_problems" class="form-control form-control-solid form-control-lg select2" multiple required>
                                <option value="">Seleccione...</option>
                                @foreach($specific_problems as $key => $text)
                                <option value="{{ $key }}" {{ in_array($key,  $specificProblems ?? []) ? 'selected' : '' }}>{{ $text }}</option>

                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-xl-6">
                        <div class="form-group fv-plugins-icon-container">
                            <div class="form-group">
                                <label class="col-12 col-form-label">Población <strong>(Puedes seleccionar más de una opción)</strong></label>
                                <select name="poblacion[]" id="poblacion" class="form-control form-control-solid form-control-lg select2" multiple required>
                                    @foreach($poblations as $key => $text)
                                    <option value="{{ $key }}" {{ in_array($key,  $poblation ?? []) ? 'selected' : '' }}>{{ $text }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-4">
                        <div class="form-group fv-plugins-icon-container">
                            <label class="col-12 col-form-label">Idioma <strong>(Puedes seleccionar más de una opción)</strong></label>
                            <select name="language_id[]" id="language" class="form-control form-control-solid form-control-lg select2" required>
                                <option value="">Seleccione...</option>
                                @foreach($languages as $key => $text)
                                <option value="{{ $key }}" {{ in_array($key,  $language ?? []) ? 'selected' : '' }}>{{ $text }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-xl-4">
                        <div class="form-group fv-plugins-icon-container">
                            <label class="col-6 col-form-label">Modalidad</label>
                            <select name="modality" id="modality" class="form-control form-control-solid form-control-lg" required>
                                <option value="lineal" {{($obj->modality == 'lineal') ? 'selected' : ''  }}>Línea</option>
                                <option value="presencial" {{($obj->modality == 'presencial') ? 'selected' : ''  }}>Presencial</option>
                                <option value="ambas" {{($obj->modality == 'ambas') ? 'selected' : ''  }}>Ambas</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-xl-4">
                        <div class="form-group fv-plugins-icon-container">
                            <label class="col-6 col-form-label">Tipo de Terapia</label>
                            <select name="therapy_type" id="therapy_type" class="form-control form-control-solid form-control-lg" required>
                                <option value="individual" {{($obj->therapy_type == 'individual') ? 'selected' : ''  }}>Individual</option>
                                <option value="pareja" {{($obj->therapy_type == 'pareja') ? 'selected' : ''  }}>Pareja</option>
                                <option value="familiar" {{($obj->therapy_type == 'familiar') ? 'selected' : ''  }}>Familiar</option>
                            </select>
                        </div>
                    </div>
                </div>

                
                 <div class="white_section row {{($obj->modality == 'lineal') ? 'hidden' : ''  }}" id="filed_direction">
                        <div class="col-md-12">
                            <span style="font-size: large; font-weight: 600; color: #e7a4a2">Dirección de Consultorio</span><br>
                            
                        </div>
                        <div class="col-xl-3 mt-12">
                            <button type="button" class="btn btn-primary btn-block" id="addAddress" style="margin-bottom: 20px;"><strong><i class="fa fa-plus"></i> Añadir Dirección</strong>     </button>
                        </div>
                    @if(isset($addressclinic))
                        @foreach($addressclinic as $key => $address)
                            <div class="col-xl-12">
                                <div class="form-group fv-plugins-icon-container">
                                    <label class="col-12 col-form-label">Dirección</label>
                                    <textarea name="address[]" id="address" cols="30" rows="3" class="form-control form-control-solid" >{{$address->address}}</textarea>
                                </div>
                            </div>
                            <div class="col-xl-3">
                                <div class="form-group fv-plugins-icon-container">
                                    <label class="col-6 col-form-label">Código Postal</label>
                                    <input onblur="getAddress('{{ $key }}')" type="text" name="cod_postal[]" id="cod_postal{{$key}}" class="form-control form-control-solid form-control-lg"  placeholder="Código Postal" value="{{$address->cod_postal}}">
                                </div>
                            </div>
                            <div class="col-xl-3">
                                <div class="form-group fv-plugins-icon-container">
                                    <label class="col-6 col-form-label">Ciudad</label>
                                    <input type="text" class="form-control form-control-solid form-control-lg" name="city[]" id="city{{$key}}" value="{{$address->city}}">
                                </div>
                            </div>
                            <div class="col-xl-3">
                                <div class="form-group fv-plugins-icon-container">
                                    <label class="col-9 col-form-label">Delegación o Municipio</label>
                                    <input type="text" class="form-control form-control-solid form-control-lg" name="delegation[]" id="delegation{{$key}}" value="{{$address->delegation}}">
                                </div>
                            </div>
                            <div class="col-xl-3">
                                <div class="form-group fv-plugins-icon-container">
                                    <label class="col-6 col-form-label">Colonia</label>
                                    <input type="text" class="form-control form-control-solid form-control-lg" name="colonia[]" id="colonia{{$key}}" value="{{$address->colonia}}">
                                </div>
                            </div>
                        @endforeach
                    @endif   
                        
                        <div class="col-xl-12">
                            <div id="addressclinic"></div>
                        </div>
         
                
                </div>
                <div class="row">
                  
                </div>
                <div  id="kt_repeater_1" class="white_section">
                    <div class="row">
                        <div class="col-md-12">
                            <span style="font-size: large; font-weight: 600; color: #e7a4a2">Estudios</span><br>
                            <strong>(Llena la información de tus estudios uno por uno y los vas subiendo)</strong>
                        </div>
                    </div><br><br>
                    <div class="form-group form-group-last row" id="kt_repeater_2">
                        
                        <div data-repeater-list="" class="col-lg-12">
                            <div data-repeater-item="" class="form-group row align-items-center">     
                                <div class="col-md-4">
                                    <div class="kt-form__group--inline">
                                        <div class="kt-form__label">
                                            <label>Licenciatura/Maestría/Diplomado:</label>
                                        </div>
                                        <div class="kt-form__control">
                                            <input type="text" class="form-control" placeholder="Licenciatura" name="licenciatura" id="licenciatura"> 
                                        </div>
                                    </div>
                                    <div class="d-md-none kt-margin-b-10"></div>
                                </div>
                                <div class="col-md-4">
                                    <div class="kt-form__group--inline">
                                        <div class="kt-form__label">
                                            <label class="kt-label m-label--single">Nombre de la Institución</label>
                                        </div>
                                        <div class="kt-form__control">
                                            <input type="text" class="form-control" placeholder="Nombre de la Institución" name="institucion" id="institucion">   
                                        </div>
                                    </div>
                                    <div class="d-md-none kt-margin-b-10"></div>
                                </div>  
                                <div class="col-lg-4 mt-8 pr-3" id="subir_file">
                                    <div class="custom-input-file col-md-6 col-sm-6 col-xs-6">
                                    <input type="file" id="fichero-tarifas" class="input-file" onchange="getImgBase()"    name="uploadfiles[]"  multiple="multiple">
                                        Subir Archivos...
                                    </div>
                                </div>
                            </div>                            
                        </div>                 
                    </div>
                    <div class="form-group form-group-last row">
                        <div class="col-lg-3">
                            
                            <a href="javascript:;" class="btn btn-primary btn-block"  id="btnAdd">
                                <i class="fa fa-plus"></i> Subir Estudio
                            </a>
                        </div>   
                        <div style="display: flex;justify-content: end;align-items: end;" class="col-lg-8" id="archivos_seleccionados"></div>                                     
                    </div>
                    <div id="loading" class="loading-overlay hidden" >
                        <div class="spinner"></div>
                        <div class="loading-text">Cargando el Archivo...</div>
                    </div>
                    
                    <br>
                 
                </div>
                @if(count($degree) > 0)
                    <div class="row">
                        <h3 class="ml-4">Licenciaturas</h3>
                        @foreach($degree as $degre)
                        <div id="file_{{$degre->id}}"  class="card mb-3 mr-3 ml-3 card-ppal" >
                            <div  class="card-body" style="display: flex;justify-content: space-between;align-items: baseline;flex-wrap:wrap">
                                <h5 class="card-title">{{ $degre->licenciatura }}</h5>
                                <h6 class="card-subtitle mb-2 text-muted">{{ $degre->institucion }}</h6>
                                @if($degre->degreeFiles)
                                    @foreach($degre->degreeFiles as $degreefiles)
                                        <div class="card-text"><a href="{{ url($degreefiles->url) }}" target="_blank" class="btn btn-default btn-sm ml-3" style="cursor:pointer">{{ $degreefiles->file_name }}</a></div>
                                    @endforeach
                                @endif
                                <div class="bt_eliminar">
                                    <button class="btn btn-danger btn-sm delete-certificate" onclick="degreedelete(event,{{ $degre->id }})">Eliminar</button>
                                </div>
                            </div>
                        </div>
                        @endforeach
                    </div>
                @endif
                 <br>
                <div id="certificateList"></div>
                <div class="row">
                    <span class="sub-titulo" style="font-size: 20px; font-weight: 600; color: #e7a4a2">DATOS COMPLEMENTARIOS</span>
                </div>
                <div class="row">
                    <span class="sub-titulo" style="font-size: 16px; font-weight: 500;">Horarios de Trabajo</span>
                </div>
               
                @include('modules.admin.candidates._horarios')
                
                <div class="row">
                    @php
                        $start = 50;
                        $end = 1500;
                        $step = 50;
                    @endphp

                    <div  id="minimum_in_fee_lineal" class="col-xl-3 {{ ($obj->modality == 'lineal'  || $obj->modality == 'ambas') ? '' : 'hidden'  }}">
                        <div class="form-group">
                            <label id="minimum_fee_label" class="col-9 col-form-label">Honorario Mínimo en Línea </label>
                            <select  class="form-control form-control-solid form-control-lg" name="minimum_fee" id="minimum_fee" placeholder="Honorario Mínimo" value="{{$obj->minimum_fee}}" >
                                @foreach (range($start, $end, $step) as $number)
                                    <option value="{{$number}}" {{($obj->minimum_fee == $number) ? 'selected' : ''  }}>{{$number}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div id="minimum_in_fee" class="col-xl-3 {{( $obj->modality == 'presencial'  || $obj->modality == 'ambas') ? '' : 'hidden'  }}">
                        <div class="form-group">
                            <label  class="col-9 col-form-label">Honorario Mínimo Presencial</label>
                            <select  class="form-control form-control-solid form-control-lg" name="minimum_in_person_fee" id="minimum_in_person_fee" placeholder="Honorario Mínimo" value="{{$obj->minimum_in_person_fee}}">
                                @foreach (range($start, $end, $step) as $number)
                                    <option value="{{$number}}" {{($obj->minimum_in_person_fee == $number) ? 'selected' : ''  }}>{{$number}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div id="minimum_in_family" class="col-xl-3">
                        <div class="form-group">
                            <label id="minimum_fee_label" class="col-12 col-form-label">Honorario mínimo Pareja o Familia</label>
                            <select  class="form-control form-control-solid form-control-lg" name="minimum_fee_family" id="minimum_fee_family" placeholder="Honorario Mínimo" value="{{$obj->minimum_fee_family}}" >
                                @foreach (range($start, $end, $step) as $number)
                                    <option value="{{$number}}" {{($obj->minimum_fee_family == $number) ? 'selected' : ''  }}>{{$number}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-xl-3">
                        <div class="form-group fv-plugins-icon-container">
                            <label class="col-6 col-form-label">Factura</label>
                            <select name="invoice" id="invoice" class="form-control form-control-solid form-control-lg" >
                                <option value="">Seleccione...</option>
                                <option value="si" {{($obj->invoice == 'si') ? 'selected' : ''  }}>Si</option>
                                <option value="no" {{($obj->invoice == 'no') ? 'selected' : ''  }}>No</option>
                            </select>
                        </div>
                    </div>
                    @role('rrhh')
                    <div class="col-xl-3">
                        <div class="form-group fv-plugins-icon-container">
                            <label class="col-6 col-form-label">Calificación</label>
                            <div class="rating">
                                <span class="star fas fa-star"  data-value="1"></span>
                                <span class="star fas fa-star" data-value="2"></span>
                                <span class="star fas fa-star" data-value="3"></span>
                                <span class="star fas fa-star" data-value="4"></span>
                                <span class="star fas fa-star" data-value="5"></span>
                            </div>
                            <input type="hidden" class="form-control form-control-solid form-control-lg" name="qualification" id="qualification" placeholder="Calificación" value="{{$obj->qualification}}" readonly>
                        </div>
                    </div>
                    <div class="col-xl-3">
                        <div class="form-group">
                            <label class="col-6 col-form-label">Nota Calificación</label>
                            <input type="text" class="form-control form-control-solid form-control-lg" name="note_qualification" id="note_qualification" placeholder="Nota" value="{{$obj->note_qualification}}" >
                        </div>
                    </div>
                    @endrole
                </div>
                @role('rrhh')
                <div class="row">
                    <div class="col-xl-12">
                        <div class="form-group fv-plugins-icon-container">
                            <label class="col-6 col-form-label">Notas Administrador</label>
                            <textarea class="form-control form-control-solid form-control-lg" name="admin_notes" id="admin_notes" placeholder="Notas Administrado" >{{$obj->admin_notes}}</textarea>
                            <div class="fv-plugins-message-container"></div>
                        </div>
                    </div>
                </div>
                @endrole
                @role('therapist')
                <div class="row">
                    <div class="col-xl-12">
                        <div class="form-group">
                            <label class="col-6 col-form-label">Nota Terapeuta</label>
                            <textarea class="form-control form-control-solid form-control-lg" name="therapist_notes" id="therapist_notes" placeholder="Notas Terapeuta" >{{$obj->therapist_notes}}</textarea>
                        </div>
                    </div>
                </div>
                @endrole
                <div class="row">
                    <div class="col-xl-12">
                        <div class="form-group fv-plugins-icon-container">
                            <label class="col-6 col-form-label">Acerca de Mí</label>
                            <textarea class="form-control form-control-solid form-control-lg" name="about_me" id="about_me" placeholder="Acerca de Mí" >{{$obj->about_me}}</textarea>
                            <div class="fv-plugins-message-container"></div>
                        </div>
                    </div>
                </div>
            </div>
        </form>