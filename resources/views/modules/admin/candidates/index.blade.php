{{-- Extends layout --}}
@extends('layout.default')
@section('title',__($_title))
{{-- Content --}}
<!-- CSS de Summernote -->


<style>
        .error {
            color: red;
        }
    </style>
@section('content')
    <div class="card">
        <div class="card-body">
            <div class="row">
                <div class="col-md-4">
                    <label for=""><b>STATUS</b></label>
                    <select name="filter" id="filter" class="form-control">
                        <option value="all">Todos</option>
                        <option value="aceptado">Aceptado</option>
                        <option value="pendiente">Pendiente</option>
                        <option value="rechazado">Rechazado</option>
                        <option value="cita_agendada">Cita Agendada</option>
                        <!-- <option value="de_alta">De Alta</option> -->
                    </select>
                </div>
            </div>
            <br>
            <div class="row">
                <div class="col-md-12">
                    <table class="display table table-striped table-bordered" id="dtTableCandidates" style="width:100%">
                    </table>
                </div>
            </div>
        </div>
    </div>
@include('modules.admin.candidates.modals.modal')
@include('modules.admin.candidates.modals.modalQuotes')
@endsection


{{-- Styles Section --}}
@section('styles')

@endsection

{{-- Scripts Section --}}
@section('scripts')

<script src="https://malsup.github.io/jquery.form.js"></script> 
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.20.0/jquery.validate.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.20.0/additional-methods.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.20.0/localization/messages_es.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote-bs4.js"></script>
<script src="{{ asset('js/functions.js') }}"></script>

<script>
    
    const url = '{{url($_module_route)}}'
    const token = '{{ csrf_token() }}';
    var userData = {!! json_encode(auth()->user()) !!};
    
    $('.summernote').summernote();

    var initDataTable = function(status){
        var table = $('#dtTableCandidates');
        var oTable = table.dataTable({
            destroy: true,
            "loadingMessage": 'Cargando...',
            "ajax": {
               url:  url + "/get_data/" +status,
               "dataSrc": "list",
               beforeSend:function(){
                  // $.LoadingOverlay("show");
               },complete:function(data){
                /* 
                     if(data.responseJSON.list.length > 0){
                        
                     }
                     
                   //$.LoadingOverlay("hide"); */
               },
            },
            "createdRow": function ( row, data, index ) {
            },
            "columns": [
                { "data": "id", "title": "Id", "visible": true },
                { "data": "full_name", "title": "Nombre Completo del Candidato", "visible": true },
                { "data": "status", "title": "Status", "visible": true,
                    render: function (data, type, row) {
                        switch (data) {
                            case 'aceptado':
                                return '<span class="badge badge-success">Aceptado</span>';
                                break;
                            case 'rechazado':
                                return '<span class="badge badge-danger">Rechazado</span>';
                                break;
                            case 'cita_agendada':
                                return '<span class="badge badge-info">Agendado</span>';
                                break;
                            case 'de_alta':
                                return '<span class="badge badge-primary">De Alta</span>';
                                break;
                            default:
                                return '<span class="badge badge-warning">Pendiente</span>';
                                break;
                        }
                        
                    }
                },
                { "data": "", "title": "Acciones", "visible": true, "orderable": true ,
                    render: function (data, type, row) {
                        var btAceptado = '<a class="btn btn-xs btn-success" id="btAceptado" style="background-color: #5867dd; border-color: #5867dd" title="Aceptado">'+
                                        '<i class="fa fa-check"></i>'+
                                    '</a>';
                        var btRechazado =   '<a class="btn btn-xs btn-danger" id="btRechazado" style="width: 44px;height: 32px">'+
                                        '<i class="fa fa-ban" style="color:white" title="Rechazado"></i>'+
                                        '</a>';
                        var btCita = '<a class="btn btn-xs btn-info" id="btCita" style="" title="Dar Cita" style="width: 44px;height: 32px">'+
                                        '<i class="fa fa-calendar"></i>'+
                                    '</a>';
                        var btDeALta =   '<a class="btn btn-xs btn-primary" id="btDeAlta" title="De ALta" style="width: 44px;height: 32px">'+
                                        '<i class="fa fa-user-plus"  ></i>'+
                                        '</a>';
                        var btContactar =   '<a class="btn btn-xs btn-warning" id="btContactar" style="width: 44px;height: 32px">'+
                                        '<i class="fa fa-envelope" style="color:white" title="Contactar"></i>'+
                                        '</a>';
                        var btnVer      = '<a  class="btn btn-info" href="'+ url +'/'+ row.id +'" style="background-color: #e7a4a2; border-color:#e7a4a2; width: 44px;height: 32px"> <i class="fa fa-eye"  style="font-size:14px; text-align:center"></i></a>';

                        var btnEditar   = '<a  class="btn btn-primary" href="'+ url +'/'+ row.id +'/edit" style="background-color: #0abb87; border-color:#0abb87; width: 44px;height: 32px"> <i class="fa fa-edit"  style="font-size:14px; text-align:center"></i></a>';
                       
                        switch (row.status) {
                            case 'aceptado':
                                return  btnVer + '  ' + btnEditar + '  ' + btCita + '  ' + btContactar;
                                break;
                            case 'rechazado':
                                return btnVer;
                                break;
                            case 'cita_agendada':
                                return btnVer + '  ' + btnEditar + '  ' + btDeALta;
                                break;
                            case 'pendiente':
                                return btnVer + '  ' + btnEditar + '  ' +btAceptado+ '  ' +btRechazado;
                                break;
                            default:
                                return '';
                                break;
                        }
                        


                        
                    }
                }
            ],
            responsive: false,
            "order": [
                /*[0, 'asc']*/
            ],
            "lengthMenu": [
                [5, 10, 15, 20, -1],
                [5, 10, 15, 20, "All"] // change per page values here
            ],
            // set the initial value
            "pageLength": 10,
        });

        $('#dtTableCandidates tbody').on('click', '#btAceptado, #btRechazado, #btContactar, #btCita, #btDeAlta', function() {
            var data = oTable.DataTable().row($(this).parents('tr')).data();

          if (this.id == "btAceptado") {
                changeStatus(data);
            }//fin de btEditar

            if(this.id == "btRechazado"){
                $.get(url + "/decline/"+data.id, function(response) {
                    oTable.DataTable().ajax.reload();
                    Swal.fire({
                        position: "top-end",
                        icon: "success",
                        title: "Operación Exitosa!!!",
                        showConfirmButton: false,
                        timer: 2500
                    });
                });
            }

            if(this.id == "btDeAlta"){
                $.get(url + "/de_alta/"+data.id, function(response) {
                    oTable.DataTable().ajax.reload();
                    if(response.success){
                        oTable.DataTable().ajax.reload();
                        Swal.fire({
                            position: "top-end",
                            icon: "success",
                            title: response.message,
                            showConfirmButton: false,
                            timer: 2500
                        });
                    }else{
                         Swal.fire({
                            position: "top-end",
                            icon: "error",
                            title: response.message,
                            showConfirmButton: false,
                            timer: 2500
                        });
                    }
                });
            }

            if (this.id == "btContactar") {
                
                $('.therapist_id').val(data.id)
                $('#exampleModal').modal('show')
                $('#email_therapist').text(data.email)
                $('#phone_therapist').text(data.phone)
                
                var defaultValue = 'Hola, soy la psicóloga   <strong>'+ userData.name +'</strong>   encargada del área de integración de terapeutas de Conociendote.<br>'+  
                                   'Revisamos tu solicitud y estamos interesados en que formes parte de la plataforma.<br>'+
                                    'Me encantaría coordinar una entrevista contigo para explicarte con detalle como funciona la plataforma, profundizar en tu experiencia y conocerte mejor.<br>'+   
                                    '¿Podrías proporcionarme tus horarios disponibles para que podamos programar una entrevista de 30-45 por videollamada? Estaré atenta a tu respuesta. <br>'+ 
                                    'Muchas gracias de nuevo.';

                $('.summernote').summernote({
                    focus: true,
                    height: 1400
                }).summernote('code', defaultValue);
                
            }

            if (this.id == "btCita") {
                const formulario = document.getElementById('FormQuotes');
                formulario.reset();
                $('.therapist_id').val(data.id)
                $('#mQuotes').modal('show')
            }
        });

        var changeStatus = function(data){

            $.get(url + "/change_status/" + data.id, function (response) {
                if(response){
                    oTable.DataTable().ajax.reload();
                    Swal.fire({
                        position: "top-end",
                        icon: "success",
                        title: "Operación Exitosa!!!",
                        showConfirmButton: false,
                        timer: 2500
                    });
                }
            });
        }
    }
        
    $('#submitContact').on('click',function(){
        var form = 'FormContact';
        var urlp = url + "/send_contact";
        var validator = $('#FormContact').validate({
            errorClass: "error",
                errorElement: "span",
                errorPlacement: function(error, element) {
                    error.appendTo(element.parent());
                }
        });
		
        if(validator.form()){
            var contenido = $('.summernote').summernote('code');
            $('#mensaje').val(contenido)
            submit(form,urlp);
            $('#exampleModal').modal('hide')

            var table = $('.table');
			var oTable = table.dataTable();
				oTable.DataTable().ajax.reload();
        }
        
    })

    $('#submitQuotes').on('click',function(){
        var form = 'FormQuotes';
        var urlp = url + "/save_quotes";
        var validator = $('#FormQuotes').validate({
            errorClass: "error",
                errorElement: "span",
                errorPlacement: function(error, element) {
                    error.appendTo(element.parent());
                }
        });
		
        if(validator.form()){
            submit(form,urlp);
            $('#mQuotes').modal('hide')
            
            var table = $('.table');
			var oTable = table.dataTable();
				oTable.DataTable().ajax.reload();
        }
        
    })
    
	submit = function (form, urlp) {

		$('#' + form).ajaxSubmit({
			headers: {
				'X-CSRF-TOKEN': token
			},
			url: urlp,
			type: 'POST',
			beforeSubmit: function (formData, jqForm, options) {
			},
			success: function (responseText, statusText, xhr, $form) {

				if(responseText.success){
			        $('#mQuotes').modal('hide')
                    Swal.fire({
                        position: "top-end",
                        icon: "success",
                        title: "Operación Exitosa!!!",
                        showConfirmButton: false,
                        timer: 2500
                    });

				}else{
                    Swal.fire({
                        position: "top-end",
                        icon: "error",
                        title: "Algo salio mal!",
                        showConfirmButton: false,
                        timer: 2500
                    });
				}
	
			},complete: function(){
				//$.LoadingOverlay("hide");
			}
		});
	
	}

    function changeStatus(id, status) {
        $.ajax({
            headers: {
				'X-CSRF-TOKEN': token
			},
            url: url + "/change_status",
            method: 'POST',
            data: { id: id, status: status },
            success: function(response) {
                
                var table = $('.table');
			    var oTable = table.dataTable();
				oTable.DataTable().ajax.reload();

                Swal.fire({
                    position: "top-end",
                    icon: "success",
                    title: "Operación Exitosa!!!",
                    showConfirmButton: false,
                    timer: 2500
                });
            },
            error: function(error) {
                // Manejo de errores
            }
        });
    }
    
    $('#dtTableCandidates').empty();
    initDataTable('all');

    $('#filter').on('change', function(){
        $('#dtTableCandidates').empty();
        initDataTable($(this).val());
    });
</script>
@endsection

