{{-- Extends layout --}}
@extends('layout.default')
@section('title',__($_title))
  {{-- Styles Section --}}
@section('styles')
  <link rel="stylesheet" href="{{asset('css/therapist/style-edit.css')}}">
  
  <style>
        .loading-overlay {
        position: fixed;
        top: 0;
        left: 0;
        width: 100%;
        height: 100%;
        background-color: rgba(0, 0, 0, 0.5);
        display: flex;
        justify-content: center;
        align-items: center;
        flex-direction: column;
        z-index: 9999;
    }

    .spinner {
        border: 4px solid #f3f3f3;
        border-top: 4px solid #3498db;
        border-radius: 50%;
        width: 40px;
        height: 40px;
        animation: spin 1s linear infinite;
    }
    .loading-text {
        margin-top: 10px;
        color: white;
        font-size: 16px;
    }

    @keyframes spin {
        0% {
            transform: rotate(0deg);
        }
        100% {
            transform: rotate(360deg);
        }
    }

    .hidden {
        display: none;
    }

    .avatar-placeholder {
        position: relative;
        display: inline-block;
        box-shadow: 2px 2px 2px 1px rgba(0, 0, 0, 0.3);
    }

    .edit-icon {
        position: absolute;
        top: -8px;
        right: -9px;
        font-size: 20px;
        color: #000;
        cursor: pointer;
            background: white;
        padding: 5px;
        border-radius: 31px;
        box-shadow: 2px 2px 2px 1px rgba(0, 0, 0, 0.2);
        height: 31px;
    }
    .pencil-container {
 /*        width: 50px;
        height: 50px;
        border-radius: 50%;
        background-color: red; */
    }
  </style>
@endsection

@section('content')

{!! Form::open()->put()->url(url($_module_route."/".$obj->id))->fill($obj) !!}
<div class="card">
    <div class="card-body">
        <div class="text-right">
            <a class="btn btn-dark mt-1" href="{{ url($_module_route) }}">
                Volver al listado
            </a>
        </div>
             @include('modules.admin.candidates._form-edit')
        
        <div class="col-12 d-flex justify-content-end">
            <button type="submit" class="btn btn-primary mr-1 mb-1">Actualizar</button>
        </div>
    </div>
</div>
{!! Form::close() !!}
@endsection

{{-- Styles Section --}}
@section('styles')
@endsection

{{-- Scripts Section --}}
@section('scripts')
<script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/jquery.validate.min.js"></script>
<script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/additional-methods.min.js"></script>
</script>
<script src="{{ asset('js/messages_es.js') }}"></script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@11.0.19/dist/sweetalert2.all.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
<script src="https://unpkg.com/@popperjs/core@2"></script>
<script src="https://unpkg.com/tippy.js@6"></script>
<script src="/js/ktavatar.js"></script>
<script src="{{ asset('js/functions.js') }}"></script>
<script>
    const url = '{{url($_module_route)}}'
    CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
    let token = '{{ csrf_token() }}'
    const urlBase  = "{{URL::to('/')}}";

    var starValue = '{{$obj->qualification}}'
        
        $('.rating .star').each(function() {
            var valor = parseInt($(this).data('value')); 
            
            if (valor <= starValue) {
                $(this).addClass('gold'); 
            }
        });
    
</script>
<script src="{{ asset('js/therapists/therapists.js') }}"></script>

@endsection