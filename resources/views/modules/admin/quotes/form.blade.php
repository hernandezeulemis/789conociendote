<div class="form-row">
    <div class="col-md-4">
        <label for="">Nombre Completo</label>
        <input type="text" name="full_name" id="full_name" class="form-control" value="{{($obj->therapist) ? $obj->therapist->full_name : ''}}">
    </div>
    <div class="col-md-4">
        {!! Form::date("date","Fecha *") !!}
    </div>
    <div class="col-md-4">
        {!! Form::time("hour","Hora *") !!}
    </div>
</div>


