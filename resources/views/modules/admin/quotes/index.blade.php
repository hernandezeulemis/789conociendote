{{-- Extends layout --}}
@extends('layout.default')
@section('title',__($_title))
{{-- Content --}}
<style>
        .error {
            color: red;
        }
    </style>
@section('content')
    <div class="card">
        <div class="card-body">
            <div class="row">
                <div class="col-md-4">
                    <label for=""><b>STATUS</b></label>
                    <select name="filter" id="filter" class="form-control">
                        <option value="all">Todos</option>
                        <option value="pendiente">Pendiente</option>
                        <option value="lograda">Lograda</option>
                        <option value="cancelada">Cancelada</option>
                        <option value="reagendada">Reagendada</option>
                    </select>
                </div>

            </div>
            
            <div class="row">
                <div class="col-md-12">
                    <table class="display table table-striped table-bordered" id="dtQuote"
                           style="width:100%"></table>
                </div>
            </div>
        </div>
    </div>
    @include('modules.admin.candidates.modals.modalQuotes')
@endsection

{{-- Styles Section --}}
@section('styles')

@endsection

{{-- Scripts Section --}}
@section('scripts')
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-loading-overlay/2.1.7/loadingoverlay.min.js" ></script>
<script src="https://malsup.github.io/jquery.form.js"></script> 
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.20.0/jquery.validate.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.20.0/additional-methods.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.20.0/localization/messages_es.min.js"></script>
<script src="{{ asset('js/functions.js') }}"></script>
<script>
    let param = '';
    const url = '{{url($_module_route)}}'
    const token = '{{ csrf_token() }}'
    const buttonsCrud = (id) => {
        return `
        <div class="btn-group" role="group">
            ${ btnVer( id,url ) }
            
        </div>
        `
    }
    var initDataTable = function(status){
        var table = $('#dtQuote');
        var oTable = table.dataTable({
            destroy: true,
            "loadingMessage": 'Cargando...',
            "ajax": {
               url:  url + "/get_data/" +status,
               "dataSrc": "list",
               beforeSend:function(){
                   //$.LoadingOverlay("show");
               },complete:function(data){

                     if(data.responseJSON.list.length > 0){
                        
                     }
                     
                   //$.LoadingOverlay("hide");
               },
            },
            "createdRow": function ( row, data, index ) {
            },
            "columns": [
                {
                    data: 'id', 
                    name: 'id', 
                    title: 'ID',
                    visible:false
                },
                {
                    data: 'therapist.full_name', 
                    name: 'name', 
                    title: 'Nombre'
                },
                {
                    data: 'date', 
                    name: 'date', 
                    title: 'Fecha',
                },
                {
                    data: 'hour', 
                    name: 'hour', 
                    title: 'Hora',
                },
                {
                    data: 'status', 
                    name: 'status', 
                    title: 'Estatus',
                            render: function (data, type, row) {
                                switch (data) {
                                    case 'lograda':
                                        return '<span class="badge badge-success">Lograda</span>';
                                        break;
                                    case 'cancelada':
                                        return '<span class="badge badge-danger">Cancelada</span>';
                                        break;
                                    case 'reagendada':
                                        return '<span class="badge badge-primary">Reagendada</span>';
                                        break;
                                    case 'pendiente':
                                        return '<span class="badge badge-warning">Pendiente</span>';
                                        break;
                                }
                                
                            }
                },
                {
                    title: 'Acciones', 'searchable': false,
                    data: 'status',
                    className: 'text-center',
                            render: function (data, type, row) {
                                //console.log(row, type, data)

                                var boton = '<div class="btn-group">'+
                                            '<div class="dropdown">' +
                                            '<button class="btn btn-info dropdown-toggle" type="button" id="statusDropdown" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Cambiar Status</button>' +
                                            '<div class="dropdown-menu" aria-labelledby="statusDropdown">' +
                                            '<a class="dropdown-item" href="#" onclick="changeStatus(' + row.id + ', \'pendiente\')">Pendiente</a>'+
                                            '<a class="dropdown-item" href="#" onclick="changeStatus(' + row.id + ', \'lograda\')">Lograda</a>'+
                                            '<a class="dropdown-item" href="#" onclick="changeStatus(' + row.id + ', \'cancelada\')">Cancelada</a>'+
                                            '<a class="dropdown-item" href="#" onclick="changeStatus(' + row.id + ', \'reagendada\')">Reagendada</a>'+
                                            '</div></div></div>';

                                
                                var btCita = '<a class="btn btn-xs btn-primary" id="btCita" style="" title="Reagendar Cita" onclick="reagendarCita(' + row.id + ')">' +
                                            '<i class="fa fa-calendar"></i>' +
                                            '</a>';

                                if(data == 'reagendada'){
                                    return boton + '  ' + btCita;
                                }else{
                                    return boton;
                                }
                                
                            }
                        
                }
            ],
            responsive: false,
            "order": [
                /*[0, 'asc']*/
            ],
            "lengthMenu": [
                [5, 10, 15, 20, -1],
                [5, 10, 15, 20, "All"] // change per page values here
            ],
            // set the initial value
            "pageLength": 10,
        });

        $('#dtQuote tbody').on('click', '#btNota', function() {
            var data = oTable.DataTable().row($(this).parents('tr')).data();

            

           
        });

        
    }
    /* const columns = [
        
    ] */
    
    function changeStatus(id, status) {
        $.ajax({
            headers: {
				'X-CSRF-TOKEN': token
			},
            url: url + "/change_status",
            method: 'POST',
            data: { id: id, status: status },
            success: function(response) {
                
                var table = $('.table');
			    var oTable = table.dataTable();
				oTable.DataTable().ajax.reload();

                Swal.fire({
                    position: "top-end",
                    icon: "success",
                    title: "Operación Exitosa!!!",
                    showConfirmButton: false,
                    timer: 2500
                });
            },
            error: function(error) {
                // Manejo de errores
            }
        });
    }


    function reagendarCita(id){

        $.get(url + "/get_quote/"+id, function(res) {
            $('#quote_id').val(res.id)
            $('.user_id').val(res.user_id)
            $('.therapist_id').val(res.therapist_id)
            $('#date').val(res.date)
            $('#hour').val(res.hour)
            

        });
        
            $('#mQuotes').modal('show')
            
    }

    $('#submitQuotes').on('click',function(){
        var form = 'FormQuotes';
        var urlp = url + "/save_quotes";
        var validator = $('#FormQuotes').validate({
            errorClass: "error",
                errorElement: "span",
                errorPlacement: function(error, element) {
                    error.appendTo(element.parent());
                }
        });
		
        if(validator.form()){
            submit(form,urlp);
            $('#mQuotes').modal('hide')
            
            var table = $('.table');
			var oTable = table.dataTable();
				oTable.DataTable().ajax.reload();
        }
        
    })

    $('#filter').on('change', function(){
        $('.table').empty();
        param = $(this).val()
        initDataTable(param)
    });

    param = 'all'
    initDataTable(param)

    submit = function (form, urlp) {
		$('#' + form).ajaxSubmit({
			headers: {
				'X-CSRF-TOKEN': token
			},
			url: urlp,
			type: 'POST',
			beforeSubmit: function (formData, jqForm, options) {
			},
			success: function (responseText, statusText, xhr, $form) {
				if(responseText.success){
			        $('#mQuotes').modal('hide')
                    Swal.fire({
                        position: "top-end",
                        icon: "success",
                        title: "Operación Exitosa!!!",
                        showConfirmButton: false,
                        timer: 2500
                    });
				}else{
                    Swal.fire({
                        position: "top-end",
                        icon: "error",
                        title: "Algo salio mal!",
                        showConfirmButton: false,
                        timer: 2500
                    });
				}
			},complete: function(){
				$.LoadingOverlay("hide");
			}
		});

	}
</script>
@endsection

