<div class="form-row">
    <div class="col-md-4">
        {!! Form::text("name","Nombre *") !!}
    </div>
    <div class="col-md-4">
        {!! Form::date("date","Fecha *") !!}
    </div>
    <div class="col-md-4">
        {!! Form::time("hour","Hora *") !!}
    </div>
    <div class="col-md-4">
    <select name="status" id="" class="form-control">
        <option value="">Seleccione...</option>
        <option value="pendiente">Pendiente</option>
        <option value="lograda">Lograda</option>
        <option value="cancelada">Cancelada</option>
        <option value="reagendada">Reagendada</option>
    </select>
    </div>
</div>


