{{-- Extends layout --}}
@extends('layout.default')
@section('title',__($_title))
@section('styles')
  <link rel="stylesheet" href="{{asset('css/therapist/style-edit.css')}}">
@endsection


{{-- Content --}}
@section('content')
<link rel="" href="https://cdnjs.cloudflare.com/ajax/libs/lightgallery/2.7.2/css/lg-comments.min.css"/>
{!! Form::open()->post()->url(route('update_profile_therapist'))->fill($obj) !!}
<div class="card">
 
        <div class="card-body">
            <!-- <img src="{{ asset('/new-front/images/dashboard/user_icon.png') }}" alt=""><br> -->
            <div class="cont-title">
                <h2 class="text-left title-mod-green" >Mi Perfil TERAPEUTA</h2>
            </div>
            <br>
             @include('modules.admin.candidates._form-edit')
             <div class="col-12 d-flex justify-content-end">
                <button type="submit" class="btn btn-primary mr-1 mb-1">Actualizar</button>
            </div>
      </div>
@endsection
@section("scripts")
<script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/jquery.validate.min.js"></script>
<script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/additional-methods.min.js"></script>
</script>
<script src="{{ asset('js/messages_es.js') }}"></script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@11.0.19/dist/sweetalert2.all.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
<script src="https://unpkg.com/tippy.js@6"></script>
<script src="/js/ktavatar.js"></script>
<script src="{{ asset('js/functions.js') }}"></script>
<script>
    const url = '{{url($_module_route)}}'
    CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
    let token = '{{ csrf_token() }}'
    const urlBase  = "{{URL::to('/')}}";
</script>
<script src="{{ asset('js/therapists/therapists.js') }}"></script>
@endsection
