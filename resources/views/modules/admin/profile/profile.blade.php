{{-- Extends layout --}}
@extends('layout.default')
@section('title',__($_title))
{{-- Content --}}
@section('content')
<div class="card">
        <div class="card-body">
            <!-- <img src="{{ asset('/new-front/images/dashboard/user_icon.png') }}" alt=""><br> -->
            <div class="cont-title">
                <h2 class="text-left title-mod-green" >Mi Perfil</h2><p class="sub-title-mod">{{$user->name}}</p>
            </div>
            <br>
                
            <form class="form-to-send form-row" action="{{route('update_profile')}}" method="post">
                @csrf
                <input name="edit_user_id" type="hidden" value="{{$user->id}}" required>
                <div class="col-md-4">
                    <label><b class="text-danger">*</b> Nombre y apellido</label>
                    <div class="input-group mb-3">
                        <input name="name" type="text" class="form-control" placeholder="Requerido" value="{{$user->name}}" required>
                    </div>
                </div>
                <div class="col-md-4">
                    <label><b class="text-danger">*</b> Correo electrónico</label>
                    <div class="input-group mb-3">
                        <input name="email" type="email" class="form-control" placeholder="Requerido" value="{{$user->email}}" required>
                    </div>
                </div>                              
                <div class="col-md-4">
                    <label>Nueva contraseña</label>
                    <div class="input-group mb-3">
                        <input id="myInput" name="password" type="password" class="form-control" placeholder="Opcional">
                    </div>
                    <div class="mt-1">
                        <input type="checkbox" onclick="myFunction()"> <small>Mostrar nueva contraseña</small>
                    </div>
                </div>
                <div class="col-12 d-flex justify-content-end">
                    <button type="submit" class="btn btn-success mr-1 mb-1 btn-to-send">Actualizar</button>
                </div>
            </form>

        </div>
    </div>
@endsection
@section("scripts")
    <script>
        
        $(document).ready(function() {

            $('.form-to-send').submit(function() {
                $('.btn-to-send').prop('disabled',true);
            });

        });

        function myFunction() {

            var x = document.getElementById("myInput");

            if(x.type === "password") {
                x.type = "text";
            }else {
                x.type = "password";
            }

        }

    </script>
@endsection
