{{-- Extends layout --}}
@extends('layout.default')
@section('title',__($_title))
{{-- Styles Section --}}
@section('styles')
    <link rel="stylesheet" href="{{asset('css/therapist/style-view.css')}}">
@endsection
    
@section('content')

<div class="card">
    <div class="card-body">
        
        {!! Form::open()->get()->url("logo")->fill($obj) !!}
            @include('modules.admin.candidates._form-view')
        {!! Form::close() !!}
        
    </div>
</div>
    
@endsection





{{-- Scripts Section --}}
@section('scripts')
    <script>
        $('#specific_problems, #current_complementaries, #poblacion, #language').select2();
        $('#specific_problems, #current_complementaries, #poblacion, #language').select2({
            multiple: true
        });

        var starValue = '{{$obj->qualification}}'
        
        $('.rating .star').each(function() {
            var valor = parseInt($(this).data('value')); 
            
            if (valor <= starValue) {
                $(this).addClass('gold'); 
            }
        });
        
    </script>
@endsection