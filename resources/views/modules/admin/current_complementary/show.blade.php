{{-- Extends layout --}}
@extends('layout.default')
@section('title',__($_title))

@section('content')
    <div class="card">
        <div class="card-body">
            {!! Form::open()->get()->url("logo")->fill($obj) !!}
                @include('modules.'.$_module_route.'.form')
            {!! Form::close() !!}
        </div>
    </div>

@endsection



{{-- Styles Section --}}
@section('styles')

@endsection

{{-- Scripts Section --}}
@section('scripts')
    <script>
        $("input,textarea,select").attr("readonly", "readonly");
        $(".form-check-input").attr("disabled", true);
        $("#inp-password").parents('.form-group').css("display", "none");
        
    </script>
@endsection
