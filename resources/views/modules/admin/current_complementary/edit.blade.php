{{-- Extends layout --}}
@extends('layout.default')
@section('title',__($_title))


@section('content')
    {!! Form::open()->put()->url(url($_module_route."/".$obj->id))->fill($obj) !!}
    <div class="card">
        <div class="card-body">
            <div class="text-right">
                <a class="btn btn-dark mt-1" href="{{ url($_module_route) }}">
                    Volver al listado
                </a>
            </div>
            @include('modules.'.$_module_route.'.form')
            <div class="col-12 d-flex justify-content-end">
                <button type="submit" class="btn btn-primary mr-1 mb-1">{{__("Guardar")}}</button>
            </div>
        </div>
    </div>
    {!! Form::close() !!}
@endsection

{{-- Styles Section --}}
@section('styles')
@endsection

{{-- Scripts Section --}}
@section('scripts')
@endsection
