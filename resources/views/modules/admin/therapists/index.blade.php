{{-- Extends layout --}}
@extends('layout.default')
@section('title',__($_title))
{{-- Content --}}
<style>
        .error {
            color: red;
        }
    </style>
@section('content')
    <div class="card">
        <div class="card-body">
            <div class="row">
                <div class="col-md-4">
                    <label for=""><b>STATUS</b></label>
                    <select name="filter" id="filter" class="form-control">
                        <option value="all">Todos</option>
                        <option value="nuevo">Nuevo</option>
                        <option value="activo">Activo</option>
                        <option value="baja_temporal">Baja Temporal</option>
                        <option value="baja_definitiva">Baja Definitiva</option>
                        <option value="principiante">Principiante</option>                        
                    </select>
                </div>
            </div>
            <br>
            <div class="row">
                <div class="col-md-12">
                    <table class="display table table-striped table-bordered" id="dtTableCandidates" style="width:100%">
                    </table>
                </div>
            </div>
        </div>
    </div>
@include('modules.admin.therapists.modals.modal')
@endsection


{{-- Styles Section --}}
@section('styles')

@endsection

{{-- Scripts Section --}}
@section('scripts')

<script src="https://malsup.github.io/jquery.form.js"></script> 
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.20.0/jquery.validate.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.20.0/additional-methods.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.20.0/localization/messages_es.min.js"></script>

<script src="{{ asset('js/functions.js') }}"></script>

<script>
    
    const url = '{{url($_module_route)}}'
    const token = '{{ csrf_token() }}';
    const urlBase  = "{{URL::to('/')}}";
 
    var initDataTable = function(status){
        var table = $('#dtTableCandidates');
        var oTable = table.dataTable({
            destroy: true,
            "loadingMessage": 'Cargando...',
            "ajax": {
               url:  url + "/get_data/" +status,
               "dataSrc": "list",
               beforeSend:function(){
                   //$.LoadingOverlay("show");
               },complete:function(data){

                     if(data.responseJSON.list.length > 0){
                        
                     }
                     
                   //$.LoadingOverlay("hide");
               },
            },
            "createdRow": function ( row, data, index ) {
            },
            "columns": [
                { "data": "id", "title": "Id", "visible": true },
                { "data": "full_name", "title": "Nombre Completo del Candidato", "visible": true },
                { "data": "status_therapist", "title": "Status", "visible": true,
                    render: function (data, type, row) {
                        switch (data) {
                            case 'nuevo':
                                return '<span class="badge badge-success">Nuevo</span>';
                                break;
                            case 'activo':
                                return '<span class="badge badge-danger">Activo</span>';
                                break;
                            case 'baja_temporal':
                                return '<span class="badge badge-info">Baja Temporal</span>';
                                break;
                            case 'baja_definitiva':
                                return '<span class="badge badge-warnig">Baja Definitiva</span>';
                                break;
                            case 'principiante':
                                return '<span class="badge badge-primary">Principiante</span>';
                                break;
                            
                        }
                        
                    }
                },
                { "data": "user.status_sign", "title": "Status Contrato", "visible": true,
                    render: function (data, type, row) {
                        
                        switch (data) {
                            case 'pendiente':
                                return '<span class="badge badge-warning">Pendiente por Firmar</span>';
                                break;
                            case 'firmado':
                                return '<span class="badge badge-primary">Firmado</span>';
                                break;
                            default : 
                            return '';                            
                        }
                        
                    }
                },
                { "data": "", "title": "Acciones", "visible": true, "orderable": true, "witdh": "50%",
                    render: function (data, type, row) {
                        var boton = '<div class="btn-group">'+
                                    '<div class="dropdown">' +
                                    '<button style="height: 32px" class="btn btn-info dropdown-toggle" type="button" id="statusDropdown" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Cambiar Status</button>' +
                                    '<div class="dropdown-menu" aria-labelledby="statusDropdown">' +
                                    '<a class="dropdown-item" href="#" onclick="changeStatus(' + row.id + ', \'nuevo\')">Nuevo</a>'+
                                    '<a class="dropdown-item" href="#" onclick="changeStatus(' + row.id + ', \'activo\')">Activo</a>'+
                                    '<a class="dropdown-item" href="#" onclick="changeStatus(' + row.id + ', \'baja_temporal\')">Baja Temporal</a>'+
                                    '<a class="dropdown-item" href="#" onclick="changeStatus(' + row.id + ', \'baja_definitiva\')">Baja Definitiva</a>'+
                                    '<a class="dropdown-item" href="#" onclick="changeStatus(' + row.id + ', \'principiante\')">Principiante</a>'+
                                    '</div></div></div>';

                        var btnEditar   = '<a  class="btn btn-primary" href="'+ url +'/'+ row.id +'/edit" style="  width: 44px;height: 32px"> <i class="fa fa-edit fa-1x"  ></i></a>';
                        
                        var btnVer      = '<a  class="btn btn-info" href="'+ url +'/'+ row.id +'" style="background-color: #e7a4a2; border-color:#e7a4a2; width: 44px;height: 32px"> <i class="fa fa-eye"  style="font-size:14px; text-align:center"></i></a>';

                        var btNota =   '<a class="btn btn-xs btn-warning" id="btNota">'+
                                        '<i class="fa fa-sticky-note" style="color:white" title="Notas"></i>'+
                                        '</a>';
                        var btnVerContrats = '<a  class="btn btn-dark" href="'+ urlBase +'/admin/contrato_by_id/'+ row.id +'" target="_blanck" style="background-color: #45c1a2; border-color:#45c1a2; width: 44px;height: 32px"> <i class="flaticon2-contract"  style="font-size:14px; text-align:center" title="Ver Contrato" target="_blank"></i></a>';
                       
                        return boton + '  ' + '  ' + btnVer + '  ' + btnEditar + '  ' + btNota + '  ' + btnVerContrats ;
                        


                        
                    }
                }
            ],
            responsive: false,
            "order": [
                /*[0, 'asc']*/
            ],
            "lengthMenu": [
                [5, 10, 15, 20, -1],
                [5, 10, 15, 20, "All"] // change per page values here
            ],
            // set the initial value
            "pageLength": 10,
        });

        $('#dtTableCandidates tbody').on('click', '#btNota', function() {
            var data = oTable.DataTable().row($(this).parents('tr')).data();

            if (this.id == "btNota") {
                $('.therapist_id').val(data.id)
                $('#exampleModal').modal('show')
                initDataTableNotes(data.id)
            }

           
        });

        
    }
     var initDataTableNotes = function(therapist_id){
        var table = $('#dtTableNotes');
        var oTable = table.dataTable({
            destroy: true,
            "loadingMessage": 'Cargando...',
            "ajax": {
               url:  url + "/get_note_for_therapist_id/" +therapist_id,
               "dataSrc": "list",
               beforeSend:function(){
                   //$.LoadingOverlay("show");
               },complete:function(data){

                     if(data.responseJSON.list.length > 0){
                        
                     }
                     
                   //$.LoadingOverlay("hide");
               },
            },
            "createdRow": function ( row, data, index ) {
            },
            "columns": [
                { "data": "id", "title": "Id", "visible": true },
                { "data": "note", "title": "Nota", "visible": true },
/* 
                { "data": "", "title": "Acciones", "visible": true, "orderable": true, "witdh": "50%",
                    render: function (data, type, row) {
                        var boton = '<div class="btn-group">'+
                                    '<div class="dropdown">' +
                                    '<button style="height: 32px" class="btn btn-info dropdown-toggle" type="button" id="statusDropdown" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Cambiar Status</button>' +
                                    '<div class="dropdown-menu" aria-labelledby="statusDropdown">' +
                                    '<a class="dropdown-item" href="#" onclick="changeStatus(' + row.id + ', \'nuevo\')">Nuevo</a>'+
                                    '<a class="dropdown-item" href="#" onclick="changeStatus(' + row.id + ', \'activo\')">Activo</a>'+
                                    '<a class="dropdown-item" href="#" onclick="changeStatus(' + row.id + ', \'baja_temporal\')">Baja Temporal</a>'+
                                    '<a class="dropdown-item" href="#" onclick="changeStatus(' + row.id + ', \'baja_definitiva\')">Baja Definitiva</a>'+
                                    '<a class="dropdown-item" href="#" onclick="changeStatus(' + row.id + ', \'principiante\')">Principiante</a>'+
                                    '</div></div></div>';

                        var btnEditar   = '<a  class="btn btn-primary" href="'+ url +'/'+ row.id +'/edit" style="  width: 44px;height: 32px"> <i class="fa fa-edit fa-1x"  ></i></a>';
                        
                        var btnVer      = '<a  class="btn btn-info" href="'+ url +'/'+ row.id +'" style="background-color: #e7a4a2; border-color:#e7a4a2; width: 44px;height: 32px"> <i class="fa fa-eye"  style="font-size:14px; text-align:center"></i></a>';

                        var btNota =   '<a class="btn btn-xs btn-warning" id="btNota">'+
                                        '<i class="fa fa-sticky-note" style="color:white" title="Notas"></i>'+
                                        '</a>';
                       
                        return boton + '  ' + '  ' + btnVer + '  ' + btnEditar + '  ' + btNota;
                        


                        
                    }
                } */
            ],
            responsive: false,
            "order": [
                /*[0, 'asc']*/
            ],
            "lengthMenu": [
                [5, 10, 15, 20, -1],
                [5, 10, 15, 20, "All"] // change per page values here
            ],
            // set the initial value
            "pageLength": 10,
        });

        $('#dtTableCandidates tbody').on('click', '#btNota', function() {
            var data = oTable.DataTable().row($(this).parents('tr')).data();

            if (this.id == "btNota") {
                $('.therapist_id').val(data.id)
                $('#exampleModal').modal('show')
                
            }

           
        });

        
    }

    function changeStatus(id, status) {
        $.ajax({
            headers: {
				'X-CSRF-TOKEN': token
			},
            url: url + "/change_status",
            method: 'POST',
            data: { id: id, status: status },
            success: function(response) {
                
                var table = $('.table');
			    var oTable = table.dataTable();
				oTable.DataTable().ajax.reload();

                Swal.fire({
                    position: "top-end",
                    icon: "success",
                    title: "Operación Exitosa!!!",
                    showConfirmButton: false,
                    timer: 2500
                });
            },
            error: function(error) {
                // Manejo de errores
            }
        });
    }
    
    $('#submitContact').on('click',function(){
        var form = 'FormContact';
        var urlp = url + "/save_nota";
        var validator = $('#FormContact').validate({
            errorClass: "error",
                errorElement: "span",
                errorPlacement: function(error, element) {
                    error.appendTo(element.parent());
                }
        });
		
        if(validator.form()){
            submit(form,urlp);

            var table = $('.table');
			var oTable = table.dataTable();
				oTable.DataTable().ajax.reload();
        }
        
    })
    
	submit = function (form, urlp) {

		$('#' + form).ajaxSubmit({
			headers: {
				'X-CSRF-TOKEN': token
			},
			url: urlp,
			type: 'POST',
			beforeSubmit: function (formData, jqForm, options) {
			},
			success: function (responseText, statusText, xhr, $form) {

				if(responseText.success){
			        $('#mQuotes').modal('hide')
                     initDataTableNotes($('.therapist_id').val())
                    Swal.fire({
                        position: "top-end",
                        icon: "success",
                        title: "Operación Exitosa!!!",
                        showConfirmButton: false,
                        timer: 2500
                    });

				}else{
                    Swal.fire({
                        position: "top-end",
                        icon: "error",
                        title: "Algo salio mal!",
                        showConfirmButton: false,
                        timer: 2500
                    });
				}
	
			},complete: function(){
				//$.LoadingOverlay("hide");
			}
		});
	
	}

    $('#dtTableCandidates').empty();
    initDataTable('all');

    $('#filter').on('change', function(){
        $('#dtTableCandidates').empty();
        initDataTable($(this).val());
    });
</script>
@endsection

