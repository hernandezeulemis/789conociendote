<div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Agregar Nota</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form id="FormContact" action="#" role="form" style="display: contents;">
            <div class="form-group hide">
              <input type="hidden" class="therapist_id" name="therapist_id">
            </div>
            <div class="form-group">
                <div class="col-12">
                    <textarea  placeholder="Nota" class="form-control" name="note" id="nombrePaciente" required></textarea>
                </div>
            </div>
        </form>
        <div class="row">
            <div class="col-md-12">
                <table class="display table table-striped table-bordered" id="dtTableNotes" style="width:100%">
                </table>
            </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
        <button type="button" class="btn btn-primary" id="submitContact">Guardar</button>
      </div>
    </div>
  </div>
</div>