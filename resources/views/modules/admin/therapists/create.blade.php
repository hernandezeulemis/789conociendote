{{-- Extends layout --}}
@extends('layout.default')
@section('title',__($_title))
{{-- Content --}}

@section('content')
<form method="POST" enctype="multipart/form-data" name="form-dropzone" id="form-dropzone" action="{{ url($_module_route) }}">
    @csrf
    <div class="card">
        <div class="card-body">
            @include('modules.'.$_module_route.'.form')
        
            <div class="col-12 d-flex justify-content-end">
                <button type="submit" id="save" class="btn btn-primary mr-1 mb-1">{{__("Guardar")}}</button>
            </div>
        </div>
    </div>
</form>
@endsection


