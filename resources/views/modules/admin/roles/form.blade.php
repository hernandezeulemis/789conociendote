<div class="form-row">
    <div class="col-md-4">
        {!! Form::text("name",__("Nombre del permiso *")) !!}
    </div>
    <div class="col-md-12">
        <h3>Permisos</h3>
    </div>
    @foreach($permissions as $permission)
        <div class="col-md-3">
            {!!Form::checkbox('names[]', $permission->name,$permission->id)->id($permission->id)->checked( $obj->permissions && $obj->permissions->contains('id',$permission->id) )!!}
        </div>
    @endforeach
</div>
