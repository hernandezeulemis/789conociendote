{{-- Extends layout --}}
@extends('layout.default')
@section('title',__($_title))
{{-- Content --}}
@section('content')
    <div class="card">
        <div class="card-body">
            @can('crear roles')
                <div class="row">
                    <div class="col-md-12">
                        <a class="btn btn-success btn-large border" style="margin: 10px;"
                        href="{{url($_module_route)}}/create">{{__('Agregar')}}</a>
                    </div>
                </div>
            @endcan
            <div class="row">
                <div class="col-md-12">
                    <table class="display table table-striped table-bordered"
                        style="width:100%"></table>
                </div>
            </div>
        </div>
    </div>

@endsection

{{-- Styles Section --}}
@section('styles')

@endsection

{{-- Scripts Section --}}
@section('scripts')
<script src="{{ asset('js/functions.js') }}"></script>
<script>
    
    const url = '{{url($_module_route)}}'
    const token = '{{ csrf_token() }}'

    const buttonsCrud = (id) => {
        return `
        <div class="btn-group" role="group">
            ${ btnEditar( id,url )  }
            ${ btnBorrar( id, url, token ) }
        </div>
        `
    }

    const columns = [
        {
            data: 'id', 
            name: 'id', 
            title: 'ID'
        },
        {
            data: 'created_at', 
            name: 'created_at', 
            title: 'Fecha',
            render: function(created_at) {
                return moment(created_at).format('YYYY-MM-DD')
            }
        },
        {
            data: 'name', 
            name: 'name', 
            title: 'Nombre'
        },
        {
            data: 'permissions', 
            name: 'permissions', 
            title: 'Permisos',
            className: 'text-center',
            render: data => data.length
        },
        {
            title: 'Acciones', 'searchable': false,
            data: 'id',
            className: 'text-center',
            render: buttonsCrud 
        }
    ]

    datatables({columns,url})
    
</script>
@endsection

