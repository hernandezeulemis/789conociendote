{{-- Extends layout --}}
@extends('layout.default')
@section('title',__($_title))
{{-- Content --}}
@section('content')
    <div class="card">
        <div class="card-body">
            {!! Form::open()->post()->url(url($_module_route)) !!}
                @include('modules.'.$_module_route.'.form')

                <div class="col-12 d-flex justify-content-end">
                    <button type="submit" class="btn btn-primary mr-1 mb-1">{{__("Guardar")}}</button>
                </div>
            {!! Form::close() !!}
        </div>
    </div>
@endsection


{{-- Styles Section --}}
@section('styles')
@endsection

{{-- Scripts Section --}}
@section('scripts')
@endsection

