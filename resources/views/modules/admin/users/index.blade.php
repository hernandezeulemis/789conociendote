{{-- Extends layout --}}
@extends('layout.default')
@section('title',__($_title))
{{-- Content --}}
@section('content')
    <div class="card">
        <div class="card-body">
            @can('crear usuarios')
            <div class="row">
                <div class="col-md-12">
                    <a class="btn btn-success btn-large border" style="margin: 10px;"
                       href="{{url($_module_route)}}/create">{{__('Agregar')}}</a>
                </div>
            </div>
            @endcan
            <div class="row">
                <div class="col-md-12">
                    <table class="display table table-striped table-bordered" style="width:100%"></table>
                </div>
            </div>
        </div>
    </div>

@endsection

{{-- Styles Section --}}
@section('styles')

@endsection

{{-- Scripts Section --}}
@section('scripts')
<script src="{{ asset('js/functions.js') }}"></script>
<script>
    
    const canShow = ['ver usuarios']
    const canEdit = ['editar usuarios']
    const canDelete = ['eliminar usuarios']
    const url = '{{url($_module_route)}}'
    const token = '{{ csrf_token() }}'
    const buttonsCrud = (id) => {
        return `
        <div class="btn-group" role="group">
            ${ canPermission(canShow) ? btnVer( id,url ) : '' }
            ${ canPermission(canEdit) ? btnEditar( id, url ) : '' }
            ${ canPermission(canDelete) ? btnBorrar( id, url, token ) : '' }
        </div>
        `
    }

    const columns = [
        {
            data: 'id', 
            name: 'id', 
            title: 'ID'
        },
        {
            data: 'created_at', 
            name: 'created_at', 
            title: 'Fecha',
            render: function(created_at) {
                return moment(created_at).format('YYYY-MM-DD')
            }
        },
        {
            data: 'name', 
            name: 'name', 
            title: 'Nombre'
        },
        {
            data: 'email', 
            name: 'email', 
            title: 'Correo electrónico'
        },
        {
            title: 'Acciones', 'searchable': false,
            data: 'id',
            className: 'text-center',
            visible: canPermission([...canShow,...canEdit,...canDelete]),
            render: buttonsCrud 
        }
    ]

    datatables({columns,url,excel:false})


    const labels = [
        'Enero',
        'Febrero',
        'Marzo',
        'Abril',
        'Mayo',
        'Junio',
    ];
    
    const data = {
        labels: labels,
        datasets: [{
            label: 'Registro de usuarios',
            backgroundColor: 'rgb(54 153 255)',
            borderColor: 'rgb(54 153 255)',
            data: [0, 10, 5, 2, 20, 30, 45],
        }]
    };

    
</script>
@endsection

