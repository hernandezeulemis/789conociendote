{{-- Styles Section --}}
@section('styles')
    <link rel="stylesheet" href="{{ asset('css/custom-starter/dropzone.css') }}" />
@endsection
<div class="card">
    <div class="card-body">
        <div class="form-row">
            <div class="col-md-4">
                {!! Form::text("name","Nombre *") !!}
            </div>
            <div class="col-md-4">
                {!! Form::text('email', 'Correo Electrónico *')->type('email') !!}
            </div>
                
            <div class="col-md-4">
                {!! Form::text("password","Contraseña *")->type('password') !!}
            </div>
            
            @foreach($roles as $role)
                <div class="col-md-3">
                    {!!Form::checkbox('roles[]', $role->name,$role->id)->id($role->id)->checked( $obj->roles && $obj->roles->contains('id',$role->id) )!!}
                    <ol>
                        @foreach($role->permissions as $permission)
                            <li>
                                {{$permission->name}}
                            </li>
                        @endforeach
                    </ol>
                </div>
            @endforeach
            
            
        </div>
    </div>
</div>


{{-- Scripts Section --}}
@section('scripts')
<script src="{{ asset('js/custom-starter/dropzone.js') }}"></script>
<script src="{{ asset('js/custom-starter/initial-dropzone.js') }}"></script>


<script type="text/javascript">
    let token = '{{ csrf_token() }}'
    //initialDropzone('dropzoneForm','form-dropzone','#save')
  </script>
@endsection

