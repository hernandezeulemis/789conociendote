{{-- Extends layout --}}
@extends('layout.default')
@section('title',__($_title))

@section('content')
    {!! Form::open()->get()->url("nogo")->fill($obj) !!}
    @include('modules.'.$_module_route.'.form')
    {!! Form::close() !!}
    
    
@endsection



{{-- Styles Section --}}
@section('styles')

@endsection

{{-- Scripts Section --}}
@section('scripts')
    <script>
        $("input,textarea,select").attr("readonly", "readonly");
        $(".form-check-input").attr("disabled", true);
        $("#inp-password").parents('.form-group').css("display", "none");
        
    </script>
@endsection
