<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <style>
        body {
            font-family: Arial, Helvetica, sans-serif;
        }

        .card{
            float: left;
            width: 100%;
            border-radius: 5px;
            border: 1px solid black;
            margin-left: 5px;
        }
        .card-title{
            background-color: black;
            color: white;
            text-align: center;
        }
        .card-content{
            padding: 3px;
            width: 100%;
            font-size: 11px
        }

        .check-container > span {
            display: inline-block;
            margin-top: 5px;
            font-size: 11px;
        }

        .square{
            width: 10px;
            height: 10px;
            border: 1px solid black;
            margin-top: 5px;
            display: inline-block;
            font-size: 9px;
            text-align: center;
            font-weight: bolder;
        }

        .container-option > .check-container{
            display: inline;
        }


    </style>
</head>
<body>

<table style="width: 100%">
    <tr>
        <td><img src="{{asset('images/logo.jpg')}}" alt="" width="150px"></td>
        <td></td>
        <td style="text-align: right; font-size: 14px" rowspan="2">
            <br>Date: {{$date}}
            <br>Sales Rep: {{$data->sales_person ?? ''}}
            <br>Po Number: {{$data->account->po_number ?? ''}}
        </td>
    </tr>
    <tr>
        <td></td>
        <td style="text-align:left; vertical-align: bottom"><h3>Maintenance Agreement</h3></td>
        <td></td>
    </tr>
</table>
<table style="width: 100%">
    <tr>
        <td>
            <div class="card">
                <div class="card-title">Bill To</div>
                <div class="card-content">
                    {{$data->account->company_legal_name ?? ''}}
                    <br>{{$data->account->address1 ?? ''.' '.$data->account->address2 ?? ''}}
                    <br>{{$data->account->state ?? ''}}, {{$data->account->city ?? ''}} {{$data->account->zipcode ?? ''}}
                </div>
            </div>
        </td>
        <td>
            <div class="card">
                <div class="card-title">Installation Location</div>
                <div class="card-content">
                    @if( count($data->installation_info->locations) > 1)
                        MULTIPLE
                    @else
                        @if(count($data->installation_info->locations) > 0)
                            {{$data->installation_info->locations[0]->name ?? ''}}
                            <br>{{$data->installation_info->locations[0]->address1 ?? ''.' '.$data->installation_info->locations[0]->address2 ?? ''}}
                            <br>{{$data->installation_info->locations[0]->state ?? ''}}, {{$data->installation_info->locations[0]->city ?? ''}} {{$data->installation_info->locations[0]->zipcode ?? ''}}
                        @endif
                    @endif
                </div>
            </div>
        </td>
    </tr>
</table>

<table style="width: 100%; margin-top: 100px">
    <tr>
        @if ($data->billing_contact->first_name != $data->it_contact->first_name)
        <td>
                <div class="card">
                    <div class="card-title">Billing Contact</div>
                    <div class="card-content">
                        {{$data->billing_contact->first_name ?? ''}} {{$data->billing_contact->last_name ?? ''}},  {{$data->billing_contact->title ?? ''}}
                        <br>{{$data->billing_contact->phone ?? ''}}
                        <br>{{$data->billing_contact->email ?? ''}}
                    </div>
                </div>
        </td>
        @endif
        <td>
            <div class="card">
                <div class="card-title">IT Contact</div>
                <div class="card-content">
                    {{$data->it_contact->first_name ?? ''}} {{$data->it_contact->last_name ?? ''}}, {{$data->it_contact->title ?? ''}}
                    <br>{{$data->it_contact->phone ?? ''}}
                    <br>{{$data->it_contact->email ?? ''}}
                </div>
            </div>
        </td>
    </tr>
</table>

<table style="width: 100%; margin-top: 100px">
    <tr>
        <td style="width: 50%">
            @if ($data->tax_exempt->applies == 1)
                <div class="card">
                    <div class="card-title">Tax Exempt Info</div>
                    <div class="card-content">
                        {{$data->tax_exempt->number ?? ''}}
                    </div>
                </div>
            @endif
        </td>
        <td style="width: 50%">
            <div class="card">
                <div class="card-title">Meter Contact</div>
                <div class="card-content">
                     {{$data->meter_contact->first_name ?? ''}} {{$data->meter_contact->last_name ?? ''}}, {{$data->meter_contact->title ?? ''}}
                    <br>{{$data->meter_contact->phone ?? ''}}
                    <br>{{$data->meter_contact->email ?? ''}}
                </div>
            </div>
        </td>
    </tr>
</table>
<table style="width: 100%; margin-top: 0px">
    <tr>
        <td style="width: 50%">
            <div class="card">
                <div class="card-title">Base Billing</div>
                <div class="card-content">
                    Base billed by: {{$data->maintenance_agreement->base_billed_by ?? ''}}
                    @if($data->maintenance_agreement->base_billing_amount == 'Docutrend')
                        <br>Term of Agreement: {{$data->maintenance_agreement->term ?? ''}}
                    @else
                        <br>Term of Agreement: {{$data->payment_info->lease_term}}
                    @endif
                    <br>Base Billing Amount: {{$data->maintenance_agreement->base_billing_amount ?? ''}}
                    <br>Base Billing Frecuency: {{$data->maintenance_agreement->frecuency ?? ''}}
                    <br>Overage Billing Frecuency: {{$data->maintenance_agreement->overage_billing_frequency ?? ''}}
                </div>
            </div>
        </td>
        <td style="width: 50%">

        </td>
    </tr>
</table>


{{--ESTO SERA VARIABLE--}}


{{--OPCIÓN 1--}}
<table style="width: 100%; margin-top: 100px">
    <tr>
        <td>
            <strong style="font-size: 11px">Systems covered under this agreement</strong>
        </td>
    </tr>
</table>
<table style="width: 100%;   border-collapse: collapse; font-size: 11px">
    <thead>
    <tr>
        <td style="background-color: black; color: white; text-align: center">
            MAKE/MODEL
        </td>
        <td style="background-color: black; color: white; text-align: center">
            SERIAL #
        </td>
        <td style="background-color: black; color: white; text-align: center">
            LOCATION
        </td>
        {{-- <td style="background-color: black; color: white; text-align: center">
            QUANTITY
        </td> --}}
    </tr>
    </thead>
    @foreach($data->equipment as $equipment)
        <tr>
            <td style="border: 1px solid">
                {{$equipment->make_model}}
            </td>
            <td style="border: 1px solid">
                {{$equipment->mainframe_matrix->serial}}
            </td>
            <td style="border: 1px solid">
                {{$equipment->mainframe_matrix->location}}
            </td>
        </tr>
    @endforeach
</table>
<table style="width: 100%; border-collapse: collapse; font-size: 11px">
    <thead>
    <tr>
        <td style="background-color: black; color: white; text-align: center">
            COVERAGE TYPE
        </td>
        <td style="background-color: black; color: white; text-align: center">
            METER TYPE
        </td>
        <td style="background-color: black; color: white; text-align: center">
            ALLOWANCE/MONTH
        </td>
        <td style="background-color: black; color: white; text-align: center">
            OVERAGE RATE
        </td>
    </tr>
    </thead>
    @if($data->equipment[0]->mainframe_matrix->options[0]->meter_type)
        <tr>
            <td style="border: 1px solid">
                @if(count($data->equipment) > 0)
                    {{$data->equipment[0]->mainframe_matrix->options[0]->coverage_type}}
                @endif
            </td>
            <td style="border: 1px solid">
                @if(count($data->equipment) > 0)
                 {{$data->equipment[0]->mainframe_matrix->options[0]->meter_type}}
                @endif
            </td>
            <td style="border: 1px solid">
                @if(count($data->equipment) > 0)
                    {{$data->equipment[0]->mainframe_matrix->options[0]->allowance_month}}
                @endif
            </td>
            <td style="border: 1px solid">
                @if(count($data->equipment) > 0)
                    {{$data->equipment[0]->mainframe_matrix->options[0]->coverage_type}}
                @endif
            </td>
        </tr>
    @endif
    @if($data->equipment[0]->mainframe_matrix->options[1]->meter_type)
        <tr>
            <td style="border: 1px solid white; border-right-color: black">

            </td>
            <td style="border: 1px solid">
                @if(count($data->equipment) > 0)
                    {{$data->equipment[0]->mainframe_matrix->options[1]->meter_type}}
                @endif
            </td>
            <td style="border: 1px solid">
                @if(count($data->equipment) > 0)
                    {{$data->equipment[0]->mainframe_matrix->options[1]->allowance_month}}
                @endif
            </td>
            <td style="border: 1px solid">
                @if(count($data->equipment) > 0)
                    {{$data->equipment[0]->mainframe_matrix->options[1]->coverage_type}}
                @endif
            </td>
        </tr>
    @endif
    @if($data->equipment[0]->mainframe_matrix->options[2]->meter_type)
        <tr>
            <td style="border: 1px solid white; border-right-color: black">

            </td>
            <td style="border: 1px solid">
                @if(count($data->equipment) > 0)
                    {{$data->equipment[0]->mainframe_matrix->options[2]->meter_type}}
                @endif
            </td>
            <td style="border: 1px solid">
                @if(count($data->equipment) > 0)
                    {{$data->equipment[0]->mainframe_matrix->options[2]->allowance_month}}
                @endif
            </td>
            <td style="border: 1px solid">
                @if(count($data->equipment) > 0)
                    {{$data->equipment[0]->mainframe_matrix->options[2]->coverage_type}}
                @endif
            </td>
        </tr>
    @endif
    @if($data->equipment[0]->mainframe_matrix->options[2]->meter_type)
        <tr>
            <td style="border: 1px solid white; border-right-color: black">

            </td>
            <td style="border: 1px solid">
                @if(count($data->equipment) > 0)
                    {{$data->equipment[0]->mainframe_matrix->options[3]->meter_type}}
                @endif
            </td>
            <td style="border: 1px solid">
                @if(count($data->equipment) > 0)
                    {{$data->equipment[0]->mainframe_matrix->options[3]->allowance_month}}
                @endif
            </td>
            <td style="border: 1px solid">
                @if(count($data->equipment) > 0)
                    {{$data->equipment[0]->mainframe_matrix->options[3]->coverage_type}}
                @endif
            </td>
        </tr>
    @endif
</table>

{{----}}
<table>
    <tr>
        <td style="font-size: 11px"><u>Docutrend Data Collection Agent (DCA)</u></td>
    </tr>
    <tr>
        <td><span style="font-size: 11px">Docutrend is commited to providing exceptional customer support during the term of this agreement.
                Obtaining accurate,
                real-time information such as supply levels and meter readings is vital to provide this level of
                support.
                installation of our DCA will allow automatic meter reporting resulting in accurate billing and proactive
                toner management and replenishments.</span></td>
    </tr>
    <tr>
        <td>
            <div class="container-option">
                <span style="font-size: 11px; display: inline; margin-top: 5px">Accept Installation?</span>
                <div class="check-container">
                    <div class="square">&nbsp;</div>
                    <span>YES</span>
                </div>
                <div class="check-container">
                    <div class="square">&nbsp;</div>
                    <span>NO</span>
                </div>
                <span style="font-size: 11px; display: inline; margin-top: 5px">(check one)</span>
            </div>
        </td>
    </tr>
    <tr>
        <td><span style="font-size: 11px">If not accepted, who is the designated meter contact</span></td>
    </tr>
</table>
<table style="width:100%">
    <tr>
        <td style="border-top: 1px solid black"><span style="font-size: 11px">Name</span></td>
        <td style="border-top: 1px solid black"><span style="font-size: 11px">Email</span></td>
        <td style="border-top: 1px solid black"><span style="font-size: 11px">Phone</span></td>
    </tr>
    <tr>
        <td>
            <span style="font-size: 11px">In the event the DCA is not installed and meters are not reported when required, Docutrend will change
            a meter collection fee as per the maintenance agreement terms and conditions</span>
        </td>
    </tr>
    <tr>
        <td>
            <span style="font-size: 11px">This document is not valid without a signature from the client</span>
        </td>
    </tr>
</table>
<span style="font-size: 11px; font-weight:bold;">Contract Specifics</span>
<br><span style="font-size: 9px;">{{  $data->specifics }}</span>
<table style="width: 100%; border-collapse: collapse; margin-top: 50px">
    <tr>
        <td style="border: 2px solid; border-bottom-color: darkgoldenrod"><span style="color: red">X</span></td>
        <td style="border: 2px solid black; font-size: 11px; text-align: center;">{{-- {{$data->billing_contact->title ?? ''}} --}}</td>
        <td style="border: 2px solid black; font-size: 11px; text-align: center">{{-- {{$date}} --}}</td>
    </tr>
    <tr>
        <td style="border: 2px solid; border-bottom-color: black; font-size: 11px; text-align: center; border-right-color: white; border-left-color: white;">Authorized by (customer)</td>
        <td style="border: 2px solid; border-bottom-color: black; font-size: 11px; text-align: center; border-left-color: white; border-right-color: white;">Title</td>
        <td style="border: 2px solid; border-bottom-color: black; font-size: 11px; text-align: center; border-right-color: white;">Date</td>
    </tr>
</table>
<div class="page-break">
    <img src="{{ asset('docutrend/tac_ma.jpg') }}" alt="" width="100%" height="auto">
</div>
</body>
</html>
