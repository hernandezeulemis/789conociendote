<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <style>
        body {
            font-family: Arial, Helvetica, sans-serif;
        }

        .card{
            float: left;
            width: 100%;
            border-radius: 5px;
            border: 1px solid black;
            margin-left: 5px;
        }
        .card-title{
            background-color: black;
            color: white;
            text-align: center;
        }
        .card-content{
            padding: 3px;
            width: 100%;
            font-size: 11px
        }


    </style>
</head>
<body>

<table style="width: 100%">
    <tr>
        <td><img src="{{asset('images/logo.jpg')}}" alt="" width="150px"></td>
        <td></td>
        <td style="text-align: right; font-size: 14px" rowspan="2">
            <br>Date: {{$date}}
            <br>Sales Rep: {{$data->sales_person ?? ''}}
            <br>Po Number: {{$data->account->po_number ?? ''}}
        </td>
    </tr>
    <tr>
        <td></td>
        <td style="text-align:left; vertical-align: bottom"><h3>Sales Oder Agreement</h3></td>
        <td></td>
    </tr>
</table>
<table style="width: 100%">
    <tr>
        <td>
            <div class="card">
                <div class="card-title">Bill To</div>
                <div class="card-content">
                    {{$data->account->company_legal_name ?? ''}}
                    <br>{{$data->account->address1 ?? ''.' '.$data->account->address2 ?? ''}}
                    <br>{{$data->account->state ?? ''}}, {{$data->account->city ?? ''}} {{$data->account->zipcode ?? ''}}
                </div>
            </div>
        </td>
        <td>
            <div class="card">
                <div class="card-title">Installation Location</div>
                <div class="card-content">
                    @if( count($data->installation_info->locations) > 1)
                        MULTIPLE
                    @else
                        @if(count($data->installation_info->locations) > 0)
                            {{$data->installation_info->locations[0]->name ?? ''}}
                            <br>{{$data->installation_info->locations[0]->address1 ?? ''.' '.$data->installation_info->locations[0]->address2 ?? ''}}
                            <br>{{$data->installation_info->locations[0]->state ?? ''}}, {{$data->installation_info->locations[0]->city ?? ''}} {{$data->installation_info->locations[0]->zipcode ?? ''}}
                        @endif
                    @endif
                </div>
            </div>
        </td>
    </tr>
</table>


<table style="width: 100%; margin-top: 100px">
    <tr>
        @if ($data->billing_contact->first_name != $data->it_contact->first_name)
            <td>
                <div class="card">
                    <div class="card-title">Billing Contact</div>
                    <div class="card-content">
                        {{$data->billing_contact->first_name ?? ''}} {{$data->billing_contact->last_name ?? ''}},  {{$data->billing_contact->title ?? ''}}
                        <br>{{$data->billing_contact->phone ?? ''}}
                        <br>{{$data->billing_contact->email ?? ''}}
                    </div>
                </div>
            </td>
        @endif
            <td>
                <div class="card">
                    <div class="card-title">IT Contact</div>
                    <div class="card-content">
                        {{$data->it_contact->first_name ?? ''}}  {{$data->it_contact->last_name ?? ''}},  {{$data->it_contact->title ?? ''}}
                        <br>{{$data->it_contact->phone ?? ''}}
                        <br>{{$data->it_contact->email ?? ''}}
                    </div>
                </div>
            </td>
        </tr>
    </table>

<table style="width: 100%; margin-top: 100px">
    <tr>
        <td style="width: 50%">
            @if ($data->tax_exempt->applies == 1)
                <div class="card">
                    <div class="card-title">Tax Exempt Info</div>
                    <div class="card-content">
                    {{$data->tax_exempt->number ?? ''}}
                    </div>
                </div>
            @endif
        </td>
        <td style="width: 50%">
            <div class="card">
                <div class="card-title">Main Contact</div>
                <div class="card-content">
                    {{$data->meter_contact->first_name ?? ''}}  {{$data->meter_contact->last_name ?? ''}}, {{$data->meter_contact->title ?? ''}}
                    <br>{{$data->meter_contact->phone ?? ''}}
                    <br>{{$data->meter_contact->email ?? ''}}
                </div>
            </div>
        </td>
    </tr>
</table>

@if($data->sale_type == 'Purchase')
    <table style="width: 100%;  margin-top: 100px; border-collapse: collapse; font-size: 11px">
        <thead>
            <tr>
                <td style="background-color: black; color: white; text-align: center" colspan="3">
                    FINANCIAL INFORMATION
                </td>
        </thead>
        <tbody>
            <tr>
                <td style="border: 1px solid">
                    Purchase Amount: ${{$data->payment_info->purchase_amount ?? ''}}
                </td>
                <td style="border: 1px solid">
                    Tax: ${{$data->payment_info->tax ?? ''}}
                </td>
                <td style="border: 1px solid">
                    Total and Tax: ${{$data->payment_info->total_and_taxt ?? ''}}
                </td>
            </tr>
        </tbody>
    </table>
@elseif($data->sale_type == 'Lease')
    <table style="width: 100%;  margin-top: 100px; border-collapse: collapse; font-size: 11px">
        <thead>
            <tr>
                <td style="background-color: black; color: white; text-align: center" colspan="4">
                    FINANCIAL INFORMATION
                </td>
        </thead>
        <tbody>
            <tr>
                <td style="border: 1px solid">
                    Payment Amount: {{$data->payment_info->payment_amount ?? ''}}
                </td>

                <td style="border: 1px solid">
                    Lease Term: {{$data->payment_info->lease_term ?? ''}}
                </td>

                <td style="border: 1px solid">
                    Lease Type: {{$data->payment_info->lease_type ?? ''}}
                </td>
                <td style="border: 1px solid">
                    Deferred Payment: {{$data->payment_info->deferred_payment ?? ''}}
                </td>
            </tr>
        </tbody>
    </table>
@endif

<table style="width: 100%;  margin-top: 20px; border-collapse: collapse; font-size: 11px">
    <thead>
    <tr>
        <td style="background-color: black; color: white; text-align: center">
            ITEM
        </td>
        <td style="background-color: black; color: white; text-align: center">
            MODEL
        </td>
        <td style="background-color: black; color: white; text-align: center">
            DESCRIPTION
        </td>
        <td style="background-color: black; color: white; text-align: center">
            QTY
        </td>
    </tr>
    </thead>
    <tbody>
        @foreach($data->equipment as $equipment)
            <tr>
                <td style="border: 1px solid; background-color: lightgrey">
                    {{$equipment->item}}
                </td>
                <td style="border: 1px solid; background-color: lightgrey">
                    {{$equipment->make_model}}
                   {{-- <table>
                        @foreach($equipment->accesories as $accesorie)
                            <tr><td>ITEM: {{$accesorie->item}} MODEL: {{$accesorie->model}} Q: {{$accesorie->q}}</td></tr>
                        @endforeach
                    </table>--}}

                </td>
                <td style="border: 1px solid; background-color: lightgrey">
                    {{$equipment->description}}
                </td>
                <td style="border: 1px solid; background-color: lightgrey">
                    {{$equipment->q}}
                </td>
            </tr>
                @foreach($equipment->accesories as $accesorie)
                    @if( !$accesorie )
                        @continue
                    @endif
                    <tr>
                        <td style="border: 1px solid">{{$accesorie->item }}</td>
                        <td style="border: 1px solid">{{$accesorie->model}}</td>
                        <td style="border: 1px solid">{{$accesorie->description}}</td>
                        <td style="border: 1px solid">{{$accesorie->q}}</td>
                    </tr>
                @endforeach
        @endforeach
    </tbody>
</table>
<ol style="font-size: 11px;">
    <li>Docutrend, inc. shall retain title to the Equipment until Customer has paid this sales order in full or has commenced its lease agreement.</li>
    <li>
        This constitutes the complete and exclusive statement of the Agreement between the parties which supersedes all proposals,
        oral or written, and all other communications between the parties relating the this sales order. Customer acknowledges that it has read
        this Agreement, and understand and agrees to all terms and conditions state herein.
    </li>
</ol>
<table style="width: 100%; border-collapse: collapse; font-size: 11px">
    <tr>
        <td  style="background-color: black; color: white; text-align: center">Notes</td>
    </tr>
    <tr>
        <td style="border: 1px solid black">{{$data->sales_agreement->notes ?? ''}}</td>
    </tr>
</table>
<br>
<div style="position: absolute; left: 0; top: 900px;">
    <table style="width: 100%; border-collapse:collapse; font-size: 11px">
        <tr>
            <td style="border: 1px solid black"><span style="color: red">X</span></td>
            <td style="border: 1px solid black"></td>
            <td style="border: 1px solid black"></td>
        </tr>
        <tr>
            <td style="text-align: center">Authorized by (Customer)</td>
            <td style="text-align: center">Title</td>
            <td style="text-align: center">Date</td>
        </tr>
        <tr>
            <td style="border: 1px solid black">&nbsp;</td>
            <td style="border: 1px solid black"></td>
            <td style="border: 1px solid black"></td>
        </tr>
        <tr>
            <td style="text-align: center">Authorized by (Docutrend, Inc)</td>
            <td style="text-align: center">Title</td>
            <td style="text-align: center">Date</td>
        </tr>
    </table>
</div>
<div class="page-break">
    <img src="{{ asset('docutrend/tac_soa.jpg') }}" alt="" width="100%" height="auto">
</div>



</body>
</html>
