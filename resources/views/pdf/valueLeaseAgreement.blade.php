<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Value Leasing Agreement</title>
    <style>
        body {
            font-family: Arial, Helvetica, sans-serif;
        }
        .check-container{
            display: inline;
        }

        .check-container > span {
            display: inline-block;
            margin: 0;
            font-size: 7px;
        }

        .square{
            width: 9px;
            height: 9px;
            border: 1px solid black;
            margin-top: 2px;
            margin-bottom: 2px;
            padding: 1px;
            display: inline-block;
            font-size: 9px;
            text-align: center;
            font-weight: bolder;
        }
        .page-break {
            page-break-after: always;
        }


    </style>
</head>
<body>

    <table style="width: 100%; border-spacing: 15px">
        <tr>
            <td style="width: 25%"><img src="{{asset('images/logo.jpg')}}" alt="" width="200px"></td>
            <td style="text-align: center"><strong style="font-style: italic; font-family: 'Times New Roman'">Value Leasing Agreement</strong></td>
            <td style="width: 15%;text-align: center; font-size: 9px; border: 1px solid">APPLICATION NO.</td>
            <td style="width:15%;text-align: center; font-size: 9px; border: 1px solid">AGREEMENT NO.</td>
        </tr>
    </table>

    <table style="width: 100%">
        <tr>
            <td style="background-color: black; color: white; font-size: 9px; font-style: italic; font-weight: bold" colspan="2">
                575 8th Avenue - New York, NY 10018 - Phone: 212.382.0300 - Fax: 212.695.0038
            </td>
        </tr>
        <tr>
            <td style="font-size: 9px" colspan="2">
                The words <strong>"Lesse"</strong>, <strong>"you"</strong> and <strong>"your"</strong> referer to <strong>Customer.</strong> The words
                <strong>"Lessor"</strong>, <strong>"we"</strong>, <strong>"us"</strong> and <strong>"our"</strong> referer to
                <strong>Docutrend, Inc.</strong>
            </td>
        </tr>
    </table>

    <table style="width: 100%; border-collapse: collapse">
        <tr>
            <td style="background-color: black; color: white; font-size: 9px; font-style: italic; font-weight: bold" colspan="2">
               CUSTOMER INFORMATION
            </td>
        </tr>
        <tr>
            <td style="font-size: 7px; border-bottom: 1px solid black">
              FULL LEGAL NAME <br>{{$data->account->company_legal_name ?? ''}}
            </td>
            <td style="font-size: 7px; border-bottom: 1px solid black">
                STREET ADDRESS <br>{{$data->account->address1 ?? ''}}
            </td>
        </tr>
    </table>
    <table style="width: 100%; border-collapse: collapse">
        <tr>
            <td style="font-size: 7px; border-bottom: 1px solid black">CITY <br>{{$data->account->city ?? ''}}</td>
            <td style="font-size: 7px; border-bottom: 1px solid black">STATE <br>{{$data->account->state ?? ''}}</td>
            <td style="font-size: 7px; border-bottom: 1px solid black">ZIP <br>{{$data->account->zipcode ?? ''}}</td>
            <td style="font-size: 7px; border-bottom: 1px solid black">PHONE <br>{{$data->meter_contact->phone ?? ''}}</td>
            <td style="font-size: 7px; border-bottom: 1px solid black">FAX</td>
        </tr>
    </table>
    <table style="width: 100%; border-collapse: collapse">
        <tr>
            <td style="font-size: 7px; border-bottom: 1px solid black">BILLING NAME(IF DIFFERENT FROM ABOVE) <br>
            @if($data->billing_contact)
                @if( $deal->name != $data->billing_contact->first_name ?? ''.' '.$data->billing_contact->last_name ?? '')
                    {{ $data->billing_contact->first_name.' '.$data->billing_contact->last_name }}
                @endif
            @endif
            <td style="font-size: 7px; border-bottom: 1px solid black">BILLING STREET ADDRESS </td>
        </tr>
    </table>
    <table style="width: 100%; border-collapse: collapse">
        <tr>
            <td style="font-size: 7px; border-bottom: 1px solid black">CITY {{-- <br>{{$data->account->city ?? ''}} --}}</td>
            <td style="font-size: 7px; border-bottom: 1px solid black">STATE {{-- <br>{{$data->account->state ?? ''}} --}}</td>
            <td style="font-size: 7px; border-bottom: 1px solid black">ZIP {{-- <br>{{$data->account->zipcode ?? ''}} --}}</td>
            <td style="font-size: 7px; border-bottom: 1px solid black">E-MAIL{{--  <br>{{$data->billing_contact->email ?? ''}} --}}</td>
        </tr>
    </table>
    <table style="width: 100%; border-collapse: collapse">
        <tr>
            <td style="font-size: 7px; border-bottom: 1px solid black">
                EQUIPMENT LOCATION (IF DIFFERENT FROM ABOVE)
                @if ($data->account->address1 != $data->installation_info->locations[0]->address1)
                    ADDRESS 1: {{ $data->installation_info->locations[0]->address1 }} ADDRESS 2: {{ $data->installation_info->locations[0]->address1 }} CITY:{{ $data->installation_info->locations[0]->city }} STATE: {{ $data->installation_info->locations[0]->state }} ZIP: {{ $data->installation_info->locations[0]->zipcode }}
                @endif
            </td>
        </tr>
    </table>
    <table style="width: 100%; border-collapse: collapse">
        <tr>
            <td style="background-color: black; color: white; font-size: 9px; font-style: italic; font-weight: bold" colspan="6">
                EQUIPMENT DESCRIPTION
            </td>
        </tr>
        <tr>
            <td style="font-size: 7px; text-align:center">

            </td>
            <td style="font-size: 7px; text-align:center">

            </td>
            <td style="font-size: 7px; text-align:center">
                B&W IMAGES
            </td>
            <td style="font-size: 7px; text-align:center">
                COLOR IMAGES
            </td>
            <td style="font-size: 7px; text-align:center">
                MONTHLY B&W
            </td>
            <td style="font-size: 7px; text-align:center">
                MONTHLY COLOR
            </td>
          {{--  <td style="font-size: 7px; border-bottom: 1px solid black" colspan="2">
                SQUARE FOOTAGE
            </td>--}}
            {{--<td style="font-size: 7px; border-bottom: 1px solid black" colspan="2">
                STARTING METER
            </td>--}}
        </tr>
        <tr>
            <td style="font-size: 7px; border-bottom: 1px solid black; text-align:center">
                MAKE/MODEL/ACCESORIES
            </td>
            <td style="font-size: 7px; border-bottom: 1px solid black; text-align:center">
                SERIAL NO.
            </td>
            <td style="font-size: 7px; border-bottom: 1px solid black; text-align:center">
                INCLUDED/MONTH
            </td>
            <td style="font-size: 7px; border-bottom: 1px solid black; text-align:center">
                INCLUDED/MONT
            </td>
            <td style="font-size: 7px; border-bottom: 1px solid black; text-align:center">
                OVERAGE RATE*
            </td>
            <td style="font-size: 7px; border-bottom: 1px solid black; text-align:center">
                OVERAGE RATE*
            </td>
           {{-- <td style="font-size: 7px; border-bottom: 1px solid black" colspan="3">
               VALUE SQ/FT
            </td>--}}
            {{--<td style="font-size: 7px; border-bottom: 1px solid black">
                B&W
            </td>
            <td style="font-size: 7px; border-bottom: 1px solid black">
                COLOR
            </td>--}}
        </tr>
        @foreach($data->equipment as $equipment)
            <tr>
                <td style="font-size: 7px; border-bottom: 1px solid black; text-align: center">{{$equipment->make_model}}</td>
                <td style="font-size: 7px; border-bottom: 1px solid black; text-align: center">{{$equipment->mainframe_matrix->serial ?? ''}}</td>
                <td style="font-size: 7px; border-bottom: 1px solid black; text-align: center">{{ \App\Helpers\Helper::getMeterTypesIncluded('Mono', 'allowance_month', $equipment->mainframe_matrix->options) }}</td>
                <td style="font-size: 7px; border-bottom: 1px solid black; text-align: center">{{ \App\Helpers\Helper::getMeterTypesIncluded('Color', 'allowance_month', $equipment->mainframe_matrix->options) }}</td>
                <td style="font-size: 7px; border-bottom: 1px solid black; text-align: center">{{ \App\Helpers\Helper::getMeterTypesIncluded('Mono', 'overage_rate', $equipment->mainframe_matrix->options) }}</td>
                <td style="font-size: 7px; border-bottom: 1px solid black; text-align: center">{{ \App\Helpers\Helper::getMeterTypesIncluded('Color', 'overage_rate', $equipment->mainframe_matrix->options) }}</td>
            </tr>
        @endforeach
        @php
            $cantidadEquipment = count($data->equipment) ?? 0;
            if($cantidadEquipment <= 1){
                $tope = 20;
            }elseif($cantidadEquipment == 2){
                $tope = 19;
            }elseif($cantidadEquipment == 3){
                $tope = 18;
            }elseif($cantidadEquipment == 4){
                $tope = 17;
            }elseif($cantidadEquipment == 5){
                $tope = 16;
            }elseif($cantidadEquipment == 6){
                $tope = 15;
            }elseif($cantidadEquipment == 7){
                $tope = 14;
            }elseif($cantidadEquipment == 8){
                $tope = 13;
            }elseif($cantidadEquipment == 9){
                $tope = 12;
            }elseif($cantidadEquipment == 10){
                $tope = 11;
            }elseif($cantidadEquipment == 11){
                $tope = 10;
            }elseif($cantidadEquipment == 12){
                $tope = 9;
            }elseif($cantidadEquipment == 13){
                $tope = 8;
            }elseif($cantidadEquipment == 14){
                $tope = 7;
            }elseif($cantidadEquipment == 15){
                $tope = 6;
            }elseif($cantidadEquipment == 16){
                $tope = 5;
            }elseif($cantidadEquipment == 17){
                $tope = 4;
            }elseif($cantidadEquipment == 18){
                $tope = 3;
            }elseif($cantidadEquipment == 19){
                $tope = 2;
            }elseif($cantidadEquipment == 20){
                $tope = 1;
            }else{
                $tope = 0;
            }
        @endphp
        @for($celda = 0; $celda <= $tope; $celda++)
            <tr>
                <td style="font-size: 7px; border-bottom: 1px solid black; text-align: center">&nbsp;</td>
                <td style="font-size: 7px; border-bottom: 1px solid black; text-align: center">&nbsp;</td>
                <td style="font-size: 7px; border-bottom: 1px solid black; text-align: center">&nbsp;</td>
                <td style="font-size: 7px; border-bottom: 1px solid black; text-align: center">&nbsp;</td>
                <td style="font-size: 7px; border-bottom: 1px solid black; text-align: center">&nbsp;</td>
                <td style="font-size: 7px; border-bottom: 1px solid black; text-align: center">&nbsp;</td>
            </tr>
        @endfor
        <tr>
            <td style="text-align: center; border-bottom: 1px solid black; margin-left: 10%" colspan="8">
                <div class="check-container">
                    <div class="square">&nbsp;</div>
                    <span>See attached Schedule A</span>
                </div>
                <div class="check-container">
                    <div class="square">&nbsp;</div>
                    <span>See attached Billing Schedule</span>
                </div>
            </td>
        </tr>
    </table>
    <table style="width: 100%; border-collapse: collapse">
        <tr>
            <td style="background-color: black; color: white; font-size: 10px; font-style: italic; font-weight: bold" colspan="3">
                TERM AND PAYMENT INFORMATION
            </td>
        </tr>
        <tr>
            <td style="font-size: 12px;" colspan="3">
                &nbsp;
            </td>
        </tr>
        <tr>
            <td style="font-size: 9px;">
                <u>{{$data->payment_info->lease_term ?? ''}}</u> Payments* of <u>{{$data->payment_info->payment_amount}}</u>
            </td>
            <td style="font-size: 9px;">
                If you are exempt from sales tax, attach your certificate
            </td>
            <td style="font-size: 9px;">
                *plus applicable taxes
            </td>
        </tr>
        <tr>
            <td style="font-size: 9px;" colspan="3">
                The payment ("Payment") period is monthly unless otherwise indicated
        </tr>
    </table>
    <table style="width: 100%; border-collapse: collapse">
        <tr>
            <td style="background-color: black; color: white; font-size: 9px; font-style: italic; font-weight: bold" colspan="3">
               END OF TERM OPTIONS
            </td>
        </tr>
        <tr>
            <td style="font-size: 7px; text-align: justify-all" colspan="3">
                You may choose one of the following options, which you may exercise at the end of the term, provided that no event of default under this Agreement has occurred and is continuing. If no box is
                checked and initialed, Fair Market Value will be your end of term option. Fair Market Value means the value of the Equipment in continued use.
            </td>
        </tr>
        <tr>
            <td style="font-size: 7px; width: 75%">
                <div class="check-container">
                    @if($data->payment_info->lease_type == 'FMV')
                        <div class="square">X</div>
                    @else
                        <div class="square">&nbsp;</div>
                    @endif
                    <span>Purchase all of the Equipment for its Fair Market Value, renew this Agreement, or return the Equipment.</span>
                </div>
            </td>
            <td style="font-size: 7px; width:25%; text-align: right" colspan="2">______Customer’s Initials</td>
        </tr>
        <tr>
            <td style="font-size: 7px; width: 75%">
                <div class="check-container">
                    <div class="square">&nbsp;</div>
                    <span>Purchase all of the Equipment for $1.00. At the end of the term, title to the Equipment will automatically transfer to you, AS IS, WHERE IS, with no warranties of any kind.</span>
                </div>
            </td>
            <td style="font-size: 7px; width:25%; text-align: right" colspan="2">______Customer’s Initials</td>
        </tr>
    </table>
    <table style="width: 100%; border-collapse: collapse">
        <tr>
            <td style="background-color: black; color: white; font-size: 7px; font-style: italic; font-weight: bold" colspan="5">
                LESSOR ACCEPTANCE
            </td>
        </tr>
        <tr>
            <td></td>
        </tr>
        <tr>
            <td style="font-size: 7px; width: 30%; border-bottom: 1px solid black">
                <strong>Docutrend, Inc.</strong>
            </td>
            <td style="border: 1px solid; width: 40%; border-bottom: 2px solid black">
                &nbsp;
            </td>
            <td style="width: 15%; border-bottom: 1px solid black">

            </td>
            <td style="width: 15%; border-bottom: 1px solid black" colspan="2">

            </td>
        </tr>
        <tr>
            <td style="font-size: 7px; width: 30%; ">
                LESSOR
            </td>
            <td style="font-size: 7px; width: 40%">
                SIGNATURE
            </td>
            <td style="font-size: 7px; width: 15%">
                TITLE
            </td>
            <td style="font-size: 7px; width: 15%" colspan="2">
                DATED
            </td>
        </tr>
    </table>

    <table style="width: 100%; border-collapse: collapse">
        <tr>
            <td style="background-color: black; color: white; font-size: 7px; font-style: italic; font-weight: bold" colspan="5">
                CUSTOMER ACCEPTANCE
            </td>
        </tr>
        <tr>
            <td colspan="5" style="font-size: 7px">
                <strong>BY SIGNING BELOW OR AUTHENTICATING AN ELECTRONIC RECORD HEREOF, YOU CERTIFY THAT YOU HAVE REVIEWED AND DO AGREE TO ALL TERMS AND CONDITIONS OF
                    THIS AGREEMENT ON THIS PAGE AND ON PAGE 2 ATTACHED HERETO.</strong>
            </td>
        </tr>
        <tr>
            <td style="font-size: 7px; width: 30%; border-bottom: 1px solid black">
                {{$data->account->company_legal_name}}
            </td>
            <td style="border: 1px solid; width: 40%; border-bottom: 2px solid black">
                <h3 style="padding: 0; margin: 0px; color: red;">X</h3>
            </td>
            <td style="width: 15%; border-bottom: 1px solid black" colspan="3">

            </td>
        </tr>
        <tr>
            <td style="font-size: 7px; width: 30%; border-bottom: 1px solid black">
                CUSTOMER (as referenced above)
                <br>{{$data->tax_id->number}}
            </td>
            <td style="font-size:7px; border-bottom: 1px solid black">
               SIGNATURE
            </td>
            <td style="font-size: 7px; width: 15%; border-bottom: 1px solid black">
                TITLE
            </td>
            <td style="font-size:7px;width: 15%; border-bottom: 1px solid black" colspan="2">
                DATED
            </td>
        </tr>
        <tr>
            <td style="font-size: 7px;" colspan="">
                FEDERAL TAX ID #
            </td>
            <td style="font-size:7px;" colspan="4">
                PRINT NAME
            </td>
        </tr>
    </table>

    <table style="width: 100%; border-collapse: collapse">
        <tr>
            <td style="background-color: black; color: white; font-size: 7px; font-style: italic; font-weight: bold" colspan="5">
                DELIVERY & ACCPETANCE CERTIFICATE
            </td>
        </tr>
        <tr>
            <td colspan="5" style="font-size: 7px">
                <strong>You certify and acknowledge that all of the Equipment listed above: 1) has been received, installed and inspected; and 2) is fully operational and unconditionally accepted. Upon you signing below,
                    your promises in this Agreement will be irrevocable and unconditional in all respects.</strong>
            </td>
        </tr>
        <tr>
            <td style="font-size: 7px; width: 30%; border-bottom: 1px solid black">

            </td>
            <td style="border: 1px solid; width: 40%; border-bottom: 2px solid black">
                <h3 style="padding: 0; margin: 0px; color: red;">X</h3>
            </td>
            <td style="width: 15%; border-bottom: 1px solid black">

            </td>
            <td style="width: 15%; border-bottom: 1px solid black" colspan="2">

            </td>
        </tr>
        <tr>
            <td style="font-size: 7px; width: 30%; border-bottom: 1px solid black">
                CUSTOMER (as referenced above)
            </td>
            <td style="font-size:7px; border-bottom: 1px solid black">
                SIGNATURE
            </td>
            <td style="font-size: 7px; width: 15%; border-bottom: 1px solid black">
                TITLE
            </td>
            <td style="font-size:7px;width: 15%; border-bottom: 1px solid black" colspan="2">
                ACCEPTANCE DATED
            </td>
        </tr>
    </table>

    <table style="width: 100%; border-collapse: collapse">
        <tr>
            <td style="background-color: black; color: white; font-size: 7px; font-style: italic; font-weight: bold" colspan="7">
                TERMS AND CONDITIONS
            </td>
        </tr>
        <tr>
            <td colspan="7" style="font-size: 7px;">
                <p style="text-align: justify; margin-top: 1px"><strong>1. AGREEMENT:</strong> You agree to lease from us the goods, together with all replacements,
                    parts, repairs, additions, and accessions incorporated therein or attached thereto and any and all
                    proceeds of the foregoing, including, without limitation,
                    insurance recoveries ("Equipment") and, if applicable, finance certain software, software
                    license(s), software components and/or professional services in connection with software
                    (collectively, the “Financed Items,” which are included in the
                    word “Equipment” unless separately stated) from software licensor(s) and/or supplier(s)
                    (collectively, the “Supplier”), all as described in this Agreement and in any attached schedule,
                    addendum or amendment hereto (“Agreement”). You
                    represent and warrant that you will use the Equipment for business purposes only. You agree to all
                    of the terms and conditions contained in this Agreement, which, with the acceptance certification,
                    is the entire agreement between you and us
                    regarding the Equipment and which supersedes all prior agreements, including any purchase order,
                    invoice, request for proposal, response or other related document. This Agreement becomes valid upon
                    execution by us. If any provision of
                    this Agreement is declared unenforceable, the other provisions herein shall remain in full force and
                    effect to the fullest extent permitted by law. (Continued on Page 2)</p>
            </td>
        </tr>
        <tr>
            <td colspan="6">
                <p style="text-align: center; font-size: 11px; margin-top: 0px">
                    <strong style="margin-top: 0px">
                        Upon acceptance of the Equipment, THIS AGREEMENT IS NONCANCELABLE, IRREVOCABLE AND CANNOT BE
                        TERMINATED.
                    </strong>
                </p>
            </td>
        </tr>
        <tr>
            <td style="font-size: 7px; text-align: center;" colspan="2">29366 (2017)</td>
            <td style="font-size: 7px; text-align: center;" colspan="2">Page 1 of 2</td>
            <td style="font-size: 7px; text-align: center;" colspan="2">Rev. 04/16/2019</td>
        </tr>
    </table>
    <div class="page-break"></div>
    <table style="width: 100%">
        <tr>
            <td>
                <p style="font-size: 7px; text-align: justify; margin-top: 3px; margin-bottom: 3px">
                    <strong>2. OWNERSHIP; PAYMENTS; TAXES AND FEES:</strong> We own the Equipment, excluding any Financed Items. Ownership of any Financed Items shall remain with Supplier thereof. You will pay all Payments, as adjusted, when due, without
                    notice or demand and without abatement, set-off, counterclaim or deduction of any amount whatsoever. If any part of a Payment is more than 5 days late, you agree to pay a late charge equal to: a) the higher of 10% of the Payment which is
                    late or $26.00, or b) if less, the maximum charge allowed by law. The Payment may be adjusted proportionately upward or downward: (i) if the shipping charges or taxes differ from the estimate given to you; and/or (ii) to comply with the tax
                    laws of the state in which the Equipment is located. You shall pay all applicable taxes, assessments and penalties related to this Agreement, whether levied or assessed on this Agreement, on us (except on our income) or you, or on the
                    Equipment, its lease, sale, ownership, possession, use or operation. If we pay any taxes or other expenses that are owed hereunder, you agree to reimburse us when we request. We may charge you a processing fee for administering property
                    tax filings. You agree to pay us a fee of up to $50 for filing and/or searching costs required under the Uniform Commercial Code (“UCC”) or other laws. You agree to pay us an origination fee of up to $125 for all closing costs. We may apply
                    all sums received from you to any amounts due and owed to us under the terms of this Agreement. If for any reason your check is returned for insufficient funds, you will pay us a service charge of $30 or, if less, the maximum charge allowed
                    by law. We may make a profit on any fees, estimated tax payments and other charges paid under this Agreement.
                </p>
            </td>
        </tr>
        <tr>
            <td>
               <p style="font-size: 7px;text-align: justify; margin-top: 3px; margin-bottom: 3px"><strong>3. EQUIPMENT; SECURITY INTEREST:</strong> At your expense, you shall keep the Equipment: (i)
                    in good repair, condition and working order, in compliance with applicable laws, ordinances and
                    manufacturers’ and regulatory standards; (ii) free
                    and clear of all liens and claims; and (iii) at your address shown on page 1, and you agree not to
                    move it unless we agree in writing. You grant us a security interest in the Equipment to secure all
                    amounts you owe us under this Agreement or
                    any other agreement with us (“Other Agreements”), except amounts under Other Agreements which are
                    secured by land and/or buildings. You authorize and ratify our filing of any financing statement(s)
                    to show our interest. You will not
                    change your name, state of organization, headquarters or residence without providing prior written
                    notice to us. You will notify us within 30 days if your state of organization revokes or terminates
                    your existence.</p>
            </td>
        </tr>
        <tr>
            <td>
                <p style="font-size: 7px;text-align: justify; margin-top: 3px; margin-bottom: 3px"><strong>4. INSURANCE; COLLATERAL PROTECTION; INDEMNITY; LOSS OR DAMAGE:</strong> You agree to keep
                    the Equipment fully insured against all risk, with us named as lender’s loss payee, in an amount not
                    less than the full replacement value
                    of the Equipment until this Agreement is terminated. You also agree to maintain commercial general
                    liability insurance with such coverage and from such insurance carrier as shall be satisfactory to
                    us and to include us as an additional insured
                    on the policy. You will provide written notice to us within 10 days of any modification or
                    cancellation of your insurance policy(s). You agree to provide us certificates or other evidence of
                    insurance acceptable to us. If you do not provide us
                    with acceptable evidence of property insurance within 30 days after the start of this Agreement, we
                    may, at our sole discretion, to do so as provided in either (A) or (B) below, as determined in our
                    discretion: (A) We may secure property loss
                    insurance on the Equipment from a carrier of our choosing in such forms and amounts as we deem
                    reasonable to protect our interests. If we place insurance on the Equipment, we will not name you as
                    an insured and your interests may not
                    be fully protected. If we secure insurance on the Equipment, you will pay us an amount for the
                    premium which may be higher than the premium that you would pay if you placed the insurance
                    independently and an insurance fee which may
                    result in a profit to us through an investment in reinsurance; or (B) We charge you a monthly
                    property damage surcharge of up to .0035 of the Equipment cost as a result of our credit risk and
                    administrative and other costs, as would be further
                    described on a letter from us to you. We may make a profit on this program. NOTHING IN THIS
                    PARAGRAPH WILL RELIEVE YOU OF RESPONSIBILITY FOR LIABILITY INSURANCE ON THE EQUIPMENT. We are not
                    responsible for,
                    and you agree to hold us harmless and reimburse us for and to defend on our behalf against, any
                    claim for any loss, expense, liability or injury caused by or in any way related to delivery,
                    installation, possession, ownership, leasing, manufacture,
                    use, condition, inspection, removal, return or storage of the Equipment. All indemnities will
                    survive the expiration or termination of this Agreement. You are responsible for any loss, theft,
                    destruction or damage to the Equipment (“Loss”),
                    regardless of cause, whether or not insured. You agree to promptly notify us in writing of any Loss.
                    If a Loss occurs and we have not otherwise agreed in writing, you will promptly pay to us the unpaid
                    balance of this Agreement, including any
                    future Payments to the end of the term plus the anticipated residual value of the Equipment, both
                    discounted to present value at 2%. Any proceeds of insurance will be paid to us and credited against
                    the Loss. You authorize us to sign on your
                    behalf and appoint us as your attorney-in-fact to endorse in your name any insurance drafts or
                    checks issued due to a Loss.</p>
            </td>
        </tr>
        <tr>
            <td>
                <p style="font-size: 7px;text-align: justify; margin-top: 3px; margin-bottom: 3px"><strong>5. ASSIGNMENT: YOU SHALL NOT SELL, TRANSFER, ASSIGN, ENCUMBER, PLEDGE OR SUBLEASE THE EQUIPMENT OR
                        THIS AGREEMENT, without our prior written consent.</strong> You shall not consolidate or merge with or into
                    any other entity, distribute, sell or dispose of all or any substantial portion of your assets other
                    than in the ordinary course of business, without our prior written consent, and the surviving, or
                    successor entity or the transferee of such assets, as
                    the case may be, shall assume all of your obligations under this Agreement by a written instrument
                    acceptable to us. No event shall occur which causes or results in a transfer of majority ownership
                    of you while any obligations are outstanding
                    hereunder. We may sell, assign, or transfer this Agreement without notice to or consent from you.
                    You agree that if we sell, assign or transfer this Agreement, our assignee will have the same rights
                    and benefits that we have now and will not
                    have to perform any of our obligations. You agree that our assignee will not be subject to any
                    claims, defenses, or offsets that you may have against us. This Agreement shall be binding on and
                    inure to the benefit of the parties hereto
                    and their respective successors and assigns.</p>
            </td>
        </tr>
        <tr>
            <td>
                <p style="font-size: 7px;text-align: justify; margin-top: 3px; margin-bottom: 3px"><strong>6. DEFAULT AND REMEDIES:</strong> You will be in default if: (i) you do not pay any Payment or other sum due
                    to us or you fail to perform in accordance with the covenants, terms and conditions of this
                    Agreement or any other agreement with us
                    or any of our affiliates or fail to perform or pay under any material agreement with any other
                    entity; (ii) you make or have made any false statement or misrepresentation to us; (iii) you or any
                    guarantor dies, dissolves, liquidates, terminates
                    existence or is in bankruptcy; (iv) you or any guarantor suffers a material adverse change in its
                    financial, business or operating condition; or (v) any guarantor defaults under any guaranty for
                    this Agreement. If you are ever in default, at our
                    option, we can cancel this Agreement and require that you pay the unpaid balance of this Agreement,
                    including any future Payments to the end of term plus the anticipated residual value of the
                    Equipment, both discounted to present value at
                    2%. We may recover default interest on any unpaid amount at the rate of 12% per year. Concurrently
                    and cumulatively, we may also use any remedies available to us under the UCC and any other law and
                    we may require that you immediately
                    stop using any Financed Items. If we take possession of the Equipment, you agree to pay the costs of
                    repossession, moving, storage, repair and sale. The net proceeds of the sale of any Equipment will
                    be credited against what you owe us
                    under this Agreement and you will be responsible for any deficiency. In the event of any dispute or
                    enforcement of our rights under this Agreement or any related agreement, you agree to pay our
                    reasonable attorneys’ fees (including any
                    incurred before or at trial, on appeal or in any other proceeding), actual court costs and any other
                    collection costs, including any collection agency fee. WE SHALL NOT BE RESPONSIBLE TO PAY YOU ANY
                    CONSEQUENTIAL, INDIRECT
                    OR INCIDENTAL DAMAGES FOR ANY DEFAULT, ACT OR OMISSION BY ANYONE. Any delay or failure to enforce
                    our rights under this Agreement will not prevent us from enforcing any rights at a later time. You
                    agree that this Agreement
                    is a "Finance Lease" as defined by Article 2A of the UCC and your rights and remedies are governed
                    exclusively by this Agreement. You waive all rights under sections 2A-508 through 522 of the UCC. If
                    interest is charged or
                    collected in excess of the maximum lawful rate, we will refund such excess to you, which will be
                    your sole remedy.</p>
            </td>
        </tr>
        <tr>
            <td>
                <p style="font-size: 7px;text-align: justify; margin-top: 3px; margin-bottom: 3px"><strong>7. INSPECTIONS AND REPORTS:</strong> We have the right, at any reasonable time, to inspect the Equipment and
                    any documents relating to its installation, use, maintenance and repair. Within 30 days after our
                    request (or such longer period as
                    provided herein), you will deliver all requested information (including tax returns) which we deem
                    reasonably necessary to determine your current financial condition and faithful performance of the
                    terms hereof. This may include: (i) compiled,
                    reviewed or audited annual financial statements (including, without limitation, a balance sheet, a
                    statement of income, a statement of cash flow, a statement of changes in equity and notes to
                    financial statements) within 120 days after your
                    fiscal year end, and (ii) management-prepared interim financial statements within 45 days after the
                    requested reporting period(s). Annual statements shall set forth the corresponding figures for the
                    prior fiscal year in comparative form, all in
                    reasonable detail without any qualification or exception deemed material by us. Unless otherwise
                    accepted by us, each financial statement shall be prepared in accordance with generally accepted
                    accounting principles consistently applied
                    and shall fairly and accurately present your financial condition and results of operations for the
                    period to which it pertains. You authorize us to obtain credit bureau reports for credit and
                    collection purposes and to share them with our affiliates
                    and agents.</p>
            </td>
        </tr>
        <tr>
            <td>
                <p style="font-size: 7px;text-align: justify; margin-top: 3px; margin-bottom: 3px"><strong>8. END OF TERM:</strong> Unless the purchase option is $1.00, at the end of the initial term, this Agreement
                    shall renew for successive 12-month renewal term(s) under the same terms hereof unless you send us
                    written notice between 90 and 150
                    days before the end of the initial term or at least 30 days before the end of any renewal term that
                    you want to purchase or return the Equipment, and you timely purchase or return the Equipment. You
                    shall continue making Payments and
                    paying all other amounts due until the Equipment is purchased or returned. As long as you have given
                    us the required written notice, if you do not purchase the Equipment, you will return all of the
                    Equipment to a location we specify, at your
                    expense, in retail re-saleable condition, full working order and complete repair. YOU ARE SOLELY
                    RESPONSIBLE FOR REMOVING ANY DATA THAT MAY RESIDE IN THE EQUIPMENT, INCLUDING BUT NOT LIMITED TO
                    HARD DRIVES,
                    DISK DRIVES OR ANY OTHER FORM OF MEMORY. Unless the purchase option is $1.00, you cannot pay off
                    this Agreement or return the Equipment prior to the end of the initial term without our consent. If
                    we consent, we may charge you,
                    in addition to other amounts owed, an early termination fee equal to 10% of the price of the
                    Equipment.</p>
            </td>
        </tr>
        <tr>
            <td>
                <p style="font-size: 7px;text-align: justify; margin-top: 3px; margin-bottom: 3px"><strong>9. USA PATRIOT ACT NOTICE; ANTI-TERRORISM AND ANTI-CORRUPTION COMPLIANCE:</strong> To help the government
                    fight the funding of terrorism and money laundering activities, federal law requires all financial
                    institutions to obtain,
                    verify, and record information that identifies each customer who opens an account. When you enter
                    into a transaction with us, we ask for your business name, address and other information that will
                    allow us to identify you. We may also ask
                    to see other documents that substantiate your business identity. You and any other person who you
                    control, own a controlling interest in, or who owns a controlling interest in or otherwise controls
                    you in any manner (“Representatives”) are
                    and will remain in full compliance with all laws, regulations and government guidance concerning
                    foreign asset control, trade sanctions, embargoes, and the prevention and detection of money
                    laundering, bribery, corruption, and terrorism, and
                    neither you nor any of your Representatives is or will be listed in any Sanctions-related list of
                    designated persons maintained by the U.S. Department of Treasury’s Office of Foreign Assets Control
                    or successor or the U.S. Department of State.
                    You shall, and shall cause any Representative to, provide such information and take such actions as
                    are reasonably requested by us in order to assist us in maintaining compliance with anti-money
                    laundering laws and regulations.
                </p>
            </td>
        </tr>
        <tr>
            <td>
                <p style="font-size: 7px;text-align: justify; margin-top: 3px; margin-bottom: 3px"><strong>10. MISCELLANEOUS:</strong> Unless otherwise stated in an addendum hereto, the parties agree that: (i)
                    this Agreement and any related documents hereto may be authenticated by electronic means; (ii) the
                    “original” of this Agreement shall be the
                    copy that bears your manual, facsimile, scanned or electronic signature and that also bears our
                    manually or electronically signed signature and is held or controlled by us; and (iii) to the extent
                    this Agreement constitutes chattel paper (as
                    defined by the UCC), a security interest may only be created in the original. You agree not to raise
                    as a defense to the enforcement of this Agreement or any related documents that you or we executed
                    or authenticated such documents by
                    electronic or digital means or that you used facsimile or other electronic means to transmit your
                    signature on such documents. Notwithstanding anything to the contrary herein, we reserve the right
                    to require you to sign this Agreement or any
                    related documents hereto manually and to send to us the manually signed, duly executed documents via
                    overnight courier on the same day that you send us the facsimile, scanned or electronic transmission
                    of the documents. You agree to
                    execute any further documents that we may request to carry out the intents and purposes of this
                    Agreement. Whenever our consent is required, we may withhold or condition such consent in our sole
                    discretion, except as otherwise expressly
                    stated herein. From time to time, Supplier may extend to us payment terms for Equipment financed
                    under this Agreement that are more favorable than what has been quoted to you or the general public,
                    and we may provide Supplier information
                    regarding this Agreement if Supplier has assigned or referred it to us. All notices shall be mailed
                    or delivered by facsimile transmission or overnight courier to the respective parties at the
                    addresses shown on this Agreement or such other
                    address as a party may provide in writing from time to time. By providing us with a telephone number
                    for a cellular phone or other wireless device, including a number that you later convert to a
                    cellular number, you are expressly consenting to
                    receiving communications, including but not limited to prerecorded or artificial voice message
                    calls, text messages, and calls made by an automatic telephone dialing system, from us and our
                    affiliates and agents at that number. This express
                    consent applies to each such telephone number that you provide to us now or in the future and
                    permits such calls for non-marketing purposes. Calls and messages may incur access fees from your
                    cellular provider. You authorize us to make
                    non-material amendments (including completing and conforming the description of the Equipment) on
                    any document in connection with this Agreement. Unless stated otherwise herein, all other
                    modifications to this Agreement must be in
                    writing and signed by each party or in a duly authenticated electronic record. This Agreement may
                    not be modified by course of performance.</p>
            </td>
        </tr>
        <tr>
            <td>
                <p style="font-size: 7px;text-align: justify; margin-top: 3px; margin-bottom: 3px"><strong>11. WARRANTY DISCLAIMERS:</strong> WE ARE LEASING THE EQUIPMENT TO YOU “AS-IS.” YOU HAVE SELECTED SUPPLIER AND
                    THE EQUIPMENT BASED UPON YOUR OWN JUDGMENT. IN THE EVENT WE ASSIGN THIS
                    AGREEMENT, OUR ASSIGNEE DOES NOT TAKE RESPONSIBILITIES FOR THE INSTALLATION OR PERFORMANCE OF THE
                    EQUIPMENT. SUPPLIER IS NOT AN AGENT OF OURS AND WE ARE NOT AN AGENT OF SUPPLIER,
                    AND NOTHING SUPPLIER STATES OR DOES CAN AFFECT YOUR OBLIGATIONS HEREUNDER. <strong>YOU WILL MAKE ALL
                        PAYMENTS UNDER THIS AGREEMENT REGARDLESS OF ANY CLAIM OR COMPLAINT AGAINST ANY
                        SUPPLIER, LICENSOR OR MANUFACTURER, AND ANY FAILURE OF A SERVICE PROVIDER TO PROVIDE SERVICES
                        WILL
                        NOT EXCUSE YOUR OBLIGATIONS TO US UNDER THIS AGREEMENT. WE MAKE NO WARRANTIES,
                        EXPRESS OR IMPLIED, OF, AND TAKE ABSOLUTELY NO RESPONSIBILITY FOR, MERCHANTABILITY, FITNESS FOR
                        ANY
                        PARTICULAR PURPOSE, CONDITION, QUALITY, ADEQUACY, TITLE, DATA ACCURACY, SYSTEM
                        INTEGRATION, FUNCTION, DEFECTS, INFRINGEMENT OR ANY OTHER ISSUE IN REGARD TO THE EQUIPMENT, ANY
                        ASSOCIATED SOFTWARE AND ANY FINANCED ITEMS.</strong> SO LONG AS YOU ARE NOT IN DEFAULT UNDER
                    THIS AGREEMENT, WE ASSIGN TO YOU ANY WARRANTIES IN THE EQUIPMENT GIVEN TO US.</p>
            </td>
        </tr>
        <tr>
            <td>
                <p style="font-size: 7px;text-align: justify; margin-top: 3px; margin-bottom: 3px">12. <strong>LAW; JURY WAIVER:</strong> This Agreement will be governed by and construed in accordance with the law of
                    the principal place of business of Lessor or, if assigned, its assignee. You consent to jurisdiction
                    and venue of any state or federal
                    court in the state of the Lessor or, if assigned, its assignee has its principal place of business
                    and waive the defense of inconvenient forum. For any action arising out of or relating to this
                    Agreement or the Equipment, BOTH PARTIES WAIVE
                    ALL RIGHTS TO A TRIAL BY JURY.</p>
            </td>
        </tr>
        <tr>
            <td>
                <p style="font-size: 7px;text-align: justify; margin-top: 3px; margin-bottom: 3px"><strong>13. MAINTENANCE AND SUPPLIES:</strong> You have elected to enter into a separate arrangement
                    with Supplier for maintenance, inspection, adjustment, parts replacement, drums, cleaning material
                    required for proper operation and toner and
                    developer (“Arrangement”). You agree to pay all amounts owing under this Agreement regardless of any
                    claim you have against Supplier relating to the Arrangement. Supplier will be solely responsible for
                    performing all services and providing
                    all supplies under the Arrangement. You agree not to hold Lessor (if different from Supplier) or any
                    assignee of this Agreement responsible for Supplier’s obligations under the Arrangement. As a
                    convenience to you, we will provide you with
                    one invoice covering amounts owing under this Agreement and the Arrangement. If necessary,
                    Supplier’s obligations to you under the Arrangement may be assigned by us. You agree to pay a
                    monthly supply freight fee up to $7.00 per
                    machine to cover the costs of shipping supplies to you. Each month, you are entitled to produce the
                    minimum number of images shown on page 1 for each applicable image type. Regardless of the number of
                    images made, you will never pay
                    less than the minimum Payment. You agree to provide periodic meter readings on the Equipment. You
                    agree to pay the applicable overage charge for each metered image that exceeds the applicable
                    minimum number of images. Images
                    made on equipment marked as not financed under this Agreement will be included in determining your
                    image and overage charges. At the end of the first year of this Agreement, and once each successive
                    12-month period thereafter, the
                    maintenance and supplies portion of the Payment and the overage charges may be increased by a
                    maximum of 15% of the existing payment or charge. In order to facilitate an orderly transition, the
                    start date of this Agreement will be the date
                    the Equipment is delivered to you or a date designated by us, as shown on the first invoice. If a
                    later start date is designated, in addition to all Payments and other amounts due hereunder, you
                    agree to pay us a transitional payment equal to
                    1/30th of the Payment, multiplied by the number of days between the date the Equipment is delivered
                    to you and the designated start date. The first Payment is due 30 days after the start of this
                    Agreement and each Payment thereafter shall
                    be due on the same day of each month.</p>
            </td>
        </tr>
    </table>
<table width="100%">
    <tr>
        <td style="font-size: 7px; text-align: center;" colspan="2">29366 (2017)</td>
        <td style="font-size: 7px; text-align: center;" colspan="2">Page 2 of 2</td>
        <td style="font-size: 7px; text-align: center;" colspan="2">Rev. 04/16/2019</td>
    </tr>
</table>

</body>
</html>
