<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <style>
        body {
            font-family: Arial, Helvetica, sans-serif;
        }
    </style>
</head>
<body>
<table style="width: 100%">
    <tr>
        <td><img src="images/logo.jpg" alt="" width="250px"></td>
        <td style="text-align: center"><h3>General Payoff Agreement</h3></td>
    </tr>
</table>
<table style="width: 50%">
    <tr>
        <td style="width: 10%; font-size: 11px"><strong>Date:</strong></td>
        <td style=" font-size: 11px">{{$date}}</td>
    </tr>
</table>
<table style="width: 50%">
    <tr>
        <td><strong>Client Information:</strong></td>
    </tr>
    <tr>
        <td style=" font-size: 11px">{{$data->account->company_legal_name ?? ''}}</td>
    </tr>
    <tr>
        <td style="  font-size: 11px">{{$data->account->address1 ?? ''}} {{$data->account->address2 ?? ''}} ZIP: {{$data->account->zipcode ?? ''}}</td>
    </tr>
    <tr>
        <td style="  font-size: 11px">{{$data->account->city ?? ''}} {{$data->account->state ?? ''}}</td>
    </tr>
    <tr>
        <td style="  font-size: 11px">{{$data->account->company_url ?? ''}}</td>
    </tr>
</table>

<table style="width: 100%">
    <tr>
        <td style="width: 10%"><strong>Dear:</strong></td>
        <td style=" width: 90%; font-size: 11px">{{$data->billing_contact->first_name ?? ''}}</td>
    </tr>
</table>
<p style="text-align: justify; font-size: 11px">
    This letter is to confirm Docutrend’s commitment to advance your organization the remaining payments on the below referenced lease
    agreement(s).
</p>
<p style="text-align: justify; font-size: 11px">
    You must send in a letter to your current leasing company notifying them of your intention not to renew your agreement with them. This
    letter must be sent, on your letterhead, within the notification time frame, as per the terms and conditions of your current lease
    agreement(s.) <strong><u>It is your responsibility to deliver the notice of non-renewal to your current leasing
            company
            within the time period allowed
            under your lease. Docutrend’s obligation is limited to advancing you the payoff amount quoted below, and it
            will
            not be held liable for any
            payments due in connection with any lease renewal terms or any other charges associated with untimely
            notification to your current
            leasing company.</u></strong>
</p>
<p style="text-align: justify; font-size: 11px">
    If you ask us to store your previously leased equipment, we will warehouse the machine, at our expense, <u>until we
        receive return shipping
        instructions from you, provided by the leasing company.</u> This will be provided directly to you by the leasing company and should be
    forwarded to our attention immediately upon your receipt via fax at: <strong>(212) 695-0038 Attn: Order Processing.</strong> Failure to send us return
    shipping instructions in a timely manner could result in equipment storage charges being added to your new lease payments.
</p>

<p>
    The leases are as follows:
</p>
<table style="border: 1px solid black; font-size: 11px; width: 100%; border-collapse: collapse">
    <thead>
    <tr>
        <td style="border: 1px solid;"><strong>Leasing Company</strong></td>
        <td style="border: 1px solid;"><strong>Lease #</strong></td>
        <td style="border: 1px solid;"><strong>Lease End Date</strong></td>
        <td style="border: 1px solid;"><strong>Model #</strong></td>
        <td style="border: 1px solid;"><strong>Serial #</strong></td>
        <td style="border: 1px solid;"><strong>Payment Amount (Including Tax)</strong></td>
        <td style="border: 1px solid;"><strong>Total Due</strong></td>
    </tr>
    </thead>
    <tbody>
        @foreach($data->payoff_agreement->body as $payoff)
            <tr>
                <td style=" border: 1px solid; font-size: 11px; text-align:center;">{{$payoff->leasingCompany}}</td>
                <td style=" border: 1px solid; font-size: 11px; text-align:center;">{{$payoff->leaseNumber}}</td>
                <td style=" border: 1px solid; font-size: 11px; text-align:center;">{{\Carbon\Carbon::parse($payoff->leaseEndDate)->format('m/d/Y')}}</td>
                <td style=" border: 1px solid; font-size: 11px; text-align:center;">{{$payoff->modelNumber}}</td>
                <td style=" border: 1px solid; font-size: 11px; text-align:center;">{{$payoff->serialNumber}}</td>
                <td style=" border: 1px solid; font-size: 11px; text-align:center;">${{number_format($payoff->paymentAmount, 2)}}</td>
                <td style=" border: 1px solid; font-size: 11px; text-align:center;">${{number_format($payoff->totalDue, 2)}}</td>
            </tr>
        @endforeach
    </tbody>
</table>
<p style="text-align: justify; font-size: 11px">
    Please countersign your acceptance of these terms and within 15 business days after your new lease commences, Docutrend will issue a check
    in the amount of <u>${{number_format($data->payoff_agreement->check_amount, 2)}}</u> for your use towards the remaining payments on your current lease(s).
</p>
@if (count($data->payoff_agreement->body) == 1)
    <p style="font-size: 7px; font-weight: bold; margin-top: 300px">
        Docutrend is not responsible for any fees incurred due to any late or missed payments to your current leasing company once you have received your payoff check from us. A payoff check will be issued
        upon the date of your new lease agreement with Docutrend. Any charges owed prior to that date are your responsibility and you will not hold Docutrend responsible.
    </p>
@endif
@if (count($data->payoff_agreement->body) == 2)
    <p style="font-size: 7px; font-weight: bold; margin-top: 280px">
        Docutrend is not responsible for any fees incurred due to any late or missed payments to your current leasing company once you have received your payoff check from us. A payoff check will be issued
        upon the date of your new lease agreement with Docutrend. Any charges owed prior to that date are your responsibility and you will not hold Docutrend responsible.
    </p>
@endif
@if (count($data->payoff_agreement->body) == 3)
    <p style="font-size: 7px; font-weight: bold; margin-top: 260px">
        Docutrend is not responsible for any fees incurred due to any late or missed payments to your current leasing company once you have received your payoff check from us. A payoff check will be issued
        upon the date of your new lease agreement with Docutrend. Any charges owed prior to that date are your responsibility and you will not hold Docutrend responsible.
    </p>
@endif
@if (count($data->payoff_agreement->body) == 4)
    <p style="font-size: 7px; font-weight: bold; margin-top: 240px">
        Docutrend is not responsible for any fees incurred due to any late or missed payments to your current leasing company once you have received your payoff check from us. A payoff check will be issued
        upon the date of your new lease agreement with Docutrend. Any charges owed prior to that date are your responsibility and you will not hold Docutrend responsible.
    </p>
@endif
@if (count($data->payoff_agreement->body) == 5)
    <p style="font-size: 7px; font-weight: bold; margin-top: 220px">
        Docutrend is not responsible for any fees incurred due to any late or missed payments to your current leasing company once you have received your payoff check from us. A payoff check will be issued
        upon the date of your new lease agreement with Docutrend. Any charges owed prior to that date are your responsibility and you will not hold Docutrend responsible.
    </p>
@endif
@if (count($data->payoff_agreement->body) == 6)
    <p style="font-size: 7px; font-weight: bold; margin-top: 200px">
        Docutrend is not responsible for any fees incurred due to any late or missed payments to your current leasing company once you have received your payoff check from us. A payoff check will be issued
        upon the date of your new lease agreement with Docutrend. Any charges owed prior to that date are your responsibility and you will not hold Docutrend responsible.
    </p>
@endif
@if (count($data->payoff_agreement->body) == 7)
    <p style="font-size: 7px; font-weight: bold; margin-top: 180px">
        Docutrend is not responsible for any fees incurred due to any late or missed payments to your current leasing company once you have received your payoff check from us. A payoff check will be issued
        upon the date of your new lease agreement with Docutrend. Any charges owed prior to that date are your responsibility and you will not hold Docutrend responsible.
    </p>
@endif
@if (count($data->payoff_agreement->body) == 8)
    <p style="font-size: 7px; font-weight: bold; margin-top: 160px">
        Docutrend is not responsible for any fees incurred due to any late or missed payments to your current leasing company once you have received your payoff check from us. A payoff check will be issued
        upon the date of your new lease agreement with Docutrend. Any charges owed prior to that date are your responsibility and you will not hold Docutrend responsible.
    </p>
@endif
@if (count($data->payoff_agreement->body) == 9)
    <p style="font-size: 7px; font-weight: bold; margin-top: 140px">
        Docutrend is not responsible for any fees incurred due to any late or missed payments to your current leasing company once you have received your payoff check from us. A payoff check will be issued
        upon the date of your new lease agreement with Docutrend. Any charges owed prior to that date are your responsibility and you will not hold Docutrend responsible.
    </p>
@endif
@if (count($data->payoff_agreement->body) >= 10)
    <p style="font-size: 7px; font-weight: bold; margin-top: 120px">
        Docutrend is not responsible for any fees incurred due to any late or missed payments to your current leasing company once you have received your payoff check from us. A payoff check will be issued
        upon the date of your new lease agreement with Docutrend. Any charges owed prior to that date are your responsibility and you will not hold Docutrend responsible.
    </p>
@endif

<table style="width: 100%; border-collapse: collapse; margin-top: 10px">
    <tr>
        <td style="border: 2px solid; border-bottom-color: darkgoldenrod"><span style="color: red">X</span></td>
        <td style="border: 2px solid black; font-size: 11px; text-align: center;">{{-- {{$data->billing_contact->title ?? ''}} --}}</td>
        <td style="border: 2px solid black; font-size: 11px; text-align: center">{{-- {{$date}} --}}</td>
    </tr>
    <tr>
        <td style="border: 2px solid; border-bottom-color: black; font-size: 11px; text-align: center; border-right-color: white; border-left-color: white;">Authorized by (customer)</td>
        <td style="border: 2px solid; border-bottom-color: black; font-size: 11px; text-align: center; border-left-color: white; border-right-color: white;">Title</td>
        <td style="border: 2px solid; border-bottom-color: black; font-size: 11px; text-align: center; border-right-color: white;">Date</td>
    </tr>
</table>
<p style="text-align: center; font-size: 9px">
    NJ Office: 29J Commerce Way, Totowa, NJ 07512 / NY Office: 575 8th Ave, NY, NY 10018 / PA Office: 575 Virginia Dr, Ft. Washington, PA 19034
    Service - support@docutrend.com / Supplies - supplies@docutrend.com / Billing - meters@docutrend.com
</p>
</body>
</html>
