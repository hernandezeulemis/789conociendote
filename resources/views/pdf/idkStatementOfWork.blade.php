<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <style>
        body {
            font-family: Arial, Helvetica, sans-serif;
        }

        /*Tabla 1*/
        .tg  {border-collapse:collapse;border-spacing:0; width: 100%; border: 2px solid black}
        .tg td{border-color:black;border-style:solid;border-width:1px;font-family:Arial, sans-serif;font-size:11px;
            overflow:hidden;padding:5px 5px;word-break:normal;}
        .tg th{border-color:black;border-style:solid;border-width:1px;font-family:Arial, sans-serif;font-size:11px;
            font-weight:normal;overflow:hidden;padding:10px 5px;word-break:normal;}
        .tg .tg-1wig{font-weight:bold;text-align:left;vertical-align:top}
        .tg .tg-w747{text-align:left;vertical-align:top}
        .tg .tg-0lax{text-align:left;vertical-align:top
        }

        /*Tabla 2*/
        .tg-2  {border-collapse:collapse;border-spacing:0;}
        .tg-2 td{border-color:black;border-style:solid;border-width:1px;font-family:Arial, sans-serif;font-size:11px;
            overflow:hidden;padding:2px 5px;word-break:normal;}
        .tg-2 th{border-color:black;border-style:solid;border-width:1px;font-family:Arial, sans-serif;font-size:11px;
            font-weight:normal;overflow:hidden;padding:10px 5px;word-break:normal;}
        .tg-2 .tg-zv4m{border-color:#ffffff;text-align:left;vertical-align:top}
        .tg-2 .tg-u0o7{border-color:inherit;font-weight:bold;text-align:left;text-decoration:underline;vertical-align:top}
        .tg-2 .tg-fymr{border-color:inherit;font-weight:bold;text-align:left;vertical-align:top}
        .tg-2 .tg-x6qq{border-color:inherit;text-align:left;vertical-align:top}
        .tg-2 .tg-9353{border-color:inherit;font-weight:bold;text-align:center;text-decoration:underline;vertical-align:top}
        li{
            font-size: 11px;
        }

        .check-container > span {
            display: inline-block;
            margin: 0;
            font-size: 11px;
        }

        .square{
            width: 10px;
            height: 10px;
            border: 1px solid black;
            margin-top: 5px;
            display: inline-block;
            font-size: 9px;
            text-align: center;
            font-weight: bolder;
        }

        .container-option > .check-container{
            display: inline;
        }
        .check-container > img {
            display: inline-block;
        }
    </style>
    <title>Document</title>
</head>
<body>
<div class="header" style="text-align: center">
    <img src="{{asset('images/logo.jpg')}}" alt="" width="500px">
    <p><strong style="text-decoration: underline">STATEMENT OF WORK</strong></p>
</div>

<table class="tg">
    <tbody>
    <tr>
        <td class="tg-1wig">Customer Name</td>
        <td class="tg-w747">{{$data->account->company_legal_name ?? ''}}</td>
        <td class="tg-1wig">Sales Person</td>
        <td class="tg-w747">{{$data->sales_person ?? ''}}</td>
    </tr>
    <tr>
        <td class="tg-1wig">Adress</td>
        <td class="tg-w747" colspan="3">{{$data->account->address1 ?? ''}} {{$data->account->address2 ?? ''}} ZIP:{{$data->account->zipcode ?? ''}} {{$data->account->city ?? ''}} {{$data->account->state ?? ''}}</td>
    </tr>
    <tr>
        <td class="tg-1wig">IT Contact</td>
        <td class="tg-w747">{{$data->it_contact->first_name ?? ''}} {{$data->it_contact->last_name ?? ''}}, {{$data->it_contact->title ?? ''}}  </td>
        <td class="tg-1wig">Second Contact</td>
        <td class="tg-w747"> {{$data->billing_contact->first_name ?? ''}} {{$data->billing_contact->last_name ?? ''}}, {{$data->billing_contact->title ?? ''}}</td>
    </tr>
    <tr>
        <td class="tg-1wig">Email<br></td>
        <td class="tg-w747">{{$data->it_contact->email ?? ''}}</td>
        <td class="tg-1wig">Email</td>
        <td class="tg-w747">{{$data->billing_contact->email ?? ''}}</td>
    </tr>
    <tr>
        <td class="tg-1wig">Phone</td>
        <td class="tg-w747">{{$data->it_contact->phone ?? ''}}</td>
        <td class="tg-1wig">Phone</td>
        <td class="tg-w747">{{$data->billing_contact->phone ?? ''}}</td>
    </tr>
    </tbody>
</table>

<ol>
    <li>Docutrend HelpDesk will contact your IT to gather necessary information for configuring the new device(s) prior to delivery. Any address
        books or information that needs to be imported into the device will be provided at this time.</li>
    <li>If no IT is Available HelpDesk will request a remote session with the customer for purposes of discovery.</li>
    <li>Docutrend delivery will include set up of hardware, finishing options and wired connection to customer’s network.</li>
    <li>Docutrend HelpDesk will remotely assist with selected functionality below for up to three users.</li>
    <li>Docutrend HelpDesk will install FM Audit remote monitoring tool to provide meter readings and automated toner alerts for supply
        fulfilment.</li>
    <li>Docutrend Account Representative will provide onsite training to key operator once installation is completed</li>
</ol>

<strong>Scope of work</strong>
<table class="tg-2" style="width: 100%">
    <tbody>
   {{-- <tr>
        <td class="tg-fymr" width="15%"># of MFP Devices</td>
        <td class="tg-x6qq" width="85%">{{$data->sow->mfp_devices ?? ''}}</td>
    </tr>
    <tr>
        <td class="tg-fymr"># of SF Devices</td>
        <td class="tg-x6qq">{{$data->sow->sf_devices ?? ''}}</td>
    </tr>--}}
    <tr>
        <td class="tg-fymr" style="width: 20%">Number of PCs</td>
        <td class="tg-x6qq" style="width: 70%;"></td>
    </tr>
    <tr>
        <td class="tg-fymr">Number of MACS</td>
        <td class="tg-x6qq"></td>
    </tr>
</table>
@foreach($data->sow->body as $index => $sow)
    <table class="tg-2" style="margin-top: 10px">
        <tbody>
        <tr>
            <td class="tg-9353">Model Numbers</td>
            <td class="tg-9353">Scan</td>
            <td class="tg-9353">Active Directory</td>
            <td class="tg-9353">Dept IDS</td>
            <td class="tg-9353">Address Books</td>
            <td class="tg-9353">Default</td>
            <td class="tg-9353">System Settings cloned onto a new device (same manufacturer)</td>
            <td class="tg-9353">Space Requirement Confirmed</td>
            <td class="tg-9353">Electric Requirement Confirmed</td>
        </tr>
        <tr>
            <td class="tg-x6qq">{{$data->equipment[$index]->make_model}}</td>
            <td class="tg-x6qq"></td>
            <td class="tg-x6qq"></td>
            <td class="tg-x6qq"></td>
            <td class="tg-x6qq"></td>
            <td class="tg-x6qq"></td>
            <td class="tg-x6qq"></td>
            <td class="tg-x6qq"></td>
            <td class="tg-x6qq"></td>
        </tr>

        </tbody>
    </table>

    <table style="width: 100%; border: 1px solid; border-collapse: collapse;">
        <tr>
            <td style="border: 1px solid; padding-left: 10px; padding-top: 10px">
                <div class="container-option">
                    <div class="check-container">
                        <div class="square"></div>
                        <span>115/120V <br>15Amp</span>
                        <img src="{{asset('images/enchufe1.PNG')}}" alt="" width="35px">
                    </div>
                    <div class="check-container">
                        <div class="square"></div>
                        <span>115/120V <br>20Amp</span>
                        <img src="{{asset('images/enchufe2.PNG')}}" alt="" width="35px">
                    </div>
                    <div class="check-container">
                        <div class="square"></div>
                        <span>220V <br>20Amp</span>
                        <img src="{{asset('images/enchufe3.PNG')}}" alt="" width="35px">
                    </div>

                </div>
            </td>
        </tr>
    </table>
@endforeach

<br>
<strong style="font-size: 12px">Customer Acceptance</strong>
<table style="width: 100%; border: 2px solid black; border-collapse: collapse">
    <tr>
        <td style="border: 1px solid black; width: 25%; font-size: 11px">Print Name:</td>
        <td style="border: 1px solid black; width: 25%; font-size: 11px">{{-- {{$name}} --}}</td>
        <td style="border: 1px solid black; width: 25%; font-size: 11px">Signature</td>
        <td style="border: 1px solid black; width: 25%; font-size: 11px"></td>
    </tr>
</table>
<p style="text-align: center; font-size: 7px">
    www.docutrend.com
</p>
</body>
</html>
