<div class="js-cookie-consent cookie-consent">

    <span class="cookie-consent__message">
        {!! trans('cookieConsent::texts.message') !!}
    </span>

    <button class="js-cookie-consent-agree cookie-consent__agree btn btn-info">
        {{ trans('cookieConsent::texts.agree') }}
    </button>

</div>
<style>
.cookie-consent {
    position: fixed;
    padding:10px;
    background:white;
    z-index: 1;
    bottom:0px;
    width: 100%;
    display: flex;
    justify-content: space-between;
    align-items: center;
}
</style>