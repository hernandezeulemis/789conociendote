<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title text-center" id="exampleModalLabel"> Aviso de Privacidad de Datos Personales
                Conociéndote para ASOCIADOS (COLABORADORES)
        </h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div id="privacidad">
            <p>En cumplimiento a la Ley Federal de Protección de Datos Personales en Posesión de Particulares, ponemos a su disposición el presente Aviso de Privacidad. Agradecemos que lea cuidadosamente el presente Aviso de Privacidad, mismo que menciona el uso, tratamiento, plazos y procedimientos a seguir en caso de que considere limitar el uso de los Datos Personales que usted decide otorgarnos.</p>

            <h3>1. Responsable de la protección de sus Datos Personales.</h3>
            <p>Tere Chantal Haiat Zayat, con nombre comercial conocido como Conociéndote, con domicilio fiscal en Hacienda del Ciervo 24 A-601, Fracc. Hacienda de las Palmas, Huixquilucan de Degollado, Estado de México, C.P. 52763, México, es responsable de recabar sus datos personales, así como del tratamiento y protección de los mismos.</p>

            <h3>2. Datos Personales recabados y medios de obtención.</h3>
            <p>Los datos personales que recabemos de usted, incluyendo imágenes y sonidos captados por cámaras de seguridad, con los fines descritos en el numeral (4) cuatro del presente aviso de privacidad, son recabados:</p>
            <ul>
                <li>De forma personal en nuestras oficinas.</li>
                <li>Cuando usted nos proporciona sus datos a través de solicitudes o formatos de registro.</li>
                <li>Cuestionarios de Contacto en nuestra página web.</li>
                <li>A través de nuestras redes sociales y correos electrónicos.</li>
                <li>Vía telefónica.</li>
            </ul>
            <p>Estos datos serán utilizados y resguardados únicamente en nuestras oficinas, en una base de datos bajo la más estricta confidencialidad.</p>
            <p>Los datos personales que recabamos de usted,son los siguientes:</p>
            <ul>
                <li>Nombre completo del solicitante.</li>
                <li>Fotografía.</li>
                <li>Fecha y lugar de nacimiento.</li>
                <li>Nacionalidad.</li>
                <li>Edad.</li>
                <li>Cédula profesional.</li>
                <li>Estudios efectuados.</li>
                <li>Especialización.</li>
                <li>Seminarios en los que se ha participado (Tipo, Lugar, Fecha).</li>
                <li>Eventos en que ha participado.</li>
                <li>Domicilio.</li>
                <li>Ciudad.</li>
                <li>Estado.</li>
                <li>Datos de Clínica / Consultorio.</li>
                <li>Número telefónico local.</li>
                <li>Número de telefonía celular.</li>
                <li>Correo electrónico.</li>
                <li>Constancia de situación Fiscal.</li>
            </ul>
            
            <p>Datos Patrimoniales:</p>
            <p>Los datos patrimoniales que recabamos de usted en forma personal son para operaciones bancarias y facturación:</p>
            <ul>
                <li>Forma de pago.</li>
                <li>Nombre del titular de la cuenta.</li>
                <li>Institución financiera.</li>
                <li>Datos correspondientes de la tarjeta de crédito.</li>
                <li>Datos correspondientes a la cuenta bancaria.</li>
                <li>Sucursal.</li>
                <li>CLABE bancaria.</li>
                <li>Firma autógrafa.</li>
            </ul>
            <p>Los datos personales que nos proporciona serán tratados atendiendo a los principios de licitud, lealtad, proporcionalidad, calidad 
            y únicamente para las finalidades descritas en el presente aviso de privacidad. Así mismo para la protección de sus datos personales estableceremos las medidas de seguridad administrativas, técnicas y físicas que nos permitan proteger sus datos personales contra daño, perdida, alteración, destrucción o un uso distinto al señalado en el presente aviso de privacidad.
            </p>
            <h3>2.1.- Datos de personas en estado de incapacidad y menores de edad.</h3>
            <p>Le informamos que es de nuestro especial interés cuidar la información personal de Personas en estado de interdicción y capacidades diferentes así como de los menores de edad en términos de la ley; los datos que serán tratados y protegidos bajo las más altas medidas de seguridad y confidencialidad.</p>

            <h3>3. Datos Personales Sensibles.</h3>
            <p>Hacemos de su conocimiento que NO recabamos datos sensibles de nuestros asociados.</p>

            <h3>4. Fines de los datos personales recabados.</h3>
            <p>Sus datos personales son recabados con las siguientes finalidades:</p>
            <ul>
                <li>Facilitador de servicios de salud y desarrollo para conectar todo tipo de profesionales de la psicología y la psicoterapia.</li>
                <li>Ser un enlace para la generación de una atención especializada, ofreciendo acompañamiento psicoterapéutico, orientación psicológica y otros servicios relacionados.</li>
                <li>Ser un enlace directo entre el usuario (paciente) y los profesionales de la de salud (asociados) que estén incorporados en nuestra red.</li>
                <li>Generar logística, búsqueda, administración y planeación para el desempeño integral entre el usuario (paciente) y nuestros asociados (colaboradores).</li>
                <li>Administración y manejo de espacios donde los asociados puedan desarrollar su ejercicio profesional.</li>
                <li>Cursos Especiales, Talleres, Programas educacionales, Conferencias.</li>
                <li>Facturación.</li>
            </ul>
            <h3>5. Negativa al tratamiento de sus Datos Personales con finalidades Secundarias.</h3>
            <p>Finalidades secundarias no necesarias para la relación jurídica con el responsable:</p>
            <ul>
                <li>A) Marketing digital.</li>
                <li>B) Flyers informativos de manera física y digital.</li>
                <li>C) Publicación de imágenes y sonidos en redes sociales.</li>
                <li>D) Información sobre nuevos productos y servicios.</li>
                <li>E) Campañas específicas.</li>
                <li>F) Fines estadísticos.</li>
            </ul>
            <p>No acepto que mis datos sean utilizados para las finalidades secundarias antes descritas. <input type="checkbox" name="negativa" value="no_aceptado"></p>
            <h3>6. Transferencia de Datos Personales.</h3>
            <p>Conociéndote, en la medida en que la Ley Federal de Protección de Datos Personales en Posesión de los Particulares (la “Ley”) lo permite, podrá transferir sus datos personales a terceros. Sus Datos Personales no serán comercializados con ningún tercero.</p>
            <p>Dichos Datos Personales pueden ser transferidos a terceros con quien exista algún tipo de relación jurídica o de negocios con el fin de cumplir las finalidades primarias antes señaladas. La transferencia de datos únicamente se hace de nuestros asociados para los usuarios.</p>

            <h3>7. Medios y Procedimiento para el ejercicio de los Derechos ARCO.</h3>
            <p>Para el ejercicio de sus derechos ARCO (Acceso, Rectificación, Cancelación y Oposición) deberá hacer llegar una solicitud por escrito para atención de la Oficina de Capital Humano, vía correo electrónico a la dirección <a href="mailto:privacidad@conociendote.com.mx">privacidad@conociendote.com.mx</a>, o bien, entregarla físicamente en el domicilio indicado en el numeral uno (1) de este Aviso de Privacidad.</p>
            <p>La recepción de las solicitudes es gratuita y se llevará a cabo de lunes a viernes en un horario de 9:00 a 17:00 horas. La solicitud deberá estar redactada en el idioma español y contener los requisitos siguientes:</p>
            <ul>
                <li>a) Nombre del titular.</li>
                <li>b) Domicilio u otro medio para comunicarle la respuesta. De no cumplir el presente requisito se tendrá por no presentada la solicitud. </li>
                <li>c) Documento oficial, en original, que acredite su personalidad y que son: credencial para votar expedida por el Instituto Nacional Electoral (antes Instituto Federal Electoral), pasaporte, cartilla del servicio nacional militar expedida por la Secretaría de la Defensa Nacional y cédula profesional expedida por la Secretaría de Educación Pública. En caso de actuar a través de un apoderado legal, será necesario acreditar la personalidad a través de un poder otorgado e identificación</li>
                <li>d) Una descripción clara y precisa de los datos personales respecto de los cuales se busca ejercer los Derechos ARCO.</li>
                <li>e) Cualquier otro elemento o documentación que facilite la localización de los datos personales.</li>
                <li>f) Una vez recibida su solicitud en los días y horarios señalados en el primer párrafo de este inciso, se tendrán 20 días hábiles contados a partir de la fecha efectiva de recepción para notificarle la determinación adoptada, la cual en caso de ser procedente, será efectiva dentro de los 15 días hábiles siguientes a la fecha en que se comunicó la respuesta.</li>
                <li>g) Hacemos de su conocimiento que, en caso de que la solicitud sea insuficiente o errónea, el plazo señalado en el párrafo anterior será de cinco días hábiles a partir de la fecha efectiva de recepción de la solicitud. Para hacer de su conocimiento el requerimiento que complemente o subsane el error, usted contará con un plazo de 10 días hábiles para atender dicho requerimiento, el cual en caso de ser atendido continuará el proceso, de no hacerlo, la solicitud se tendrá por no presentada.</li>
                <li>h) Tratándose de solicitudes de Acceso a sus datos personales se procederá a la entrega física por medio de copias simples y de manera gratuita debiendo cubrir únicamente los gastos de envío y los costos de reproducción por expedición de copias simples o documentos electrónicos, previa acreditación del titular o representante legal. </li>
            </ul>
            <h3>8. Mecanismos y Procedimiento para revocar su consentimiento.</h3>
            <p>En todo momento usted podrá revocar su consentimiento al tratamiento de sus datos personales, para lo cual es necesario nos haga llegar una solicitud redactada en idioma español a la dirección de correo electrónico <a href="mailto:privacidad@conociendote.com.mx">privacidad@conociendote.com.mx</a>, la cual deberá contener los requisitos señalados en el numeral siete (7) del presente Aviso de Privacidad haciendo señalamiento de los datos personales a los cuales decida revocar dicho consentimiento.</p>
            <p>La recepción de estas solicitudes se llevará a cabo de conformidad a lo establecido en el primer párrafo del numeral siete (7) del presente Aviso de Privacidad.</p>
            <p>En un plazo no mayor de 20 días hábiles contados a partir de la recepción efectiva de su solicitud, haremos la revocación a los datos solicitados.</p>
            <p>Es importante que tenga en cuenta que no en todos los casos podremos atender su solicitud o concluir el uso de forma inmediata, ya que es posible que por alguna obligación legal requiramos seguir tratando sus datos personales. Asimismo, usted deberá considerar que para ciertos fines, la revocación de su consentimiento implicará que no le podamos seguir prestando el servicio que nos solicitó o completar el proceso de selección y reclutamiento, o la conclusión de su relación con nosotros.</p>
            <h3>9. Opciones con las que cuenta el titular para limitar el uso o divulgación de sus Datos Personales.</h3>
            <p>En todo momento usted podrá limitar el consentimiento de uso y divulgación de sus datos personales a través de mecanismos distintos al ejercicio de los derechos ARCO, mecanismos concernientes en enviar un correo electrónico a: <a href="mailto:privacidad@conociendote.com.mx">privacidad@conociendote.com.mx</a> o notificación por escrito, dirigida al Departamento de Protección de Datos, en el que se señale qué datos son los que se quiere limitar su uso.</p>
            <h3>10. Uso de cookies, web beacons u otras tecnologías similares.</h3>
            <p>Las cookies son textos que son descargados automáticamente y almacenados en el disco duro del equipo de cómputo del usuario al navegar en una página de Internet específica, que permite recordar al servidor de Internet o correo electrónico, que puede ser utilizado para monitorear el comportamiento de un visitante, como almacenar información sobre la dirección IP del usuario, duración del tiempo de interacción en dicha página y el tipo de navegador utilizado, entre otros.</p>
            <p>Si usted accede a este Aviso de Privacidad integral a través del sitio web <a href="https://www.conociendote.com.mx">www.conociendote.com.mx</a>, le informamos que el Responsable utiliza cookies para facilitar la navegación en dicho sitio. Las cookies constituyen una herramienta empleada por los servidores web para almacenar y recuperar información que se almacenan en el navegador utilizado por los usuarios o visitantes del sitio web que permiten guardar sus preferencias personales para brindarle una mejor experiencia de navegación.</p>
            <p>Las cookies tienen fecha de caducidad, que puede oscilar desde el tiempo que dura la sesión o visita al sitio web hasta una fecha específica a partir de la cual dejan de ser operativas. Las cookies empleadas en <a href="https://www.conociendote.com.mx">www.conociendote.com.mx</a> se asocian únicamente con un Usuario anónimo y su equipo informático, no proporcionan referencias que permitan deducir el nombre y apellidos del Usuario, no pueden leer datos de su disco duro ni incluir virus en sus textos.</p>
            <p>Usted puede configurar su navegador para aceptar o rechazar automáticamente todas las cookies o para recibir un aviso en pantalla sobre la recepción de cada cookie y decidir en ese momento su implantación o no en su disco duro. Le sugerimos consultar la sección de ayuda de su navegador para saber cómo cambiar la configuración sobre aceptación o rechazo de cookies. Aún y cuando configure su navegador para rechazar todas las cookies o rechace expresamente las cookies de <a href="https://www.conociendote.com.mx">www.conociendote.com.mx</a>, usted podrá seguir navegando por el sitio web con el único inconveniente de no poder disfrutar de las funcionalidades del sitio que requieran la instalación de alguna de ellas.</p>
            <p>En todo caso, usted podrá eliminar las cookies de <a href="https://www.conociendote.com.mx">www.conociendote.com.mx</a> implantadas en su disco duro, en cualquier momento, siguiendo el procedimiento establecido en la sección de ayuda de su navegador.</p>
            <h3>11. Procedimientos y medios a los cambios al Aviso de Privacidad.</h3>
            <p>Nos reservamos el derecho de efectuar modificaciones o actualizaciones al presente aviso de privacidad en cualquier momento, atendiendo novedades legislativas, políticas internas o nuevos requerimientos para la prestación u ofrecimiento de nuestros servicios o productos. Estas modificaciones estarán disponibles al público a través de los medios siguientes.</p>
            <ul>
                <li>A)	A través de la página web www.conociendote.com.mx </li>
                <li>B)	Anuncios visibles en nuestra oficina.</li>
            </ul>
            <h3>12. Tratamiento indebido de sus Datos Personales.</h3>
            <p>Si usted considera que su derecho a protección de datos personales ha sido lesionado por alguna conducta nuestra en estas actuaciones o respuestas, o bien, presume que en el tratamientos de sus datos personales existe alguna violación a las disposiciones previstas de la Ley Federal de Protección de Datos Personales en Posesión de los Particulares, podrá interponer la queja o denuncia correspondiente ante el Instituto Nacional de Transparencia, Acceso a la Información y Protección de Datos Personales. Para mayor información visite <a href="www.inai.org.mx">www.inai.org.mx</a></p>

            <p><strong>Acepto y doy mi consentimiento para que sean recolectados mis datos personales de Identificación, Patrimoniales y Sensibles para el tratamiento de las finalidades señaladas en el presente Aviso de Privacidad.</strong></p>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>
<style>
    #privacidad {
        height: 600px; /* Altura deseada del div */
        overflow-y: auto; /* Aparecerá la barra de desplazamiento solo cuando sea necesario */
    }
</style>