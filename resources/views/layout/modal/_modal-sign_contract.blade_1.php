<style>
    .custom-modal-body {
      max-height: 300px !important; 
      overflow-y: auto !important;
    }
</style>
<!-- Modal para firmar el contrato -->
<div class="modal fade" id="firmarContratoModal" tabindex="-1" role="dialog" aria-labelledby="firmarContratoModalLabel" aria-hidden="true" data-backdrop="static" data-keyboard="false">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="firmarContratoModalLabel">Firmar Contrato</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body custom-modal-body" id="modal-body">
          <div class="container">
            <div class="content_main">
              
              @if($data['users']->id == 312)
                <p>Contrato de Referencia de Pacientes entre la Plataforma Conociéndotemx y el Psicoterapeuta : <strong>{{$data['users']->name}}</strong></p>
              @else
                <p>Contrato de Referencia de Pacientes entre la Plataforma Conociéndotemx y el Psicólogo : <strong>{{$data['users']->name}}</strong></p>
              @endif
                <p><strong>Fecha :</strong> {{$data['fecha']}}</p>
                  @if($data['users']->id == 312)
                    <p>Este contrato de referencia de pacientes se establece entre Conociéndotemx, y el Psicoterapeuta <strong> {{$data['users']->name}} </strong>
                    con número de cédula de Maestría <strong> 13814819 </strong>Plataforma y el Psicoterapeuta serán referidos conjuntamente como "las Partes".</p>
                  @else
                    <p>Este contrato de referencia de pacientes se establece entre Conociéndotemx, y el psicólogo <strong> {{$data['users']->name}} </strong>
                    con número de cédula <strong>{{$data['therapist']->cedula}} </strong>Plataforma y el Psicólogo serán referidos conjuntamente como "las Partes".</p>
                  @endif
                    <h3><strong>1.Objeto del Contrato</strong></h3>
                    <p>La Plataforma se dedica a la referencia de pacientes con psicólogos y tiene como objetivo facilitarel 
                        acceso de los pacientes a servicios de atención psicológica.<br> El Psicólogo ofrece serviciosprofesionales 
                        de psicología y está interesado en recibir referencias de pacientes de la Plataforma.</p>
                    <h3><strong>2.Obligaciones de la Plataforma</strong></h3>
                    <p>2.1. La Plataforma se compromete a promocionar y referir a pacientes al Psicólogo a través de suplataforma en línea.</p>
                    <p>2.2. La Plataforma se reserva el derecho de seleccionar y determinar los pacientes a los que serealizará la referencia, 
                        teniendo en cuenta los criterios establecidos por el Psicólogo (si loshubiera).</p>
                    <p>2.3. La Plataforma se compromete a mantener la confidencialidad de la información personal ymédica de los pacientes, 
                        de acuerdo con las leyes y regulaciones aplicables.</p>
                    <p>2.4. La Plataforma establecerá los honorarios por los servicios de atención psicológica, los cualesserán informados al Psicólogo con antelación.</p>
                    <h3><strong>3. Obligaciones del Psicólogo</strong></h3>
                    <p>3.1. El Psicólogo se compromete a ofrecer servicios de atención psicológica de alta calidad a lospacientes referidos por la Plataforma.</p>
                    <p>3.2. El Psicólogo asegura que cuenta con la licencia y autorización necesarias para ejercer lapsicología y se compromete a mantenerlas 
                        vigentes durante la duración del presente Contrato.</p>
                    <p>3.3. El Psicólogo se compromete a tratar a los pacientes referidos por la Plataforma de maneraprofesional, ética y respetuosa, cumpliendo 
                        con los estándares de práctica y las leyes y regulaciones aplicables.</p>
                    <h3><strong>4. Procedimiento de Referencia de Pacientes</strong></h3>
                    <p>4.1. La Plataforma enviará al Psicólogo un formato con la información del paciente, incluyendo loshonorarios establecidos por la Plataforma.</p>
                    <p>4.2. El Psicólogo tendrá un plazo máximo de 3 horas para responder y aceptar al paciente.</p>
                    <p>4.3. Una vez aceptado el paciente, el Psicólogo deberá contactar al paciente en las siguientes 24horas por medio de un mensaje vía WhatsApp para iniciar el primer contacto.</p>
                    <p>4.4. Es de suma importancia que una vez recibida una respuesta del paciente, el Psicólogonotifique a la Plataforma la fecha acordada para la primera sesión. En caso de no recibir 
                        respuestadel paciente después de 48 horas de enviar el mensaje, también deberá notificarlo a laPlataforma.</p>
                    <h3><strong>5. Pagos y Comisión por Sesión</strong></h3>
                    <p>5.1. Una vez lograda la primera sesión, el terapeuta deberá realizar el pago de las primeras dossesiones + IVA en las siguientes 24 horas a la siguiente cuenta:</p>
                    <p>BANCO: BBVA <br>
                        Nombre del beneficiario: Tere Chantal Haiat Zayat <br>
                        Número de cuenta: 1502406246 <br>
                        Cuenta CLABE: 012180015024062464 <br>
                        Número de Tarjeta: 4152313917987575</p>
                    <p>5.2. Es muy importante que el terapeuta realice el pago dentro de dicho período de tiempo paraque el Psicólogo pueda continuar recibiendo pacientes referidos por la Plataforma.</p>
                    <p>5.3. El Psicólogo se compromete a realizar el pago de la comisión correspondiente a dos sesionesmás IVA, incluso si se ha llevado a cabo únicamente una sesión con el paciente.
                            Dicho pagodeberá realizarse dentro de las siguientes 24 horas posteriores a la celebración de la primerasesión con el paciente.</p>
                    <p>5.4 En el caso de que el terapeuta no pueda asistir a la sesión acordada inicialmente y no hayanotificado con al menos 12 horas de anticipación, se requerirá el pago de la tarifa
                            correspondiente,independientemente de si la sesión se lleva a cabo o no.</p>
                    <h3><strong>6. Comunicación y Seguimiento</strong></h3>
                    <p>6.1. La Plataforma proporcionará una línea telefónica para el contacto y seguimiento de lospacientes, y se solicita al Psicólogo que utilice este medio para cualquier tema relacionado con laplataforma.</p>
                    <p>6.2. El Psicólogo se compromete a notificar a la Plataforma cualquier información relevanterelacionada con el paciente, como cancelaciones de citas, fechas de agendamiento, entre otros.</p>
                    <h3><strong>7. No transferencia de pacientes</strong></h3>
                    <p>7.1. El Psicólogo se compromete a no referir a un paciente que haya llegado a través de laPlataforma a otro profesional o institución sin previa autorización de la Plataforma.</p>
                    <p>7.2. En caso de que el Psicólogo no pueda atender a un paciente referido, se compromete anotificar de inmediato a la Plataforma, proporcionando los motivos y detalles necesarios. LaPlataforma se encargará de gestionar la derivación adecuada del paciente a otro psicólogo oinstitución.</p>
                    <h3><strong>8. Confidencialidad</strong></h3>
                    <p>8.1. Tanto la Plataforma como el Psicólogo se comprometen a mantener la confidencialidad de lainformación personal y médica de los pacientes, de acuerdo con las leyes y regulacionesaplicables.</p>
                    <p>8.2. El Psicólogo se compromete a no divulgar información confidencial del paciente a terceros sinel consentimiento previo del paciente, excepto en los casos en que la ley lo requiera.</p>
                    <h3><strong>9. Duración y Terminación</strong></h3>
                    <p>9.1. Este Contrato tendrá una duración indefinida y podrá ser terminado por cualquiera de lasPartes mediante notificación por escrito a la otra Parte, siempre y cuando se hayan liquidadotodos los pagos pertinentes.</p>
                    <p>9.2. En caso de incumplimiento grave de las obligaciones establecidas en este Contrato,cualquiera de las Partes podrá darlo por terminado de forma inmediata.</p>

            </div>
          </div>
      </div>
      <div class="modal-footer">
        
        <button type="button" class="btn btn-primary" id="firma-contrato">Firmar Contrato</button>
      </div>
    </div>
  </div>
</div>

<script>
  document.addEventListener('DOMContentLoaded', function() {
    var modalBody = document.getElementById('modal-body');
    var buttonFirmarContrato = document.getElementById('firma-contrato');
        buttonFirmarContrato.disabled = true;
    
      modalBody.addEventListener('scroll', function() {
        if (modalBody.scrollHeight - modalBody.scrollTop === modalBody.clientHeight) {
          buttonFirmarContrato.disabled = false;
        } else {
          buttonFirmarContrato.disabled = true;
        }
      });

      $('#firma-contrato').click(function() {
			// Realizar la solicitud AJAX al controlador de Laravel
			$.ajax({
				url: 'firmar-contrato',
				type: 'POST',
				headers: {
                      "X-CSRF-TOKEN": token
                  },
				success: function(response) {
					//$('#firmarContratoModal').modal('hide');
					location.reload()
				},
				error: function(jqXHR, textStatus, errorThrown) {
					console.log(errorThrown);
				}
			});
		});


  });

</script>