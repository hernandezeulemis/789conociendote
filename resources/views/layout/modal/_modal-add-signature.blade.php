<style>
    .fondo-firma{
        background: #f5f5f5;
    }
    .leyenda{
      color: red;
      font-weight: bold;
      text-align: center;
    }
</style>
<div class="modal fade" id="agregarFirmaModal" tabindex="-1" role="dialog" aria-labelledby="agregarFirmaModalLabel" aria-hidden="true" data-backdrop="static" data-keyboard="false">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="agregarFirmaModalLabel">Agregar Firma</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;x</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="fondo-firma">
          <form id="form-firma">
              <canvas  id="signatureCanvas" width="400" height="150">canva</canvas>
              <input type="hidden" name="imgfirma" id="imgFirma">
          </form>
        </div>
        <div>
          <p class="leyenda">La firma no se registra ni se guarda hasta que se firme el contrato.</p>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary"  id="clear">Limpiar</button>
        <button class="btn btn-primary" id="save">Guardar</button>
      </div>
    </div>
  </div>
</div>

<script src="https://cdn.jsdelivr.net/npm/signature_pad@4.0.0/dist/signature_pad.umd.min.js"></script>
<script>
 
  

    document.addEventListener('DOMContentLoaded', function() {
         var canvas = document.getElementById('signatureCanvas');
        //var signaturePad = new SignaturePad(canvas);
        const signaturePad = new SignaturePad(canvas, {
            minWidth: 1,
            maxWidth: 5,
            backgroundColor: 'rgba(255, 255, 255, 0)',
            penColor: 'rgb(0, 0, 0)'
        });

        var img = new Image(),
            $canvas = $("<canvas>"), // create an offscreen canvas
            canvas = $canvas[0],
            context = canvas.getContext("2d");

        img.onload = function () {
          context.drawImage(this, 0, 0); // put the image in the canvas
          $("body").append($canvas);
          removeBlanks(this.width, this.height);
        };
        
        var saveButton = document.getElementById('save');
        var cancelButton = document.getElementById('clear');
        var form = document.getElementById('form-firma');

        saveButton.addEventListener('click', function (event) {

        var data = signaturePad.toDataURL('image/png');
        
        var formData = new FormData(form);
                    
          formData.append('firma', data);
         

          $.ajax({
                  type: "POST",
                  url: "add-firma",
                  headers: {
                      "X-CSRF-TOKEN": token
                  },
                  data: formData,
                  processData: false, 
                  contentType: false, 
                  success: function(response) {
                      location.reload()
                  },
                  error: function(xhr, status, error) {
                      console.log(xhr.responseText);
                  }
              });
        });

      cancelButton.addEventListener('click', function (event) {
        
        signaturePad.clear();
      });

      

    });
</script>