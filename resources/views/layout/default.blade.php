<!DOCTYPE html>
<html
    lang="{{ str_replace('_', '-', app()->getLocale()) }}" {{ Metronic::printAttrs('html') }} {{ Metronic::printClasses('html') }}>
<head>
    <meta charset="utf-8"/>

    {{-- Title Section --}}
    <title>{{-- {{ config('app.name') }} --}} @yield('title', $page_title ?? '')</title>

    {{-- Meta Data --}}
    <meta name="description" content="@yield('page_description', $page_description ?? '')"/>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no"/>

    {{-- Favicon --}}
    <link rel="shortcut icon" href="/media/favicon.ico"/>
    

    {{-- Fonts --}}
    {{ Metronic::getGoogleFontsInclude() }}

    {{-- Global Theme Styles (used by all pages) --}}
    @foreach(config('layout.resources.css') as $style)
        <link href="{{ config('layout.self.rtl') ? asset(Metronic::rtlCssPath($style)) : asset($style) }}"
              rel="stylesheet" type="text/css"/>
    @endforeach

    {{-- Layout Themes (used by all pages) --}}
    @foreach (Metronic::initThemes() as $theme)
        <link href="{{ config('layout.self.rtl') ? asset(Metronic::rtlCssPath($theme)) : asset($theme) }}"
              rel="stylesheet" type="text/css"/>
    @endforeach

    <link href="{{ asset('plugins/custom/datatables/datatables.bundle.css') }}" rel="stylesheet" type="text/css"/>
    <link rel="stylesheet" href="{{asset('css/custom.css')}}">
    <link rel="stylesheet" href="{{ asset('css/custom-starter/jquery.feedback.min.css') }}" />
    <link href="{{ asset('css/all.min.css') }}" rel="stylesheet" type="text/css"/>
    
    <link href="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote-bs4.css" rel="stylesheet">
    {{-- Includable CSS --}}
    @yield('styles')
    
</head>

<body {{ Metronic::printAttrs('body') }} {{ Metronic::printClasses('body') }}>
<!--  cookie consent -->
    
<!-- endcookies -->
@if (config('layout.page-loader.type') != '')
    @include('layout.partials._page-loader')
@endif

@include('layout.base._layout')
<a href="#" id="feedback" class="float">
    <i class="far fa-comment-alt my-float"></i>
</a>


{{-- Global Config (global config for global JS scripts) --}}
<script>
    var KTAppSettings = {!! json_encode(config('layout.js'), JSON_PRETTY_PRINT|JSON_UNESCAPED_SLASHES) !!};
</script>

{{-- Global Theme JS Bundle (used by all pages)  --}}
@foreach(config('layout.resources.js') as $script)
    <script src="{{ asset($script) }}" type="text/javascript"></script>
@endforeach
{{-- Feedback Plugin --}}
<script src="{{ asset('js/custom-starter/jquery.feedback-copy.min.js') }}"></script>
<script src="{{ asset('js/custom-starter/html2canvas.min.js') }}"></script>


{{-- vendors --}}
<script src="{{ asset('plugins/custom/datatables/datatables.bundle.js') }}" type="text/javascript"></script>
<script src="{{ asset('js/custom-starter/chart.js') }}" type="text/javascript"></script>
{{-- Includable JS --}}

<form method="post" id="delete-hidden-form" style="display: none;">
    <input type="hidden" name="_method" value="DELETE"/>
    @csrf
</form>

<script>
    CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
    const role = '{!! auth()->user()->roles !!}'.replaceAll('\\','\\\\')
    window.permissionsUser = parse('{!!  auth()->user()->getAllPermissions()  !!}')
    window.rolesUser = parse(role)

    function parse(j) {
        return JSON.parse(j)
    }
    
    $(document).on('click', '.delete-btn', function (e) {
        e.preventDefault();
        e.preventDefault();
        $this = $(this);
        Swal.fire({
            title: '¿Estás seguro?',
            text: "Esta acción no se puede deshacer",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Sí',
            cancelButtonText: 'No'
        }).then((result) => {
            if (result.value) {
                $this.closest("form").submit();
            }
        })
    });

    $(".datatable").dataTable();

    $(".datepicker-past").datepicker({
        format: 'yyyy-mm-dd',
        endDate: '0d'
    });
    $(".datepicker").datepicker({
        format: 'yyyy-mm-dd',
        endDate: '0d',
        orientation: 'bottom'
    });
    $(".datepicker-future").datepicker({
        format: 'yyyy-mm-dd',
        startDate: '0d'
    });

    $(document).ready(function(){
        $.feedback.text = {
        label : 'Enviar comentario',
		steps : {
			first : {
				label : 'Añade tu comentario',
				holder : 'Escribe tu comentario'
			},
			second : {
				label : 'Selecciona la parte donde ocurrio el error',
				showSection : 'Ocultar sección',
				hideSection : 'Mostrar sección'
			},
			third : {
				label : 'Resumen',
				comment : 'Tu comentario',
				globalInfo : 'Información Global',
				applicationInfo : 'Información de aplicación',
				clientInfo : 'Información del navegador'
			}
		},
		action : {
			'close' : 'Cerrar',
			'next' : 'Siguiente',
			'resume' : 'Reaundar',
			'send' : 'Enviar'

		},
		alerts : {
			success : {
				label : 'Success',
				message : 'El feedback ha sido enviado con éxito'
			},
			error : {
				label : 'Error',
				message : 'Ocurrio un error al enviar el feedBack'
			}
		}
    };
        var app = $('body').feedback({
            sendFeedBackPath : '/feedback'
        });
        $('#feedback').click(() => {                
            app.launch();
            setTimeout(() => {
                var close = document.querySelectorAll('[data-dismiss]')[1]
                close.addEventListener('click',()=>{
                    app.stop();
                    $('.modal-backdrop').remove();
                })
            }, 3000);
        });


    });
    

</script>
<script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>
<script src="https://unpkg.com/axios/dist/axios.min.js"></script>
<script src="https://unpkg.com/vuejs-datepicker"></script>
<script>
    Vue.prototype.$http = window.axios;
</script>
@yield('scripts')

</body>
</html>

