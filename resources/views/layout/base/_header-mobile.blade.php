{{-- Header Mobile --}}
<div id="kt_header_mobile" class="header-mobile {{ Metronic::printClasses('header-mobile', false) }}" {{ Metronic::printAttrs('header-mobile') }}>
    <div class="mobile-logo">
        <a href="{{ url('/') }}">
            <img alt="{{ config('app.name') }}" style="max-width: 50px;" src="{{ asset('media/logo.png') }}"/>
        </a>
    </div>
    <div class="mobile-toolbar">

        @if (config('layout.aside.self.display'))
            <button class="burger-icon burger-icon-left" id="kt_aside_mobile_toggle"><span></span></button>
        @endif

        <!-- @if (config('layout.header.menu.self.display'))
            <button class="burger-icon burger-icon-left ml-3" id="kt_header_mobile_toggle"><span></span></button>
        @endif -->
    </div>
</div>
