{{-- Content --}}
@if (config('layout.content.extended'))
    @yield('content')
@else
    <div class="d-flex flex-column-fluid">

        <div class="{{ Metronic::printClasses('content-container', false) }}">
            @if (session('success'))
                <div class="alert alert-success" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                    <div class="d-flex align-items-center">
                        <span>{{ session('success') }}</span>
                    </div>
                </div>
            @endif
            @if (session('warning'))
                    <div class="alert alert-warning" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                        <div class="d-flex align-items-center">
                            <span>{{ session('warning') }}</span>
                        </div>
                    </div>
            @endif
            @if ($errors->any())
                @foreach ($errors->all() as $error)
                    <div class="alert alert-danger" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                        <div class="d-flex align-items-center">
                            <span>{{$error}}</span>
                        </div>
                    </div>
                @endforeach
            @endif
            @yield('content')
        </div>
    </div>
@endif
