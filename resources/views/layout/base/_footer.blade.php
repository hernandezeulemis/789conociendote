{{-- Footer --}}

<div class="footer bg-white py-4 d-flex flex-lg-column {{ Metronic::printClasses('footer', false) }}" id="kt_footer">
    {{-- Container --}}
    <div class="{{ Metronic::printClasses('footer-container', false) }} d-flex flex-column flex-md-row align-items-center justify-content-between">
        {{-- Copyright --}}
        <div style="width: 100%;" >
            <span class="text-muted font-weight-bold mr-2">{{ date("Y") }} &copy;</span>
            <a href="https://789.mx" target="_blank" class="text-dark-75 text-hover-primary">Desarrollado por 789.MX</a> 
            <button class="btn btn-primary btn-sm" style="color:white;float:right"  data-toggle="modal" data-target="#exampleModal">Aviso de Privacidad</button>
        </div>

        {{-- Nav --}}
{{--        <div class="nav nav-dark order-1 order-md-2">--}}
{{--            <a href="https://keenthemes.com/metronic" target="_blank" class="nav-link pr-3 pl-0">About</a>--}}
{{--            <a href="https://keenthemes.com/metronic" target="_blank" class="nav-link px-3">Team</a>--}}
{{--            <a href="https://keenthemes.com/metronic" target="_blank" class="nav-link pl-3 pr-0">Contact</a>--}}
{{--        </div>--}}
    </div>
</div>

@include('layout.modal._modalprivacidad')