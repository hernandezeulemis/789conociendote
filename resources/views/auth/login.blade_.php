<!DOCTYPE html>
<html lang="en">
<!--begin::Head-->
<head>
    <base href="../../../../">
    <meta charset="utf-8"/>
    <title>Login</title>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no"/>
    <!--begin::Fonts-->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700"/>
    <!--end::Fonts-->
    <!--begin::Page Custom Styles(used by this page)-->
    <link href="/css/pages/login/classic/login-4.css?v=7.0.4" rel="stylesheet" type="text/css"/>
    <!--end::Page Custom Styles-->
    <!--begin::Global Theme Styles(used by all pages)-->
    <link href="/plugins/global/plugins.bundle.css?v=7.0.4" rel="stylesheet" type="text/css"/>
    <link href="/plugins/custom/prismjs/prismjs.bundle.css?v=7.0.4" rel="stylesheet" type="text/css"/>
    <link href="/css/style.bundle.css?v=7.0.4" rel="stylesheet" type="text/css"/>
    <!--end::Global Theme Styles-->
    <!--begin::Layout Themes(used by all pages)-->
    <link href="/css/themes/layout/header/base/light.css?v=7.0.4" rel="stylesheet" type="text/css"/>
    <link href="/css/themes/layout/header/menu/light.css?v=7.0.4" rel="stylesheet" type="text/css"/>
    <link href="/css/themes/layout/brand/dark.css?v=7.0.4" rel="stylesheet" type="text/css"/>
    <link href="/css/themes/layout/aside/dark.css?v=7.0.4" rel="stylesheet" type="text/css"/>
    <!--end::Layout Themes-->
    <link rel="shortcut icon" href="/media/favicon.ico"/>
</head>
<!--end::Head-->
<!--begin::Body-->
<body id="kt_body"
      class="header-fixed header-mobile-fixed subheader-enabled subheader-fixed aside-enabled aside-fixed aside-minimize-hoverable page-loading">
<!--begin::Main-->
<div class="d-flex flex-column flex-root">
    <!--  cookie consent -->
       @include('cookie-consent::index')
    <!-- endcookies -->
    <!--begin::Login-->
    <div class="login login-4 login-signin-on d-flex flex-row-fluid" id="kt_login">
        <div class="d-flex flex-center flex-row-fluid bgi-size-cover bgi-position-top bgi-no-repeat"
             style="background-image: url('/media/bg/bg-3.jpg');">
            <div class="login-form text-center p-7 position-relative overflow-hidden">
                <!--begin::Login Header-->
                <div class="d-flex flex-center mb-15">
                    <a href="#">
                        <img src="/media/logo.png" alt="" width="140"/>
                    </a>
                </div>
                <div class="login-signin">
                    <div class="mb-20">
                        <h3>Iniciar Sesion</h3>
                        <div class="text-muted font-weight-bold">Ingrese sus credenciales:</div>
                    </div>
                    <form class="form" id="kt_login_signin_form" method="post" action="{{route('login')}}">
                        @csrf
                        
                        <div class="form-group mb-5">
                            <input class="form-control h-auto form-control-solid py-4 px-8" type="text"
                                placeholder="Correo Electronico" name="email" autocomplete="off"/>
                        </div>
                        <div class="form-group mb-5">
                            <input class="form-control h-auto form-control-solid py-4 px-8" type="password"
                                placeholder="Contraseña" name="password"/>
                        </div>
                        <button id="kt_login_signin_submit" type="submit"
                                class="btn btn-primary font-weight-bold px-9 py-4 my-3 mx-4">Entrar
                        </button>
                    </form>
                    @if (count($errors))
                        <ul>
                            @foreach($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    @endif
                </div>
            </div>
        </div>
    </div>
    <!--end::Login-->
    
</div>
<!--end::Main-->
<!--end::Global Config-->
<!--begin::Global Theme Bundle(used by all pages)-->
<script src="/plugins/global/plugins.bundle.js?v=7.0.4"></script>
<script src="/plugins/custom/prismjs/prismjs.bundle.js?v=7.0.4"></script>
<script src="/js/scripts.bundle.js?v=7.0.4"></script>
<!--end::Global Theme Bundle-->
<!--begin::Page Scripts(used by this page)-->
{{--<script src="/js/pages/custom/login/login-general.js?v=7.0.4"></script>--}}
<!--end::Page Scripts-->
</body>
<!--end::Body-->
</html>
