<!DOCTYPE html>
<html lang="en">
<!--begin::Head-->

<head>
    <base href="../../../../">
    <meta charset="utf-8" />
    <title>Contrato</title>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />

    <style>
         .header {

            position: fixed;

            top: 0cm;

            left: 0cm;

            right: 0cm;

            height: 1cm;

  

            line-height: 30px;
            font-family: revert;
            width: 100%;
            height: auto;

        }



        footer {

            position: fixed;

            bottom: 0cm;

            left: 0cm;

            right: 0cm;

            height: 1cm;

            background-color: #e4cac0;

            color: white;

            text-align: center;

            line-height: 35px;
            font-family: revert;

        }
        .container{
           margin:0;
           padding: 0;
        }
        .franja_1{
            background-color: #c3c3c3;
            height: 117px;
        }
        .franja_2{
            background-color: #f9d4b5;
            height: 50px;
        }
        .div-con-lineas {
        position: relative;
        width: 180px;
        height: 200px;
  
        }

        .div-con-lineas::before,
        .div-con-lineas::after {
            content: "";
            position: absolute;
            top: -167px;
            left: 677%;
            width: 100%;
            height: 84%;
            background-color: white;
        }

        .div-con-lineas::before {
        /*         border-left: 2px solid #000000;
        border-bottom: 2px solid #000000; */
       /*  transform: skew(24deg);
        left: -2px; */
        display: none;
        }

        .div-con-lineas::after {
     /*    border-right: 2px solid #000000;
        border-top: 2px solid #000000; */
        transform: skew(24deg);
        right: -2px;
        }
        .logoimg{
            width: 100%;
    
            top: 0cm;
            left: 0cm;
            right: 0cm;
         
         /* max-height: 113px;
            margin-top: -5px;
            position: absolute;
            top: 180px;
            left: 74%;
            z-index: 5; */
        }
        .content_main{
            margin-top: 15%;
            top: 40%;
            text-align: justify;
        }
           @page {

            margin: 0cm 0cm;

            font-family: revert;

        }



        body {

            margin: 1cm 1cm 1cm;
            font-family: revert;
        }

    </style>
   
</head>


<body>

  <div class="container">
    <div class="cabecera">
        <div class="logo">
            <img class="header"  alt="" src="{{ asset('media/cabecera_2.jpeg') }}" />
        </div>
    </div>
  
    <div class="content_main">
        <p>Contrato de Referencia de Pacientes entre la Plataforma Conociéndotemx y el Psicólogo : <strong>{{($data['users']) ? $data['users']->name : '_____________'}}</strong></p>
        <p><strong>Fecha :</strong> {{($data['fecha']) ? $data['fecha'] : '_______'}}</p>
        <p>Este contrato de referencia de pacientes se establece entre Conociéndotemx, y el psicólogo <strong> {{($data['users']) ? $data['users']->name : '________________'}} </strong>
            con número de cedula <strong>{{($data['therapist']) ? $data['therapist']->cedula : '____________'}}   </strong>Plataforma y el Psicólogo serán referidos conjuntamentecomo "las Partes".</p>
            <h3><strong>1.Objeto del Contrato</strong></h3>
            <p>La Plataforma se dedica a la referencia de pacientes con psicólogos y tiene como objetivo facilitarel 
                acceso de los pacientes a servicios de atención psicológica.<br> El Psicólogo ofrece serviciosprofesionales 
                de psicología y está interesado en recibir referencias de pacientes de la Plataforma.</p>
            <h3><strong>2.Obligaciones de la Plataforma</strong></h3>
            <p>2.1. La Plataforma se compromete a promocionar y referir a pacientes al Psicólogo a través de suplataforma en línea.</p>
            <p>2.2. La Plataforma se reserva el derecho de seleccionar y determinar los pacientes a los que serealizará la referencia, 
                teniendo en cuenta los criterios establecidos por el Psicólogo (si loshubiera).</p>
            <p>2.3. La Plataforma se compromete a mantener la confidencialidad de la información personal ymédica de los pacientes, 
                de acuerdo con las leyes y regulaciones aplicables.</p>
            <p>2.4. La Plataforma establecerá los honorarios por los servicios de atención psicológica, los cualesserán informados al Psicólogo con antelación.</p>
            <h3><strong>3. Obligaciones del Psicólogo</strong></h3>
            <p>3.1. El Psicólogo se compromete a ofrecer servicios de atención psicológica de alta calidad a lospacientes referidos por la Plataforma.</p>
            <p>3.2. El Psicólogo asegura que cuenta con la licencia y autorización necesarias para ejercer lapsicología y se compromete a mantenerlas 
                vigentes durante la duración del presente Contrato.</p>
            <p>3.3. El Psicólogo se compromete a tratar a los pacientes referidos por la Plataforma de maneraprofesional, ética y respetuosa, cumpliendo 
                con los estándares de práctica y las leyes yregulaciones aplicables.</p>
            <h3><strong>4. Procedimiento de Referencia de Pacientes</strong></h3>
            <p>4.1. La Plataforma enviará al Psicólogo un formato con la información del paciente, incluyendo loshonorarios establecidos por la Plataforma.</p>
            <p>4.2. El Psicólogo tendrá un plazo máximo de 3 horas para responder y aceptar al paciente.</p>
            <p>4.3. Una vez aceptado el paciente, el Psicólogo deberá contactar al paciente en las siguientes 24horas por medio de un mensaje vía WhatsApp para iniciar el primer contacto.</p>
            <p>4.4. Es de suma importancia que una vez recibida una respuesta del paciente, el Psicólogonotifique a la Plataforma la fecha acordada para la primera sesión. En caso de no recibir 
                respuestadel paciente después de 48 horas de enviar el mensaje, también deberá notificarlo a laPlataforma.</p>
            <h3><strong>5. Pagos y Comisión por Sesión</strong></h3>
            <p>5.1. Una vez lograda la primera sesión, el terapeuta deberá realizar el pago de las primeras dossesiones + IVA en las siguientes 24 horas a la siguiente cuenta:</p>
            <p>BANCO: BBVA <br>
                Nombre del beneficiario: Tere Chantal Haiat Zayat <br>
                Número de cuenta: 1502406246 <br>
                Cuenta CLABE: 012180015024062464 <br>
                Número de Tarjeta: 4152313917987575</p>
            <p>5.2. Es muy importante que el terapeuta realice el pago dentro de dicho período de tiempo paraque el Psicólogo pueda continuar recibiendo pacientes referidos por la Plataforma.</p>
            <p>5.3. El Psicólogo se compromete a realizar el pago de la comisión correspondiente a dos sesionesmás IVA, incluso si se ha llevado a cabo únicamente una sesión con el paciente.
                    Dicho pagodeberá realizarse dentro de las siguientes 24 horas posteriores a la celebración de la primerasesión con el paciente.</p>
            <p>5.4 En el caso de que el terapeuta no pueda asistir a la sesión acordada inicialmente y no hayanotificado con al menos 12 horas de anticipación, se requerirá el pago de la tarifa
                    correspondiente,independientemente de si la sesión se lleva a cabo o no.</p>
            <h3><strong>6. Comunicación y Seguimiento</strong></h3>
            <p>6.1. La Plataforma proporcionará una línea telefónica para el contacto y seguimiento de lospacientes, y se solicita al Psicólogo que utilice este medio para cualquier tema relacionado con laplataforma.</p>
            <p>6.2. El Psicólogo se compromete a notificar a la Plataforma cualquier información relevanterelacionada con el paciente, como cancelaciones de citas, fechas de agendamiento, entre otros.</p>
            <h3><strong>7. No transferencia de pacientes</strong></h3>
            <p>7.1. El Psicólogo se compromete a no referir a un paciente que haya llegado a través de laPlataforma a otro profesional o institución sin previa autorización de la Plataforma.</p>
            <p>7.2. En caso de que el Psicólogo no pueda atender a un paciente referido, se compromete anotificar de inmediato a la Plataforma, proporcionando los motivos y detalles necesarios. LaPlataforma se encargará de gestionar la derivación adecuada del paciente a otro psicólogo oinstitución.</p>
            <h3><strong>8. Confidencialidad</strong></h3>
            <p>8.1. Tanto la Plataforma como el Psicólogo se comprometen a mantener la confidencialidad de lainformación personal y médica de los pacientes, de acuerdo con las leyes y regulacionesaplicables.</p>
            <p>8.2. El Psicólogo se compromete a no divulgar información confidencial del paciente a terceros sinel consentimiento previo del paciente, excepto en los casos en que la ley lo requiera.</p>
            <h3><strong>9. Duración y Terminación</strong></h3>
            <p>9.1. Este Contrato tendrá una duración indefinida y podrá ser terminado por cualquiera de lasPartes mediante notificación por escrito a la otra Parte, siempre y cuando se hayan liquidadotodos los pagos pertinentes.</p>
            <p>9.2. En caso de incumplimiento grave de las obligaciones establecidas en este Contrato,cualquiera de las Partes podrá darlo por terminado de forma inmediata.</p>

    </div>
    <div>
        <p>Ambas partes declaran haber leído y aceptado los términos y condiciones de este Contrato.</p>
        <table width="100%" style="margin: 0 auto;">
            <tr>
                <td>
                    @if($data['users']->signature_path)
                        <img src="{{ asset('uploads/'.$data['users']->signature_path) }}" width="100" height="70">
                    @else
                    ___________________________
                    @endif 
                </td>
                <td><img src="{{ asset('img/firma-conociendote.png')}}" width="150" height="90"> <br></td>
            </tr>
            <tr>
                <td><p>Firma del Psicologo</p></td>
                <td><p>Firma de la Plataforma</p></td>
            </tr>
            <tr>
                <td><p><strong>Fecha :</strong> {{$data['fecha']}}</p></td>
                <td><p><strong>Fecha :</strong> {{$data['fecha']}}</p></td>
            </tr>
        </table>
         
         
        
        
        
    </div>
  </div>
  <footer></footer>
</body>

</html>
