{{-- Extends layout --}}
@extends('layout.default')
@section('title',__($_title))
{{-- Content --}}
@section('content')
@section('styles')

@endsection
@if (!auth()->user()->has_signature)
    @include('layout.modal._modal-sign_contract') 
@endif

@if (auth()->user()->has_signature && auth()->user()->status_sign === 'pendiente')
    @include('layout.modal._modal-add-signature') 
@endif

@section('scripts')
<script>
    var user = {!! json_encode(auth()->user()) !!};
    let token = '{{ csrf_token() }}'
    
    document.addEventListener('DOMContentLoaded', function() {
        if(user.has_signature == 0){
            $('#firmarContratoModal').modal('show');
        }

        if(user.has_signature && user.status_sign === 'pendiente'){
            $('#agregarFirmaModal').modal('show');
        }
        
    });
</script>
@endsection