{{-- Extends layout --}}
@extends('layout.default')
@section('title',__($_title))
{{-- Content --}}
@section('content')
    <div class="card">
        <div class="card-body">
            <div class="row">
                <div class="col-md-12">
                    <table id="index-table-log" class="display table table-striped table-bordered"
                           style="width:100%">
                           <thead>
                               <tr role="row">
                                    <th >
                                        ID Log
                                    </th>
                                    <th >
                                        Titulo
                                    </th>
                                   <th >
                                        ID Modelo
                                    </th>
                                    <th >
                                        Modelo
                                    </th>
                                    <th >
                                        Usuario Id
                                    </th>
                                    <th >
                                        Url
                                    </th>
                                    <th >
                                        Ip
                                    </th>
                                    <th >
                                        Action
                                    </th>
                                    <th >
                                        Navegador
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($logsActions as $log)
                                    
                                <tr role="row" class="odd">
                                    <td class="sorting_1 dtr-control">
                                        {{$log->id}}
                                    </td>
                                    <td>
                                        {{$log->title}}
                                    </td>
                                    <td class="sorting_1 dtr-control">
                                        {{$log->id_model}}
                                    </td>
                                    <td class="sorting_1 dtr-control">
                                        {{$log->model}}
                                    </td>
                                    <td class="sorting_1 dtr-control">
                                        {{$log->user_id}}
                                    </td>
                                    <td class="sorting_1 dtr-control">
                                        {{$log->url}}
                                    </td>
                                    <td class="sorting_1 dtr-control">
                                        {{$log->ip}}
                                    </td>
                                    <td class="sorting_1 dtr-control">
                                        {{$log->controller_action}}
                                    </td>
                                    <td>
                                        {{$log->browser}}
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                </div>
            </div>
        </div>
    </div>

@endsection

{{-- Styles Section --}}
@section('styles')

@endsection

{{-- Scripts Section --}}
@section('scripts')
<script src="{{ asset('plugins/custom/datatables/datatables.bundle.js') }}" type="text/javascript"></script>

<script>
    $('#index-table-log').DataTable({
        destroy: true,
        order: [[ 0, "desc" ]],
        responsive: true,
        language: {
            sProcessing:    "Procesando...",
            sLengthMenu:    "Mostrar _MENU_ registros",
            sZeroRecords:   "No se encontraron resultados",
            sEmptyTable:    "Ningún dato disponible en esta tabla",
            sInfo:          "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
            sInfoEmpty:     "Mostrando registros del 0 al 0 de un total de 0 registros",
            sInfoFiltered:  "(filtrado de un total de _MAX_ registros)",
            sInfoPostFix:   "",
            sSearch:        "Buscar:",
            sUrl:           "",
            sInfoThousands:  ",",
            sLoadingRecords: "Cargando...",
            oPaginate: {
                sFirst:    "Primero",
                sLast:    "Último",
                sNext:    "Siguiente",
                sPrevious: "Anterior"
            },
            oAria: {
                sSortAscending:  ": Activar para ordenar la columna de manera ascendente",
                sSortDescending: ": Activar para ordenar la columna de manera descendente"
            }
        }
    });
    
    
</script>
@endsection

