{{-- Extends layout --}}
@extends('layout.default')
@section('title',__($_title))
{{-- Content --}}
@section('content')
    <div class="card">
        <div class="card-body">
            <div class="row">
                <div class="col-md-12">
                    <table id="index-table-log" class="display table table-striped table-bordered"
                           style="width:100%">
                           <thead>
                               <tr role="row">
                                    <th >
                                        ID 
                                    </th>
                                    <th >
                                        Comentario
                                    </th>
                                    <th >
                                        Imagen
                                    </th>
                                </tr>
                            </thead>
                            <tbody id="images">
                                @foreach ($feedbacks as $feedback)
                                    
                                <tr role="row" class="odd">
                                    <td class="sorting_1 dtr-control">
                                        {{$feedback->id}}
                                    </td>
                                    <td>
                                        {{$feedback->comment}}
                                    </td>
                                    <td class="sorting_1 dtr-control cursor-pointer">
                                        <img src="/uploads/{{$feedback->img}}" width="100">
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                </div>
            </div>
        </div>
    </div>

@endsection

{{-- Styles Section --}}
@section('styles')
<link rel="stylesheet" href="{{ asset('css/custom-starter/viewer.min.css') }}" integrity="sha512-r+5gXtPk5M2lBWiI+/ITUncUNNO15gvjjVNVadv9qSd3/dsFZdpYuVu4O2yELRwSZcxlsPKOrMaC7Ug3+rbOXw==" crossorigin="anonymous" referrerpolicy="no-referrer" />
<style>
.cursor-pointer {
    cursor: pointer;
}
</style>
@endsection

{{-- Scripts Section --}}
@section('scripts')
<script src="{{ asset('plugins/custom/datatables/datatables.bundle.js') }}" type="text/javascript"></script>
<script src="{{ asset('js/custom-starter/viewer.js') }}" type="text/javascript"></script>
<script>
    const gallery = new Viewer(document.getElementById('images'));
    $('#index-table-log').DataTable({
        destroy: true,
        order: [[ 0, "desc" ]],
        responsive: true,
        language: {
            sProcessing:    "Procesando...",
            sLengthMenu:    "Mostrar _MENU_ registros",
            sZeroRecords:   "No se encontraron resultados",
            sEmptyTable:    "Ningún dato disponible en esta tabla",
            sInfo:          "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
            sInfoEmpty:     "Mostrando registros del 0 al 0 de un total de 0 registros",
            sInfoFiltered:  "(filtrado de un total de _MAX_ registros)",
            sInfoPostFix:   "",
            sSearch:        "Buscar:",
            sUrl:           "",
            sInfoThousands:  ",",
            sLoadingRecords: "Cargando...",
            oPaginate: {
                sFirst:    "Primero",
                sLast:    "Último",
                sNext:    "Siguiente",
                sPrevious: "Anterior"
            },
            oAria: {
                sSortAscending:  ": Activar para ordenar la columna de manera ascendente",
                sSortDescending: ": Activar para ordenar la columna de manera descendente"
            }
        }
    });
    
    
</script>
@endsection

