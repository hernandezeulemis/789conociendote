<div class="card">
  <div class="card-header">
   
  </div>
  <div class="card-body">
    <p >
      Estimado: <strong>{{strtoupper($therapist->full_name)}}</strong> </p> <br>
      <p>Es un placer facilitarte los detalles de acceso a la plataforma de Conociéndote para que puedas comenzar a utilizar sus funcionalidades de manera efectiva. A continuación, encontrarás la información necesaria:</p><br>

      <p><strong>URL de Acceso:</strong>  <a href="https://sistema.conociendote.com.mx/login"></a>https://sistema.conociendote.com.mx/</p>
      <p><strong>Correo electrónico: </strong>  {{ $email }}</p>
      <p><strong>Contraseña: </strong> {{ $password }}</p>

      <p>Te recomendamos cambiar tu contraseña después del primer inicio de sesión por razones de seguridad. Además, asegúrate de mantener esta información de manera confidencial y no compartirla con terceros.</p>

      <p>Agradecemos tu colaboración y confianza en nuestra plataforma. Esperamos que disfrutes de una experiencia eficiente y segura.</p><br>

      Saludos cordiales,
      <strong>Conociéndote</strong>
    </p>  
    
  </div>
</div>