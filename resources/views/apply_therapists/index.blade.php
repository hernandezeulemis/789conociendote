<!DOCTYPE html>
<html lang="en">
<!--begin::Head-->

<head>
    <base href="../../../../">
    <meta charset="utf-8" />
    <title>Postulación de Terapeutas</title>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <!--begin::Fonts-->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" />
    <!--end::Fonts-->
    <!--begin::Page Custom Styles(used by this page)-->
    <link href="/css/pages/login/classic/login-4.css?v=7.0.4" rel="stylesheet" type="text/css" />
    <!--end::Page Custom Styles-->
    <!--begin::Global Theme Styles(used by all pages)-->
    <link href="/plugins/global/plugins.bundle.css?v=7.0.4" rel="stylesheet" type="text/css" />
    <link href="/plugins/custom/prismjs/prismjs.bundle.css?v=7.0.4" rel="stylesheet" type="text/css" />
    <link href="/css/style.avatar.css" rel="stylesheet" type="text/css" />
    <link href="/css/style.bundle.css" rel="stylesheet" type="text/css" />
    
    <!--end::Global Theme Styles-->
    <!--begin::Layout Themes(used by all pages)-->
    <link href="/css/themes/layout/header/base/light.css?v=7.0.4" rel="stylesheet" type="text/css" />
    <link href="/css/themes/layout/header/menu/light.css?v=7.0.4" rel="stylesheet" type="text/css" />
    <link href="/css/themes/layout/brand/dark.css?v=7.0.4" rel="stylesheet" type="text/css" />
    <link href="/css/themes/layout/aside/dark.css?v=7.0.4" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/sweetalert2@11.0.19/dist/sweetalert2.min.css">
    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
        {{-- Fonts --}}
    {{ Metronic::getGoogleFontsInclude() }}

    {{-- Global Theme Styles (used by all pages) --}}
    @foreach(config('layout.resources.css') as $style)
        <link href="{{ config('layout.self.rtl') ? asset(Metronic::rtlCssPath($style)) : asset($style) }}"
              rel="stylesheet" type="text/css"/>
    @endforeach

    {{-- Layout Themes (used by all pages) --}}
    @foreach (Metronic::initThemes() as $theme)
        <link href="{{ config('layout.self.rtl') ? asset(Metronic::rtlCssPath($theme)) : asset($theme) }}"
              rel="stylesheet" type="text/css"/>
    @endforeach

    <link href="{{ asset('plugins/custom/datatables/datatables.bundle.css') }}" rel="stylesheet" type="text/css"/>
    <link rel="stylesheet" href="{{asset('css/custom.css')}}">
    <link rel="stylesheet" href="{{ asset('css/custom-starter/jquery.feedback.min.css') }}" />
    <!--end::Layout Themes-->
    <style>
        .error {
            color: red;
        }
        .require {
            color: red;
        }
        .hidden { display: none; }
        .file-li{
                display: flex;
                -webkit-box-pack: justify;
                -ms-flex-pack: justify;
                justify-content: space-between;
                -webkit-box-align: center;
                -ms-flex-align: center;
                align-items: center;
                padding: 0.5rem 1rem;
                background-color: #f7f8fa;
                margin-top: 0.75rem;
                border-radius: 4px;
        }
        .delete-file{
            -webkit-transition: color .3s ease;
            transition: color .3s ease;
            font-size: .7rem;
            color: #a2a5b9;
        }
        .card {
            position: relative;
            display: flex;
            flex-direction: column;
            min-width: 0;
            word-wrap: break-word;
            background-color: #d8e1df;
            background-clip: border-box;
            border: 1px solid #ECF0F3;
            border-radius: 0.42rem;
        }
        .white_section{
            background-color: white;
            padding: 15px;
            border-radius: 10px;
            margin-bottom: 25px;
        }
        /* start */
        .custom-input-file {
            background-color: #e7a4a2;
            color: #fff;
            cursor: pointer;
           
            font-weight: bold;
            margin: 0 auto 0;
            min-height: 15px;
            overflow: hidden;
            padding: 10px;
            position: relative;
            text-align: center;
            width: 400px;
            border-radius:5px;
        }

        .custom-input-file .input-file {
            border: 10000px solid transparent;
            cursor: pointer;
            font-size: 10000px;
            margin: 0;
            opacity: 0;
            outline: 0 none;
            padding: 0;
            position: absolute;
            right: -1000px;
            top: -1000px;
        }
        /* end */
     
    .loading-overlay {
        position: fixed;
        top: 0;
        left: 0;
        width: 100%;
        height: 100%;
        background-color: rgba(0, 0, 0, 0.5);
        display: flex;
        justify-content: center;
        align-items: center;
        flex-direction: column;
        z-index: 9999;
    }

    .spinner {
        border: 4px solid #f3f3f3;
        border-top: 4px solid #3498db;
        border-radius: 50%;
        width: 40px;
        height: 40px;
        animation: spin 1s linear infinite;
    }
    .loading-text {
        margin-top: 10px;
        color: white;
        font-size: 16px;
    }

    @keyframes spin {
        0% {
            transform: rotate(0deg);
        }
        100% {
            transform: rotate(360deg);
        }
    }

    .hidden {
        display: none;
    }

    .badge-remove-address {
            position: absolute;
            top: 32px;
            right: -6px;
            cursor:pointer;
        }


    </style>
    <link rel="shortcut icon" href="/media/favicon.ico" />
    @section('styles')
    @endsection
</head>
<!--end::Head-->
<!--begin::Body-->

<body id="kt_body" class="header-fixed header-mobile-fixed subheader-enabled subheader-fixed aside-enabled aside-fixed aside-minimize-hoverable page-loading">
    <!--begin::Main-->
    <div class="d-flex flex-column flex-root">
        <!--  cookie consent -->
        <div class="card">
            <div class="card-body">

                <div class="row">
                    <div class="col-md-12">
                        <h1 style="text-align: center; color: #ed8885;">Postulación de Terapeutas</h1>

                        <div class="row justify-content-center my-10 px-8 my-lg-15 px-lg-10 ">
                            <div class="col-xl-12 col-xxl-9">
                                <form class="form fv-plugins-bootstrap fv-plugins-framework" id="formCRUDTerapeutas" >
                                    @csrf
                                    <div class="pb-5" data-wizard-type="step-content" data-wizard-state="current">
                                        <div class="white_section">
                                            <div class="d-flex justify-content-center" style="padding:15px">
                                                <div class="form-group row">
                                                    <div class="text-center">
                                                        <div class="kt-avatar kt-avatar--outline" id="kt_user_avatar_1">
                                                            <div class="kt-avatar__holder" style="background-image: url('/img/avatar.png')"></div>
                                                            <label class="kt-avatar__upload" data-toggle="kt-tooltip" title="" data-original-title="">
                                                                <i class="fa fa-pen"></i>
                                                                <input type="file" name="avatar" id="avatar" >
                                                            </label>
                                                            <span class="kt-avatar__cancel" data-toggle="kt-tooltip" title="" data-original-title="">
                                                                <i class="fa fa-times"></i>
                                                            </span>
                                                        </div>
                                                        <p class="text-center" style="font-weight: bold;">Sube una fotografia de buena calidad con fondo<br> blanco de frente que se vea tu rostro</p>
                                                        <span class="form-text text-muted">Archivos Permitidos:  png, jpg, jpeg.</span>
                                                        <span class="form-text text-muted">Tamaño máximo permitido 2MB.</span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="white_section">
                                            <div class="row">
                                                <div class="col-xl-6">
                                                    <div class="form-group fv-plugins-icon-container">
                                                        <label >Nombre Completo <span class="require">*</span> </label>
                                                        <input type="text" class="form-control form-control-solid form-control-lg" name="full_name" id="full_name" placeholder="Nombre Completo" value="" required>
                                                        <div class="fv-plugins-message-container"></div>
                                                    </div>
                                                </div>
                                                <div class="col-xl-6">
                                                    <div class="form-group">
                                                        <label>Correo<span class="require">*</span></label>
                                                        <input type="email" class="form-control form-control-solid form-control-lg" name="email" id="email" placeholder="Correo" value="" required>
                                                    </div>
                                                </div>
                                            </div>  
                                            <div class="row">
                                                <div class="col-xl-4">
                                                    <div class="form-group fv-plugins-icon-container">
                                                        <label class="col-12 col-form-label">Fecha de Nacimiento<span class="require">*</span></label>
                                                        <input class="form-control" type="date" value="birthdate" id="" name="birthdate" required>
                                                        <span class="form-text text-muted">Agregue Fecha de Nacimiento.</span>
                                                        <div class="fv-plugins-message-container"></div>
                                                    </div>
                                                </div>
                                                <div class="col-xl-4">
                                                    <div class="form-group fv-plugins-icon-container">
                                                        <label class="col-6 col-form-label">Teléfono</label>
                                                        <input type="text" class="form-control form-control-solid form-control-lg" name="phone" id="phone" placeholder="Teléfono" value="" required>
                                                        <div class="fv-plugins-message-container"></div>
                                                    </div>
                                                </div>
                                                <div class="col-xl-4">
                                                    <div class="form-group fv-plugins-icon-container">
                                                        <label class="col-6 col-form-label">Sexo<span class="require">*</span></label>
                                                        <select name="sexo" id="sexo" class="form-control form-control-solid form-control-lg" required>
                                                            <option value="">Seleccione...</option>
                                                            <option value="m">Masculino</option>
                                                            <option value="f">Femenino</option>
                                                        </select>
                                                    </div>
                                                    <!--end::Input-->
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-xl-6">
                                                    <div class="form-group fv-plugins-icon-container">
                                                        <label class="col-12 col-form-label" id="language_label">Idioma<span class="require">*</span><strong>(Puedes seleccionar más de una opción)</strong></label>
                                                        <select name="language_id[]" id="language" class="form-control form-control-solid form-control-lg select2" multiple required>
                                                            <option value="">Seleccione...</option>
                                                            @foreach($language as $key => $text)
                                                            <option value="{{$key}}">{{ $text }}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="white_section">
                                            <div class="row">
                                                <div class="col-xl-6">
                                                    <div class="form-group fv-plugins-icon-container">
                                                        <label>Licenciatura en Psicología<span class="require">*</span></label>
                                                        <select name="lic_psicologia" id="lic_psicologia" class="form-control form-control-solid form-control-lg" required>
                                                            <option value="">Seleccione...</option>
                                                            <option value="si">Si</option>
                                                            <option value="no">No</option>
                                                            <option value="otro">Otro</option>
                                                        </select>
                                                        <div class="fv-plugins-message-container"></div>
                                                    </div>
                                                </div>
                                                <div class="col-xl-6 hidden" id="lic_text">
                                                    <div class="form-group fv-plugins-icon-container">
                                                        <label>Otro</label>
                                                        <input type="text" class="form-control form-control-solid form-control-lg" name="lic_otro" id="lic_otro" required placeholder="">
                                                        <div class="fv-plugins-message-container"></div>
                                                    </div>
                                                </div>
                                                <div class="col-xl-6">
                                                    <div class="form-group fv-plugins-icon-container">
                                                        <label>Maestría o Especialidad<span class="require">*</span></label>
                                                        <select name="if_master_clinica"  class="form-control form-control-solid form-control-lg master_clinica" required>
                                                            <option value="">Seleccione...</option>
                                                            <option value="si">Si</option>
                                                            <option value="no">No</option>
                                                            <option value="otro">Otro</option>
                                                        </select>
                                                        <div class="fv-plugins-message-container"></div>
                                                    </div>
                                                </div>
                                                <div id="maestria" class="col-xl-6 hidden" >
                                                    <!--begin::Select-->
                                                    <div class="form-group fv-plugins-icon-container">
                                                        <label>Maestría o Especialidad Clínica<span class="require">*</span></label>
                                                        <select name="clinical_specialty_id" id="clinical_specialties" class="form-control form-control-solid form-control-lg" required>
                                                            <option value="">Seleccione...</option>
                                                            @foreach($clinical_specialties as $key => $text)
                                                            <option value="{{$key}}">{{ $text }}</option>
                                                            @endforeach
                                                        </select>
                                                        <div class="fv-plugins-message-container"></div>
                                                    </div>
                                                    <!--end::Select-->
                                                </div>
                                                <div class="col-xl-6 hidden" id="master_text">
                                                    <div class="form-group fv-plugins-icon-container">
                                                        <label>Otro</label>
                                                        <input type="text" class="form-control form-control-solid form-control-lg" name="master_otro" id="master_otro" required placeholder="">
                                                        <div class="fv-plugins-message-container"></div>
                                                    </div>
                                                </div>
                                                <div class="col-xl-6">
                                                    <div class="form-group fv-plugins-icon-container">
                                                        <label class="col-12 col-form-label">Semestre de Maestría<span class="require">*</span></label>
                                                        <select name="semester" id="semester" class="form-control form-control-solid form-control-lg" required>
                                                            <option value="primera_mitad">1era Mitad</option>
                                                            <option value="segunda_mitad">2da Mitad</option>
                                                            <option value="terminada">Terminada</option>
                                                        </select>
                                                    </div>
                                                </div>           
                                                <div class="col-xl-6">
                                                    <div class="form-group fv-plugins-icon-container">
                                                        <label class="col-9 col-form-label">Cédula Licenciatura</label>
                                                        <input type="text" class="form-control form-control-solid form-control-lg"  name="cedula" id="cedula" placeholder="Cédulas Profesionales" data-tippy-content="Escribe el número de tu cédula profesional más actual" >
                                                    </div>
                                                </div>
                                           
                                                <div class="col-xl-6">
                                                    <div class="form-group fv-plugins-icon-container">
                                                        <label class="col-9 col-form-label">Cédula Maestría</label>
                                                        <input type="text" class="form-control form-control-solid form-control-lg"  name="cedula_master" id="cedula_master" placeholder="Cédulas Maestría" >
                                                    </div>
                                                </div>
                                                <div class="col-xl-6">
                                                    <!--begin::Input-->
                                                    <div class="form-group fv-plugins-icon-container">
                                                        <label class="col-6 col-form-label">Corriente Principal<span class="require">*</span></label>
                                                        <select name="current_principal_id" id="current_principals" class="form-control form-control-solid form-control-lg" required>
                                                            <option value="">Seleccione...</option>
                                                            @foreach($current_principals as $key => $text)
                                                            <option value="{{$key}}">{{ $text }}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                    <!--end::Input-->
                                                </div>
                                                
                                         
                                                <!-- <div class="col-xl-6">
                                                    <div class="form-group fv-plugins-icon-container">
                                                        <label class="col-6 col-form-label" >Tiene Corrientes Complementarias?</label>
                                                        <select name="if_complementaries" id="current_complementaries" class="form-control form-control  form-control-solid form-control-lg">
                                                            <option value="">Seleccione...</option>
                                                            <option value="si">Si</option>
                                                            <option value="no">No</option>
                                                            <option value="otro">Otro</option>
                                                        </select>
                                                    </div>
                                                </div> -->
                                                <div class="col-xl-6" id="col_current">
                                                    <div class="form-group fv-plugins-icon-container">
                                                        <label class="col-12 col-form-label" id="complementaries">Corrientes Complementarias <span class="require">*</span><strong>(Puedes seleccionar más de una opción)</strong></label>
                                                        <select name="current_complementary_id[]" id="current_complementaries" class="form-control form-control  form-control-solid form-control-lg select2" multiple>
                                                            @foreach($current_complementaries as $key => $text)
                                                            <option value="{{$key}}">{{ $text }}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>

                                                </div>
                                                <!-- <div class="col-xl-3" id="current_otro">
                                                    <div class="form-group fv-plugins-icon-container">
                                                        <label class="col-6 col-form-label" id="complementaries">Corrientes Complementarias</label>
                                                        <input type="text" name="otro_current" id="otro_current" class="form-control">
                                                    </div>
                                                </div> -->
                                                <div class="col-xl-6">
                                                    <div class="form-group fv-plugins-icon-container">
                                                        <label class="col-12 col-form-label" id="problems">Problemáticas Específicas<span class="require">*</span> <strong>(Puedes seleccionar más de una opción)</strong></label>
                                                        <select name="specific_problem_id[]" id="specific_problems" class="form-control form-control-solid form-control-lg select2" multiple required>
                                                            
                                                            @foreach($specific_problems as $key => $text)
                                                            <option value="{{$key}}">{{ $text }}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>
                                                
                                         
                                                <div class="col-xl-4">
                                                    <div class="form-group fv-plugins-icon-container">
                                                        <div class="form-group">
                                                            <label class="col-12 col-form-label" id="poblation_label">Población<span class="require">*</span> <br><strong>(Puedes seleccionar más de una opción)</strong></label>
                                                            <select name="poblacion[]" id="poblacion" class="form-control form-control-solid form-control-lg select2" multiple required>
                                                                @foreach($poblation as $key => $text)
                                                                <option value="{{$key}}">{{ $text }}</option>
                                                                @endforeach
                                                            </select>
                                                            
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-xl-4">
                                                    <div class="form-group fv-plugins-icon-container">
                                                        <label class="col-6 col-form-label">Modalidad<span class="require">*</span></label>
                                                        <select name="modality" id="modality" class="form-control form-control-solid form-control-lg" required>
                                                            <option value="">Seleccione...</option>
                                                            <option value="lineal">Línea</option>
                                                            <option value="presencial">Presencial</option>
                                                            <option value="ambas">Ambas</option>
                                                        </select>
                                                    </div>
                                                </div>  
                                                <div class="col-xl-4">
                                                    <div class="form-group fv-plugins-icon-container">
                                                        <label class="col-6 col-form-label">Tipo de Terapia</label>
                                                        <select name="therapy_type" id="therapy_type" class="form-control form-control-solid form-control-lg" required>
                                                            <option value="individual">Individual</option>
                                                            <option value="pareja">Pareja</option>
                                                            <option value="familiar">Familiar</option>
                                                        </select>
                                                    </div>
                                                </div>     
                                            </div>
                                    </div>
                                        
                                           <div class="white_section" id="filed_direction">
                                            <div class="col-md-12">
                                                <span style="font-size: large; font-weight: 600; color: #e7a4a2">Dirección de Consultorio</span><br>
                                               
                                            </div>
                                           <div class="col-xl-2 mt-12">
                                                <button type="button" class="btn btn-primary btn-block" id="addAddress" style="margin-bottom: 20px;"><strong><i class="fa fa-plus"></i> Añadir Dirección</strong> </button>
                                            </div>
                                       <!--  <label style="margin-bottom: -5%;" class="col-12"><strong>Añadir Dirección de Consultorios</strong></label> -->
                                    
                                        <!-- <div class="col-xl-1 mt-12">
                                            <button type="button" class="btn btn-primary btn-block" id="addAddress"><i class="fa fa-plus"></i></button>
                                        </div> -->
                                        <div class="col-xl-12">
                                            <div id="addressclinic"></div>
                                        </div>
                        
                                             
                                        </div>
                                             
                                        
                                    </div>
                                    <!-- DUPLICAR FORM -->
                                    <div  id="kt_repeater_1" class="white_section">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <span style="font-size: large; font-weight: 600; color: #e7a4a2">Estudios</span><br>
                                                <strong>(Llena la información de tus estudios uno por uno y los vas subiendo)</strong>
                                            </div>
                                        </div><br><br>
                                        <div class="form-group form-group-last row" id="kt_repeater_2">
                                            
                                            <div data-repeater-list="" class="col-lg-12">
                                                <div data-repeater-item="" class="form-group row align-items-center">     
                                                    <div class="col-md-4">
                                                        <div class="kt-form__group--inline">
                                                            <div class="kt-form__label">
                                                                <label>Licenciatura/Maestría/Diplomado:</label>
                                                            </div>
                                                            <div class="kt-form__control">
                                                                <input type="text" class="form-control" placeholder="Licenciatura" name="licenciatura" id="licenciatura"> 
                                                            </div>
                                                        </div>
                                                        <div class="d-md-none kt-margin-b-10"></div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="kt-form__group--inline">
                                                            <div class="kt-form__label">
                                                                <label class="kt-label m-label--single">Nombre de la Institución</label>
                                                            </div>
                                                            <div class="kt-form__control">
                                                                <input type="text" class="form-control" placeholder="Nombre de la Institución" name="institucion" id="institucion">   
                                                            </div>
                                                        </div>
                                                        <div class="d-md-none kt-margin-b-10"></div>
                                                    </div>  
                                                    <div class="col-lg-4 mt-8 pr-3" id="subir_file">
                                                        <div class="custom-input-file col-md-6 col-sm-6 col-xs-6">
                                                        <input type="file" id="fichero-tarifas" class="input-file" onchange="getImgBase()"    name="uploadfiles[]"  multiple="multiple">
                                                            Subir Archivos...
                                                        </div>
                                                    </div>
                                                </div>                            
                                            </div>                 
                                        </div>
                                        <div class="form-group form-group-last row">
                                            <div class="col-lg-2">
                                                <a href="javascript:;" class="btn btn-primary btn-block"  id="btnAdd">
                                                    <i class="la la-plus"></i> Subir Estudio
                                                </a>
                                            </div>   
                                            <div style="display: flex;justify-content: end;align-items: end;" class="col-lg-8" id="archivos_seleccionados"></div>                                     
                                        </div>
                                        <br>
                                        <div id="certificateList" class="border-top"></div>
                                    </div>
                                    <div id="loading" class="loading-overlay hidden" >
                                        <div class="spinner"></div>
                                        <div class="loading-text">Enviando Datos al Server...</div>
                                    </div>
                                    
                                    
                                    <!--begin::Wizard Actions-->
                                    <div class="d-flex justify-content-between  mt-5 pt-10">

                                        <div>
                                            <button type="submit" class="btn btn-success font-weight-bolder text-uppercase px-9 py-4">
                                                Enviar
                                            </button>
                                            <button type="reset" class="btn btn-secondary font-weight-bolder text-uppercase px-9 py-4">
                                                Cancelar
                                            </button>
                                        </div>
                                    </div>

                                </form>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    <script src="/plugins/global/plugins.bundle.js?v=7.0.4"></script>
    <script src="/plugins/custom/prismjs/prismjs.bundle.js?v=7.0.4"></script>
    <script src="/js/scripts_nuevo.bundle.js"></script>
    <script src="/js/ktavatar.js"></script>
    <script src="/js/select2.js"></script>
    
    <script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-loading-overlay/2.1.7/loadingoverlay.min.js" ></script>
    <script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/jquery.validate.min.js"></script>
    <script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/additional-methods.min.js"></script>
    </script><script src="{{ asset('js/messages_es.js') }}"></script>
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@11.0.19/dist/sweetalert2.all.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
    <script src="https://unpkg.com/@popperjs/core@2"></script>
    <script src="https://unpkg.com/tippy.js@6"></script>
    <script src="{{ asset('js/functions.js') }}"></script>
    <script>
        const url = '{{url($_module_route)}}'
        CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content'); 

        $('#cod_postal').blur(function() {
            
            $.get({
                url: 'https://sepomex.789.com.mx/' + $(this).val(),
                type: 'get',
                beforeSend: function() {
                    //$.LoadingOverlay('show');
                },
                success: function(response, statusText, xhr) {
                    /* var opColonia = '<option selected value="' + response['asentamientos'][0] + '">' + response['asentamientos'][0] + '</option>';
                    var opCity = '<option selected value="' + response['estados'][0] + '">' + response['estados'][0] + '</option>';
                    var opDelegation = '<option selected value="' + response['municipios'][0] + '">' + response['municipios'][0] + '</option>'; */

                    $('#colonia').val(response['asentamientos'][0]);
                    $('#city').val(response['estados'][0]);
                    $('#delegation').val(response['municipios'][0]);
                },
                complete: function() {
                    //$.LoadingOverlay('hide');
                }
            });
        });
    </script>
    <script>
        $(document).ready(function() {
            window.arrayCertificate = [];
            $("#formCRUDTerapeutas").validate({
                errorClass: "error",
                errorElement: "span",
                errorPlacement: function(error, element) {
                    error.appendTo(element.parent());
                },
                submitHandler: function(form, event) {
                    event.preventDefault();
                    var loadingOverlay = document.getElementById('loading');
                    loadingOverlay.classList.remove('hidden');
                    var formData = new FormData(form); // Crear un objeto FormData directamente desde el formulario

                    CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
                    var  avatar    = document.getElementById('avatar');

                    formData.append('fileFoto', avatar.files[0]);
                    
                    

                    for (let i = 0; i < arrayCertificate.length; i++) {
                        formData.append(`titulos[${i}][licenciatura]`, arrayCertificate[i].licenciatura);
                        formData.append(`titulos[${i}][institucion]`, arrayCertificate[i].institucion);

                        for (let j = 0; j < arrayCertificate[i].file.length; j++) {
                            formData.append(`titulos[${i}][file][${j}]`, arrayCertificate[i].file[j]);
                        }
                    }

                    var input = document.getElementById('cedula');
                    var valor = input.value;
                    var regex = /^[a-zA-Z0-9]{7,8}$/;
                    
                   // $.LoadingOverlay("show");
                    $.ajax({
                        type: "POST",
                        url: url+ "/store",
                        headers: {
                            "X-CSRF-TOKEN": CSRF_TOKEN
                        },
                        data: formData,
                        processData: false, // Deshabilitar el procesamiento automático de datos
                        contentType: false, // Deshabilitar la configuración automática del tipo de contenido
                        success: function(response) {
                            form.reset();
                            arrayCertificate = [];
                            $('#certificateList').empty();
                            $('#specific_problems,#language,#current_complementaries,#poblacion').trigger('change');
                            $('#avatar').val('');

                            $('.kt-avatar__holder').css("background-image", "url('/img/avatar.png')");

                            //$.LoadingOverlay("hide");
                            loadingOverlay.classList.add('hidden');
                            Swal.fire({
                                icon: 'success',
                                title: 'Datos guardados exitosamente',
                                showConfirmButton: false,
                                timer: 1500
                            });
                        },
                        error: function(xhr, status, error) {
                            console.log(xhr.responseText);
                        }
                    });
                }
            });
            var dataUpload = []
            getImgBase = function () {
   
                const _this = this
                var event = event || window.event;
                var files = event.target.files;
               //console.log(files)
                var filesname = '';
                for (let i = 0; i < files.length; i++) {
                    //console.log(files[i])
                    const reader = new FileReader()
                    dataUpload.push(files[i]);
                    filesname += '<span class="btn btn-default btn-sm ml-3" style="cursor:pointer">' + files[i].name + '</span>';
                  
                }
                $('#archivos_seleccionados').html(filesname);
            }
            delImg = function (index) {
                //console.log(index)
                dataUpload.splice(index,1);
            }

            $('#btnAdd').click(function() {
             
                var lic = $('#licenciatura').val()
                var int = $('#institucion').val()
        
                if(lic != '' && int != '' && dataUpload.length > 0 ){
                    arrayCertificate.push({
                        licenciatura : lic,
                        institucion: int,
                        file:dataUpload
                    })
                    var lic = $('#licenciatura').val('')
                    var int = $('#institucion').val('')
                    var file = $('#fichero-tarifas').val('')
                    dataUpload = []
                    $('#archivos_seleccionados').empty()
                }else{
                    alert('Debe completar los campos')
                }
                
                // Construir el HTML
                var html = '';
                for (var i = 0; i < arrayCertificate.length; i++) {
                    var certificate = arrayCertificate[i];
                    var filesHtml = '';

                    // Construir el HTML para los archivos subidos
                    for (var j = 0; j < certificate.file.length; j++) {
                        var file = certificate.file[j];
                        var fileName = file.name;
                        var fileExtension = fileName.substring(fileName.lastIndexOf('.') + 1);
                        filesHtml += '<span class="btn btn-success ml-3">' + fileName + ' (' + fileExtension + ')</span>';
                    }

                    html += '<div class="card mb-3">';
                    html += '<div class="card-body" style="display: flex;justify-content: space-around;align-items: baseline;flex-wrap:wrap">';
                    html += '<h5 class="card-title">' + certificate.licenciatura + '</h5>';
                    html += '<h6 class="card-subtitle mb-2 text-muted">' + certificate.institucion + '</h6>';
                    html += '<div class="card-text">' + filesHtml + '</div>';
                    html += '<button class="btn btn-danger btn-xs delete-certificate" data-index="' + i + '">Eliminar</button>';
                    html += '</div>';
                    html += '</div>';
                }

                // Mostrar el HTML construido
                $('#certificateList').html(html);
            

                // Manejar el evento de eliminación de elementos
                $('.delete-certificate').click(function() {
                    var index = $(this).data('index');
                    arrayCertificate.splice(index, 1);
                    $(this).closest('div').remove();
                });


            });
       
         
            $('#specific_problems, #current_complementaries, #poblacion, #language').select2();
            $('#specific_problems, current_complementaries, #poblacion, #language').select2({
                multiple: true
            });

            $('#lic_psicologia').on('change', function(){
                if($(this).val() == 'otro'){
                    $('#lic_text').removeClass('hidden');
                }else{
                   $('#lic_text').addClass('hidden'); 
                }
            })
            $('.master_clinica').on('change',function(){
                if($(this).val() == 'otro'){
                    $('#master_text').removeClass('hidden');
                    $('#maestria').addClass('hidden'); 
                }else if($(this).val() == 'si'){
                   $('#maestria').removeClass('hidden');
                   $('#master_text').addClass('hidden'); 
                }else{
                    $('#maestria').addClass('hidden'); 
                    $('#master_text').addClass('hidden'); 
                }
            })

      
             /* codigo dinamico para añadir direcciones */
                 var int = 100
                $('#addAddress').click(function() {
                    int++
                    var html = '';

                    html +='<div class="form-group fv-plugins-icon-container">';
                    html +='<textarea name="address[]" id="address" cols="30" rows="3" class="form-control form-control-solid" ></textarea>';
                    html +='<span class="badge badge-danger badge-remove-address"><i style="color:white;" class="fas fa-times"></i></span>';
                
                    html +='<div class="row">';
                    html += '<div class="col-xl-3">';
                    html += '<div class="form-group fv-plugins-icon-container">';
                    html += '<label class="col-6 col-form-label">Código Postal</label>';
                    html += '<input onblur="getAddress('+int+')" type="text" name="cod_postal[]" id="cod_postal'+int+'" class="form-control form-control-solid form-control-lg cod_postal"  placeholder="Código Postal">';
                    html += '</div>';
                    html += '</div>';
                    html += '<div class="col-xl-3">';
                    html += '<div class="form-group fv-plugins-icon-container">';
                    html += '<label class="col-6 col-form-label">Ciudad</label>';
                    html += '<input type="text" class="form-control form-control-solid form-control-lg" name="city[]" id="city'+int+'">';
                    html += '</div>';
                    html += '</div>';
                    html += '<div class="col-xl-3">';
                    html += '<div class="form-group fv-plugins-icon-container">';
                    html += '<label class="col-9 col-form-label">Delegación o Municipio</label>';
                    html += '<input type="text" class="form-control form-control-solid form-control-lg" name="delegation[]" id="delegation'+int+'">';
                    html += '</div>';
                    html += '</div>';
                    html += '<div class="col-xl-3">';
                    html += '<div class="form-group fv-plugins-icon-container">';
                    html += '<label class="col-6 col-form-label">Colonia</label>';
                    html += '<input type="text" class="form-control form-control-solid form-control-lg" name="colonia[]" id="colonia'+int+'">';
                    html += '</div>';
                    html += '</div>';
                    html +='</div>';
                    html +='</div>';
            
                    $('#addressclinic').append(html); 
                });
                // Función para eliminar la sección de dirección
                function removeAddressSection(button) {
                    var addressSection = button.closest('.form-group');
                    addressSection.remove();
                }

                // Evento para eliminar la dirección al hacer clic en el botón
                $(document).on('click', '.badge-remove-address', function() {
                    int--
                    console.log(int)
                removeAddressSection(this);
                });
                getAddress = function(val){
                    var id = $('#cod_postal'+val+'').val();
                    $.get({
                        url: 'https://sepomex.789.com.mx/' + id,
                        type: 'get',
                        beforeSend: function() {
                            //$.LoadingOverlay('show');
                        },
                        success: function(response, statusText, xhr) {
                            
                            $('#colonia'+val+'').val(response['asentamientos'][0]);
                            $('#city'+val+'').val(response['estados'][0]);
                            $('#delegation'+val+'').val(response['municipios'][0]);

                        },
                        complete: function() {
                            //$.LoadingOverlay('hide');
                        }
                    });
                }
            /* fin del codigo */
            
            tippy('[data-tippy-content]');

            tippy('#cedula_master', {
                content: "Escribe el número de tu cédula profesional más actual",
            });
            
            tippy('#current_complementaries', {
                content: "Elige las opciones que correspondan con tu tipo de trabajo.",
            });
            
            tippy('#specific_problems', {
                content: "Elige una o varias opciones de problemáticas con las que trabajas",
            });

            tippy('#subir_file', {
                content: "Adjunta los archivos de tu cédula y títulos profesionales. ",
            });

            tippy('#current_principals', {
                content: "Elige la corriente con la que trabajas principalmente",
            });

            tippy('.kt-avatar__cancel', {
                content: "Cancel avatar",
            });

            tippy('.kt-avatar__upload', {
                content: "Sube una foto de frente y de buena calidad, procura que se vea profesional, esta foto aparecerá en el perfil que le compartiremos a los paciente que refiramos contigo.",
            });

            tippy('kt_uppy_5_input_control', {
                content: "El tamaño máximo de archivo es 1 MB y el número máximo de archivos es 5.",
            });

            tippy('#language_label', {
                content: "Selecciona más de una opción",
            });

            tippy('#complementaries', {
                content: "Selecciona más de una opción",
            });

            tippy('#problems', {
                content: "Selecciona más de una opción",
            });

            tippy('#poblation_label', {
                content: "Selecciona más de una opción",
            });


            $('#modality').change(function(){
                
                        
                if ($('#modality').val() == 'lineal'){
                    $('#filed_direction').addClass('hidden');
                    //$('#minimum_in_fee_lineal').removeClass('hidden');
                
                    $('#cod_postal').removeAttr('required');
               /*      $('#city').removeAttr('required');
                    $('#delegation').removeAttr('required');
                    $('#colonia').removeAttr('required'); */
            
                }else if($('#modality').val() == 'presencial'){
                    $('#filed_direction').removeClass('hidden');
                    $('#cod_postal').attr('required', true);
           /*          $('#city').prop('required', true);
                    $('#delegation').prop('required', true);
                    $('#colonia').prop('required', true); */
            
                }else{
                    $('#filed_direction').removeClass('hidden');
                    $('#cod_postal').prop('required', true);
             /*        $('#city').prop('required', true);
                    $('#delegation').prop('required', true);
                    $('#colonia').prop('required', true); */
                }
  
    });
            
            
        });
            
    </script>

</body>

</html>