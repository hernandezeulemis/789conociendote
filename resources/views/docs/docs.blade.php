@extends('layout.default')

@section('content')
{{-- Styles Section --}}
@section('styles')
<link rel="stylesheet" href="{{ asset('css/custom-starter/pre-code.css') }}">
@endsection
<pre class="line-numbers">
   <code class="language-js">
      const fullName = (name, lastName) {
        return `${name} ${lastName}`
      }
      console.log(fullName('Humberto', 'Andueza')) // Humberto Andueza
   </code>
</pre>
@endsection

{{-- Scripts Section --}}
@section('scripts')
<script src="{{ asset('js/custom-starter/pre-code.js') }}"></script>
@endsection